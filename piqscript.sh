#!/bin/bash

#wget https://pypi.python.org/packages/source/S/StarCluster/StarCluster-0.93.3.tar.gz
#tar xvzf StarCluster-0.93.3.tar.gz
#cd StarCluster-0.93.3
#sudo python distribute_setup.py
#sudo python setup.py install
#cd ..

EC2_ACCESS_KEY=#MY ACCESS KEY
EC2_SECRET_KEY=#MY SECRET KEY
EC2_ID= #MY EC2 ID
RSA_KEY_FILE=#DIR PATH TO RSA KEY

##########

CLUSNUM=4   # number of cluster nodes
BID=0.7      # bid price max (dollars/hr/node) (if ask>BID nodes shutdown)

EMAIL=${5:-'thashim@csail.mit.edu'}
#GENNAME=${6:-'BSgenome.Mmusculus.UCSC.mm10'}
#GENNAME=${6:-'BSgenome.Mmusculus.UCSC.mm9'}
GENNAME=${6:-'BSgenome.Hsapiens.UCSC.hg19'}
VOLNAME=${7:-piqvol-beta98-2}    # New volume name (uniq per concurrent job)
VOLSIZE=${8:-250}          # size in GB (per tatsu 20GB/sample)
CLUSNAME=${9:-piqclus-beta98-3}  # New cluster name (uniq per concurrent job)
GENTYPE=$(echo $GENNAME | cut -d. -f2)

echo Reticulating Splines

#key has directory stuff, so escape all the bad chars
KEY=$(echo $RSA_KEY_FILE | sed -e 's/[\/&]/\\&/g')
SECRET_KEY=$(echo $EC2_SECRET_KEY | sed -e 's/[\/&]/\\&/g')


#fill in stuff for config
# config.default is StarCluster(tm) config template
perl -p -e "s/#MY_ACCESS_KEY/$EC2_ACCESS_KEY/gi" config.default | perl -p -e "s/#MY_SECRET_KEY/$SECRET_KEY/gi" | perl -p -e "s/#MY_USER_ID/$EC2_ID/gi" | perl -p -e "s/#MY_KEY_LOCATION/$KEY/g" > config.new

echo Checking for EBS volume $VOLNAME

####### Check if volume exists, make new one or attach (found) unused one
if [ -z "$(starcluster -c config.new listvolumes | grep $VOLNAME$)" ]; then
    echo Making new EBS volume
    #make a new 100gb volume
    starcluster -c config.new createvolume -s --name=$VOLNAME $VOLSIZE us-east-1b
else
    echo EBS volume exists
fi

while [ -z "$(starcluster -c config.new listvolumes | grep -B5 $VOLNAME |grep available)" ]; do
echo Waiting for EBS to release lock.
sleep 10
done

# if[ null = starclsuter -c config.new listvolume |grep -B5 $VOLNAME |grep available] (spin)
# sleep 300

###### Finds volume ID from EC2, strips non-piq volumes, rewrites new StarCluster config file

volid=$(starcluster -c config.new listvolumes | grep -B5 $VOLNAME$  | sed -ne "s/volume_id: //p")
echo Using volume $volid

perl -p -e "s/# VOLUMES = dnasedata/VOLUMES = dnasedata/gi" config.new | perl -p -e "s/#\[volume dnasedata\]/\[volume dnasedata\]/gi" | perl -p -e "s/#VOLUME_ID = VOLID/VOLUME_ID = $volid/gi" | perl -p -e "s/#MOUNT_PATH = \/dnase/MOUNT_PATH=\/dnase/g" > config.ebs


###### Looks for clusters by my PIQ cluster name

sleep 300

echo Checking for clusters
if [ -z "$(starcluster -c config.ebs listclusters | grep $CLUSNAME' (')" ]; then
    echo Starting new cluster: $CLUSNAME
    starcluster -c config.ebs start $CLUSNAME
    while [ -z "$(starcluster -c config.ebs sshmaster $CLUSNAME 'df' | grep dnase)" ]; do
	echo 'NFS mount failed: rebooting to retry'
        starcluster -c config.ebs restart $CLUSNAME
    done
    starcluster -c config.ebs addnode -b $BID -n $CLUSNUM $CLUSNAME
else
    echo 'WARNINGS: Cluster already exists..'
    read -t 10 -p 'IF YOU DONT WANT CONTENTS OF EXISTING CLUSTER WIPED HIT CTRL+C NOW'
    echo
fi

###### Corrects ./scripts/allscript.r per genome from bioconductor

echo Using genome $GENNAME

perl -p -e "s/#GENOME/$GENNAME/gi" scripts/installpackages.r.default > scripts/installpackages.r
perl -p -e "s/#GENOME/$GENNAME/gi" scripts/allscript.r.default | perl -p -e "s/#GENTYPE/$GENTYPE/gi" | sed "s/#EMAIL/$EMAIL/g" > scripts/allscript.r


###### Creates directory structure/data on Master for each cluster

sleep 300
echo Moving new files

starcluster -c config.ebs put $CLUSNAME scripts/* /dnase/
starcluster -c config.ebs sshmaster $CLUSNAME 'chmod 777 /dnase'
starcluster -c config.ebs sshmaster -u ubuntu $CLUSNAME 'mkdir /dnase/pwms'
starcluster -c config.ebs sshmaster -u ubuntu $CLUSNAME 'mkdir /dnase/data'
starcluster -c config.ebs sshmaster -u ubuntu $CLUSNAME 'mkdir /dnase/tmp'
starcluster -c config.ebs sshmaster -u ubuntu $CLUSNAME 'mkdir /dnase/pwmout'
starcluster -c config.ebs sshmaster -u ubuntu $CLUSNAME 'mkdir /dnase/kernout'
starcluster -c config.ebs sshmaster -u ubuntu $CLUSNAME 'mkdir /dnase/bgout'
starcluster -c config.ebs sshmaster -u ubuntu $CLUSNAME 'mkdir /dnase/libs'
starcluster -c config.ebs sshmaster -u ubuntu $CLUSNAME 'mkdir /dnase/output'
starcluster -c config.ebs sshmaster $CLUSNAME 'rm /dnase/tmp/*'
starcluster -c config.ebs put $CLUSNAME ../common_pwms/* /dnase/pwms


####### Expects PIQ files to be in "./input/data" (named .txt)
#######   If not, looks in "./input/bamin", converts >> /input/data

echo Checking for data files
if [ -z "ls input/data | grep datin" ]; then
    echo Generating compressed data from BAMs
    cd script
    Rscript processbam.r   #converts bam -> PIQ .txt file
    cd ..
else
    echo Using existing data
fi



###### Copies local /input/data to master EC2 node /dnase/data

starcluster -c config.ebs put $CLUSNAME input/data/* /dnase/data

###### Set local params

echo Setting up runtime environment
starcluster -c config.ebs sshmaster $CLUSNAME 'sudo apt-get -y install libgsl0-dev'
starcluster -c config.ebs sshmaster $CLUSNAME 'sudo apt-get -y install fftw3 fftw3-dev libgsl0-dev'
starcluster -c config.ebs sshmaster $CLUSNAME 'sudo apt-get -y install r-base'
starcluster -c config.ebs sshmaster $CLUSNAME 'sudo apt-get -y install ess'
starcluster -c config.ebs sshmaster -u ubuntu $CLUSNAME 'Rscript /dnase/installpackages.r'

echo Setting up global variables
starcluster -c config.ebs sshmaster $CLUSNAME 'chmod -R 777 /dnase/*.r'
starcluster -c config.ebs sshmaster $CLUSNAME 'chmod -R 777 /dnase/*.R'
starcluster -c config.ebs sshmaster $CLUSNAME 'Rscript /dnase/allscript.r'    # does 'everything'


###### Grab output files
mkdir output
starcluster -c config.ebs get $CLUSNAME /dnase/output ./
#yes | starcluster -c config.ebs stop --terminate-unstoppable $CLUSNAME

####### Other files one can modify
### /scripts/processbam.r
### /scripts/allscript.r.default (template)













