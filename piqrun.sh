#!/bin/bash

CLUSNUM=5
CLUSNAME=${9:-piqclus-test}  # New cluster name (uniq per concurrent job)
RSA_KEY_FILE=~/.ssh/sckey.pem

cp $RSA_KEY_FILE tempkey
starcluster -c config.ebs listinstances | grep -m 1 -B 10 'groups: @sc-$CLUSNAME'

CLUSIP=$(starcluster -c config.ebs sshmaster $CLUSNAME ifconfig | grep -m 1 'inet addr' | cut -f 12 -d ' ' | cut -f 2 -d : | tr . -)
CLUSIPD=$(starcluster -c config.ebs listinstances | grep -B 2 $CLUSIP | grep ^dns_name: | cut -f 2 -d ' ')

expect -c "
    spawn ssh -i tempkey ubuntu@$CLUSIPD df
    expect \"Are you sure you want to continue connecting (yes/no)?\" {
        send \"yes\n\"
    }
    interact"    

echo retransferring script
starcluster -c config.ebs put $CLUSNAME scripts/* /dnase/

echo Setting up global variables
ssh -i tempkey root@$CLUSIPD 'chmod -R 777 /dnase/*.r' 
ssh -i tempkey root@$CLUSIPD 'chmod -R 777 /dnase/*.R' 

echo Starting run
ssh -i tempkey root@$CLUSIPD 'sh -c "( (Rscript /dnase/allscript.r </dev/null >nohup.out 2>&1) & )"' #runs and leaves it running

trap "echo Exited!; exit;" SIGINT SIGTERM

echo Waiting for termination or additional node request.
until  (ssh -i tempkey root@$CLUSIPD 'stat /dnase/done' > /dev/null 2>&1)
do
    printf "."
    ssh -i tempkey root@$CLUSIPD 'tail ~/nohup.out'
    if (ssh -i tempkey root@$CLUSIPD 'stat /dnase/reqnode' >/dev/null 2>&1) then
	echo Adding node
	ssh -i tempkey root@$CLUSIPD 'rm /dnase/reqnode'
	#starcluster -c config.ebs addnode -n $CLUSNUM $CLUSNAME
    fi
    sleep 10
done
ssh -i tempkey root@$CLUSIPD 'rm /dnase/done'

echo Transferring output
###### Grab output files
mkdir output
starcluster -c config.ebs get $CLUSNAME /dnase/err.out ./output/err.out
rsync -avW --size-only --progress --stats -e 'ssh -i tempkey' root@$CLUSIPD:/dnase/output/ ./output/

#starcluster -c config.ebs get $CLUSNAME /dnase/output ./

#yes | starcluster -c config.ebs stop --terminate-unstoppable $CLUSNAME

####### Other files one can modify
### /scripts/processbam.r
### /scripts/allscript.r.default (template)













