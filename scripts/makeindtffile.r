#!/usr/bin/Rscript

setwd('/dnase')
source('initec2.r')
#require("Rcpp",quietly=T)
#require("inline",quietly=T)

decodeRLE <- function(str,ncols=9){
  ss=strsplit(str,';',fixed=T)[[1]]
  ssi=strsplit(ss,',',fixed=T)
  rlein=list()
  rlein$lengths=as.integer(ssi[[1]])
  rlein$values=as.integer(ssi[[2]])
  class(rlein)='rle'
  rlevec=matrix(inverse.rle(rlein),ncol=ncols*2)
}

args=as.double(commandArgs(trailingOnly=TRUE))
day=args[1]
tfdelt=args[2]

load('/dnase/params.RData')

tfstart=as.double(Sys.getenv('SGE_TASK_ID'))
tfend=min(tfstart+tfdelt-1,maxtf)

blocksz=100
tfmax=maxtf
wid=wsize
  

for(chr in 1:chrlen){
    if(!file.exists(paste0(tmpfile,day,'-',chrstr[chr],'-',tfstart,'-factmat.RData'))){
        fconn=file(paste(datain,chrstr[chr],'datin.txt',sep=''),open='rt')
        rls=readLines(fconn)
        close(fconn)
        spos=1
        epos=length(list.files(pwmout,pattern=paste0('outfile-',chrstr[chr],'-')))
        if(epos>0){
            rcd=read.csv(paste0(datain,chrstr[chr],'rcoord.csv'))
            chrc=do.call(c,lapply(spos:epos,function(ppar){
                load(paste(pwmout,'outfile-',chrstr[chr],'-',ppar,'.RData',sep=''))
                                        #hastfs=sapply(pwmscores,function(i){any((tfstart:tfend)%in%i[,1])})
                do.call(c,lapply(1:length(pwmscores),function(i){
                    if(length(pwmscores[[i]])>0){
                        str=rls[(ppar-1)*100+i]
                        scd=rcd[(ppar-1)*100+i,2]
                        dtest=decodeRLE(str,numdays)
                        pws=pwmscores[[i]]
                                        #pwi=which(pws[,1]%in%(tfstart:tfend))
                        ptt=findInterval(c(tfstart-1,tfend),pws[,1])+c(1,0)
                        pwi=ptt[1]:ptt[2]
                        imm=outer(abs(pws[pwi,2])+500,-wid:wid,'-')
                        do=cbind(scd+abs(pws[pwi,2]),pws[pwi,1],pws[pwi,3],matrix(dtest[imm,2*(day-1)+1],nrow(imm)),matrix(dtest[imm,2*day],nrow(imm)))
                        apply(do,1,rle)
                    }else{
                        list()
                    }
                }))
            }))
            save(chrc,file=paste0(tmpfile,day,'-',chrstr[chr],'-',tfstart,'-factmat.RData'))
        }
    }
}
