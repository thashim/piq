#!/usr/bin/Rscript

source('initec2.r')
require("Rcpp",quietly=T)
require("inline",quietly=T)
require('CENTIPEDE',quietly=T)

args=as.double(commandArgs(trailingOnly=TRUE))

load('/dnase/params.RData')

tfid=as.double(Sys.getenv('SGE_TASK_ID'))

tfstart=floor(tfid/10)*10+1
fcall=lapply(1:chrlen,function(chr){
load(paste0(callsum,chr,'-',tfstart,'-factmat.RData'))
length(chrc)
chrid=sapply(chrc,function(i){i$values[2]})
pssid=sapply(chrc,function(i){i$values[3]})
mychr=which(chrid==tfid)
bigmat=t(sapply(c(1,2,mychr),function(i){as.integer(inverse.rle(chrc[[i]])[-(1:3)])}))
gc()
fcpo=tryCatch({
  fitCentipede(Xlist=list(bigmat),Y=cbind(1,pssid[c(1,2,mychr)]))
},error=function(e){
  fitCentipede(Xlist=list(bigmat+1e-50),sweeps=5)
})
#hist(fcpo$PostPr)
#plotProfile(fcpo$LambdaParList[[1]],Mlen=21)
#plot(fcpo$PostPr,log(rowSums(bigmat)),pch='.')
#plot(fcpo$PostPr,pssid[c(1,2,mychr)],pch='.')
fcpo
})
save(fcall,file=paste0(callsum,'cent2-',tfid,'.RData'))