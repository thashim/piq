#!/usr/bin/Rscript

setwd('/dnase')
system('sudo apt-get -y install libgsl0-dev')
source('initec2.r')
args=as.double(commandArgs(trailingOnly=TRUE))

day=args[1]

tfnum=as.double(Sys.getenv('SGE_TASK_ID'))

load('/dnase/params.RData')

lf=list.files(tmpfile,pattern=paste0('^',day,'-.*-',tfnum,'-factmat'))

chrlab=sapply(strsplit(lf,'-'),function(i){i[2]})
loaded=lapply(lf,function(i){load(paste0(tmpfile,i));do.call(rbind,lapply(chrc,inverse.rle))})
loadlab=do.call(c,lapply(1:length(loaded),function(i){rep(chrlab[i],nrow(loaded[[i]]))}))
datfull=do.call(rbind,loaded)
datsel=order(-datfull[,3])[1:min(nrow(datfull),maxpertf)]
datsub=datfull[datsel,-(1:3)]
datmeta=datfull[datsel,1:3]
labsub=loadlab[datsel]
rm(datfull);gc()

load(paste0(kernout,day,'-kernpars.RData'))

prsinit=sapply(1:1000,function(i){
  print(i)
  ctin=datsub[i,]
  fez=fitGP.ez(ctin,muin,ecvals,t(ecvecs),1e-3)
  ias=invapproxsol(fez[[4]],ecvecs,decvec,sparsebt)
  fcd=t(ecvecs*fez[[4]])%*%ias
  list(fez[[1]],fez[[2]])
})

allmat=do.call(rbind,prsinit[1,])
allsig=do.call(rbind,prsinit[2,])
delst=log((colSums(datsub[1:1000,])+0.1)/(colSums((exp(allmat+allsig/2)))+0.1))

adjust=1
tsa=diag(ecvals)%*%t(ecvecs)
vvv=diag(ecvecs%*%tsa)

parlist=lapply(1:nrow(datsub),function(i){datsub[i,]})

grfun<-function(dsum,expsum,par,k1,k2,g=0){
  negpen=-(par<g)*k1
  pdiff=sign(diff(par))
  diffder=k2*(c(0,pdiff)-c(pdiff,0))
  dsum-expsum*exp(par)-negpen-diffder
}

dsum=colSums(datsub[1:1000,])
expsum=colSums((exp(allmat+allsig/2)))
par=delst
eps=0.01
for(i in 1:2000){
  grs=grfun(dsum,expsum,par,5,5,0)
  par=par+eps/sqrt(i)*grs
  #plot(par,main=i,type='l')
}
delst=par+2

require('snow')
closeAllConnections()
cl <- makeCluster(4)
g=readLines("gpmodelCommon.R")
clusterExport(cl,"g")
tn=clusterCall(cl,function(){base::eval(parse(text=g),.GlobalEnv)})

adjust=1

for(m in 1:niter){
clusterExport(cl,c('ecvals','muin','ecvecs','tsa','delst','adjust'))
tsa=decvec
prsout=do.call(cbind,clusterApplyLB(cl,parlist,function(i){
  ctin=i
  fez=fitGP.ez(ctin,muin,ecvals*adjust,t(ecvecs),1e-3)
  fezbd=fitGP.ez(ctin,muin+delst,ecvals*adjust,t(ecvecs),1e-3)
  adjust=sum(log(fez[[7]])-dnorm(fez[[3]],fez[[5]],sqrt(fez[[4]]+fez[[6]]),log=T))
  adjustbd=sum(log(fezbd[[7]])-dnorm(fezbd[[3]],fezbd[[5]],sqrt(fezbd[[4]]+fezbd[[6]]),log=T))
  #
  inva=invapproxsol(fez[[4]],ecvecs,tsa,fez[[3]]-muin)
  k1=-((fez[[3]]-muin)%*%inva)[1,1]/2
  invb=invapproxsol(fezbd[[4]],ecvecs,tsa,fezbd[[3]]-muin-delst)
  k2=-((fezbd[[3]]-muin-delst)%*%invb)[1,1]/2
  d1=sum(log(fez[[4]]))+determinant(diag(nrow(tsa))+tsa%*%(ecvecs/fez[[4]]),logarithm=T)$modulus
  d2=sum(log(fezbd[[4]]))+determinant(diag(nrow(tsa))+tsa%*%(ecvecs/fezbd[[4]]),logarithm=T)$modulus
  list(fez[[1]],fez[[2]],k1-d1/2,k2-d2/2,adjust,adjustbd,fezbd[[1]],fezbd[[2]])
}))
pr1=do.call(c,prsout[3,])-do.call(c,prsout[4,])
pr2=do.call(c,prsout[5,])-do.call(c,prsout[6,])
prh=pr1+pr2#-median(pr1-pr2)
pr3=isoreg(psssub,-prh)
pr4=isoreg(rowSums(datsub),-prh)
pssmu=(psssub-median(psssub))/sd(psssub)*2
#pr=prh-5*pr3$yf[rank(psssub)]#-pr4$yf[rank(rowSums(datsub))]
pr=prh-pssmu
wadj=log(sum(1/(1+exp(pr)))/length(pr))-log(sum(1-1/(1+exp(pr)))/length(pr))
wt=1/(1+exp(pr+2-wadj))#-pr2))
dsum=colSums(datsub*wt)
expsum=colSums(exp(t(t(do.call(rbind,prsout[7,])+do.call(rbind,prsout[8,])/2)-delst))*wt)
par=delst
eps=0.001#*mean(dsum)
delt=Inf
i=1
while(delt>1){
  grs=grfun(dsum,expsum,par,1,1,0)
  delt=(crossprod(grs)/i)
  i=i+1
  par=par+eps/sqrt(i)*grs
}
delst=par
}


out=data.frame(chr=labsub,coord=abs(datmeta[,1]),dir=sign(datmeta[,1]),pssm=datmeta[,3],shape=wt,logbd=log(wt)+log(rowSums(datsub)))
write.csv(out[order(-out$logbd),],file=paste0('output/',day,'-',tfnum,'.csv.gz'))
