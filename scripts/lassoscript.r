#!/usr/bin/Rscript

source('initec2.r')
require('GenomicRanges')

         require("Rcpp")
  require("inline")
  mytabmuST='
    Rcpp::NumericVector cvec1(vec1);
Rcpp::NumericVector cscs(scs);
  Rcpp::NumericVector cmaxval(maxval);
  Rcpp::NumericVector toretsum(cmaxval[0]);
Rcpp::NumericVector toretct(cmaxval[0]);
for(int i =0; i < cvec1.size();i++){
  int a = cvec1[i];
toretsum[a-1]+=cscs[i];
  toretct[a-1]++;
  }
Rcpp::List toret;
toret["sum"]=toretsum;
toret["ct"]=toretct;
  return toret;'
  mytabmu <- cxxfunction(signature(vec1='numeric',scs='numeric',maxval='numeric'),mytabmuST,plugin='Rcpp')

args=as.double(commandArgs(trailingOnly=TRUE))
i=args[1]

load('/dnase/params.RData')
load(file=paste(tmpfile,'lassotmp.RData'))

print(i)

require('Matrix')
require('glmnet')

fos=findOverlaps(flank(llnm[[i]],width=20,both=T),allfactsnm)
rm(allfactsnm);rm(llnm);gc();
   sh=subjectHits(fos)
   gh=c(0,findInterval(unique(queryHits(fos)),queryHits(fos)))
   rm(fos);gc();
   avt=as.double(allvaltyp)
   mss=1:min(50000,(length(gh)-1))
   rld=do.call(rbind,lapply(mss,function(j){
     lm=((gh[j]+1):(gh[j+1]))
     mtm=mytabmu((avt[sh[lm]]),allvald0[sh[lm]],1331)
     cbind((mtm[[1]]/(mtm[[2]]))[which(mtm[[2]]>0)],rep(j,sum(mtm[[2]]>0)),which(mtm[[2]]>0))
   }))
   srld=sparseMatrix(i=rld[,2],j=rld[,3],x=rld[,1],dims=c(max(mss),1331))
   rm(rld);rm(avt);rm(fos);gc()
   yrld=allvald3[[i]][1:max(mss)]
   glout=cv.glmnet(srld,yrld)
   pgl=coef(glout,s='lambda.1se')
   save(glout,file=paste(tmpfile,i,'-larout.RData',sep=''))

