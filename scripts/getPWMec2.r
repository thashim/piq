#####
# Set installer
setwd('/dnase')
source('initec2.r')
load('params.RData')

require('Biostrings')
require('BSgenome')
require('IRanges')

require('MotIV')

if(!exists('prefix')){
  prefix=''
}



#####
# Load motif stuff

## Import function
importJaspar <- function(file=myloc) {
  vec <- readLines(file)
  vec <- gsub("\\[|\\]", "", vec)
  start <- grep(">", vec); end <- grep(">", vec) - 1
  pos <- data.frame(start=start, end=c(end[-1], length(vec)))
  pwm <- sapply(seq(along=pos[,1]), function(x) vec[pos[x,1]:pos[x,2]])
  pwm <- sapply(seq(along=pwm), function(x) strsplit(pwm[[x]], " {1,}"))
  pwm <- sapply(seq(along=start), function(x) matrix(as.numeric(t(as.data.frame(pwm[(pos[x,1]+1):pos[x,2]]))[,-1]), nrow=4, dimnames=list(c("A", "C", "G", "T"), NULL)))
  names(pwm) <- gsub(">", "", vec[start])
  return(pwm)
}
require("MotIV")
pwmtable <- importJaspar(paste(pwmin,"jaspar.txt",sep=''))
rpt=readPWMfile(paste(pwmin,"transfac.txt",sep=''))
pwmtable <- c(pwmtable,rpt)
lgpwm = lapply(pwmtable,function(i){apply(i+0.1,2,function(j){log(j/sum(j))-log(1/4)})})
lgpwm = lapply(pwmtable[sapply(lgpwm,maxScore)>5],function(i){apply(i+1,2,function(j){log(j/sum(j))-log(1/4)})})
tfcut=rep(4,length(lgpwm))
exportAsTransfacFile(lgpwm,'output/pwmused.txt')

maxtf=length(lgpwm)
save(flankbg,multiweight,pwmin,tmpfile,datain,pwmout,kernout,bgout,calltmp,callout,summ,callsum,numdays,maxtf,chrlen,wsize,topnum,maxct,numbp,email,pwmcut,chrstr,nvec,niterbg,niter,maxpertf,flank,exptsub,file='/dnase/params.RData')

######
# Run stuff for real
save(tfcut,lgpwm,file=paste(tmpfile,'commonfiles.RData',sep=''))
save(tfcut,lgpwm,file=paste(pwmout,'commonfiles.RData',sep=''))

cchar=chrstr#c(1:(chrlen-2),"X","Y")
chrlen=length(chrstr)
ctasks=rep('',chrlen)
for(chr in 1:length(cchar)){
  print(chr)
  rcoord=read.csv(paste(datain,cchar[chr],'rcoord.csv',sep=''))[,2:3]
  rng = 1:nrow(rcoord)
  sequences = getSeq(cgenome,paste(cchar[chr],sep=""),rcoord[rng,1]-80,rcoord[rng,2]+80,as.character=F)
  carg=c(seq(1,nrow(rcoord),by=100),(nrow(rcoord)+1))
  blks=lapply(1:(length(carg)-1),function(i){carg[i]:(carg[i+1]-1)})
  for(i in 1:length(blks)){
    print(i)
    if(length(sequences)>1){
      parlist=sequences[blks[[i]]]
    }else{
      parlist=list(sequences)
    }
    save(parlist,file=paste(tmpfile,'infile-',cchar[chr],'-',i,'.RData',sep=''),compression_level=2)
  }
  ctasks[chr]=submitJob('getPWMqscript.r',chr,T,1,length(blks))
}

while(length(system('/opt/sge6/bin/linux-x86/qstat',intern=T))>0){print('waiting for sge to clear');Sys.sleep(60)}
library(mail)
sendmail(email,subject='pwm-step done',message='ec2 finished pwm',password='rmail')

         
