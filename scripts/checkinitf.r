#!/usr/bin/Rscript

setwd('/dnase')
source('initec2.r')

args=as.double(commandArgs(trailingOnly=TRUE))
day=args[1]
tfdelt=args[2]

load('/dnase/params.RData')

i=as.double(Sys.getenv('SGE_TASK_ID'))

if(!file.exists(paste0(callout,i,'-',day,'-diagnostics.pdf'))){
    for(chr in 1:length(chrstr)){
        tryload=class(try(load(paste0(tmpfile,day,'-',chrstr[chr],'-',i,'-factmat.RData')),silent=TRUE))
        if(tryload=='try-error'){
            file.remove(paste0(tmpfile,day,'-',chrstr[chr],'-',i,'-factmat.RData'))
            print(paste0('removed ',paste0(tmpfile,day,'-',chrstr[chr],'-',i,'-factmat.RData')))
            flag=T
        }
    }
}



