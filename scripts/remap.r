#!/usr/bin/Rscript

                                        #setwd('/cluster/cwo/ec2piq/capture.q95/output')

chrlist=readLines('../input/data/chrlist.txt')
exptlist=readLines('../input/data/exptlist.txt')

require('IRanges')

rcl<-lapply(1:length(chrlist),function(chr){
    read.csv(paste0('../input/data/',chrlist[chr],'rcoord.csv'))
})

require('snow')
cl <- makeCluster(16)
clusterExport(cl,c('rcl','chrlist','exptlist'))
clusterCall(cl,function(){require('IRanges')})

gzfilein=list.files(pattern='.compact.csv.gz')

fixind<-function(fn){
    rcv=read.csv(fn)
    rcvn=rcv
    for(chr in 1:length(chrlist)){
        chrmatch=which(rcv[,2]==chr)
        fis=findInterval(rcv$coord[chrmatch]+1,rcl[[chr]][,3])
        min(fis)
        rcvn$coord[chrmatch]=rcv$coord[chrmatch]-rcl[[chr]][fis,3]+rcl[[chr]][fis,2]
    }
    write.csv(rcvn,paste0(strsplit(fn,'.',fixed=T)[[1]][1],'.fix.csv.gz'),row.names=F)
}

clusterApplyLB(cl,gzfilein,fixind)
