old.repos <- getOption("repos") 
on.exit(options(repos = old.repos)) 
new.repos <- old.repos 
new.repos["CRAN"] <- "http://cran.stat.ucla.edu"
options(repos = new.repos)
.libPaths('/dnase/libs/')
#source("http://www.bioconductor.org/biocLite.R")
#require('BiocInstaller',quietly=T)

Sys.setenv(ROOTPATH='/opt/sge6/bin/linux-x64')
if(length(grep('sge',Sys.getenv('PATH')))==0){
    Sys.setenv(PATH=paste0(Sys.getenv('PATH'),':','/opt/sge6/bin/linux-x64'))
}
Sys.setenv(SGE_ROOT='/opt/sge6')
Sys.setenv(SGE_CELL='default')
Sys.setenv(SGE_QMASTER_PORT='63231')


library(mail)
mailme<-function(){sendmail('tatsu23456@gmail.com',subject='kern-step done',message='rdb is done',password='rmail')}

paste0 <- function(...){ paste(...,sep="")}      

#system('/opt/sge6/bin/linux-x86/qconf -rattr queue slots 4 all.q@master')

checkeh<-function(){
    qstatlist= system('/opt/sge6/bin/linux-x86/qstat -f',intern=T)
    numactive=length(grep('all.q',qstatlist,fixed=T))-length(grep('-NA-',qstatlist,fixed=T))
    if(numactive==0){
        #stop('Execution hosts have died - likely spot price too low')
        file.create('/dnase/reqnode')
    }
}

checkq<-function(){
    hasq=TRUE
    while(hasq){
        cat('.');
        Sys.sleep(10);
        #checkeh()
        tryCatch({hasq=(length(system('/opt/sge6/bin/linux-x86/qstat',intern=T))>2)},error=function(e){print(e)})
    };
    #checkeh()
    cat('\n')
}

killNodes<-function(){
  
}

makeBatch<-function(min,max,by){
  carg=c(seq(min,max,by=by),(max+1))
  lapply(1:(length(carg)-1),function(i){carg[i]:(carg[i+1]-1)})
}

maketempRunner<-function(fun){
  prefix='#!/usr/bin/Rscript \n.libPaths(\'/dnase/libs/\') \nload(\'tmp/prereq.RData\')'
  funstr=paste('tempfun<-',paste(format(fun),collapse='\n'),sep='')
  parstr='args=as.double(commandArgs(trailingOnly=TRUE))\nx=args[1]'
  runstr='y<-tempfun(x)'
  savestr='save(y,file=(paste(\'tmp/\',x,\'.RData\',sep=\'\')))'
  pp=paste(prefix,funstr,parstr,runstr,savestr,sep='\n')
  pp
}

tempRunners<-function(fun,parlist,prereq){
  save(prereq,file='tmp/prereq.RData')
  sink('temprunner.r');cat(maketempRunner(fun));sink()
  system('chmod 777 temprunner.r')
  for(i in parlist){
    submitJob('temprunner.r',i)
  }
  sapply(parlist,function(i){
    print(i)
    while(!file.exists(paste('tmp/',i,'.RData',sep=''))){Sys.sleep(1)}
    load(file=paste('tmp/',i,'.RData',sep=''))
    y
  })
}

  submitJob<-function(script,params,grid=F,tst=1,ted=1,tby=1,name=NULL){
    flag=T
    while(flag){
      if(!grid){
        res=system(paste('echo \"./',script,' ',paste(params,collapse=' '),'\" | /opt/sge6/bin/linux-x86/qsub -o out.out -e err.out -wd /dnase -r y',sep=''),intern=T)
      }else{
        res=system(paste('echo \"./',script,' ',paste(params,collapse=' '),'\" | /opt/sge6/bin/linux-x86/qsub -o out.out -e err.out -wd /dnase -r y ',' -t ',tst,'-',ted,':',tby,sep=''),intern=T)
      }
      if(length(grep('has been submitted',res,fixed=T))>0){
        flag=F
      }else{
        print(res)
      }
    }
    res
  }

loadkill<-function(){
  while(TRUE){
    iss=system('ec2din -K pk.pem -C cert.pem  |grep INSTANCE ',intern=T)#|awk {\'print $2\'}',intern=T)
    qstate=system('/opt/sge6/bin/linux-x86/qstat',intern=T)
    inqueue=grep('qw',qstate)
    rqueue=grep('r ',qstate)
    if(length(inqueue)==0 & length(rqueue)>0){
      x1=lapply(iss,strsplit,split='\t')
      nodelis=sapply(x1,function(i){i[[1]][c(2,18)]})
      iplist=read.csv('/etc/hosts',sep=' ',skip=10)
      #
      iplist=iplist[iplist[,2]!='master',]
      #
      loadct=sapply(iplist[,2],function(i){length(grep(i,qstate))})
      shutnode=which(loadct==0)
      matchshut=match(format(iplist[shutnode,1],justify='none'),nodelis[2,])
      shutsel=shutnode[!is.na(matchshut)]
      matchshut=matchshut[!is.na(matchshut)]
      if(length(matchshut)>0){
        print('I WILL KILL THE FOLLOWING NODES!!!')
        print(format(iplist[shutsel,2]))
        Sys.sleep(30)
        killid=nodelis[1,matchshut]
        system(paste('ec2-terminate-instances ',paste(killid,collapse=' '),' -K pk.pem -C cert.pem ',sep=''))
      }
      print(loadct)
    }else{
      print(length(inqueue))
    }
    Sys.sleep(10)
  }
}
