assign=c(rep(0,500),rep(1,500))#rbinom(1000,1,0.5)
#muone=c(-1,-1,-1)
#mutwo=c(1,1,1)
#muone=c(-1,-1)
#mutwo=c(1,1)
muone=rep(-1,3)
mutwo=rep(1,3)
sd=0.1

matin=matrix(rnorm(length(assign)*length(muone),0,sd),length(muone))
matmean=matrix(muone,length(muone),ncol(matin))
matmean[,assign==1]=mutwo

datin=matin+matmean


cov=diag(rep(0.1,length(muone)))+0.1
#cov=diag(rep(0.25,length(muone)))+0.25
mu=rep(0,length(muone))
#
mudat=mu
taudat=0#diag(cov)
#
musl=mu
tausl=rep(0.0001,length(musl))
#
mupri=mu
taupri=1/diag(cov)
#
denslis=apply(datin,1,density,bw=0.1,from=-3,to=3,n=2000)
densfun=sapply(denslis,function(i){approxfun(i$x,i$y+1e-20,rule=2)})
intpt=seq(-3,3,length=3000)

dlog=sapply(densfun,function(i){log(i(intpt))})
dlog=dlog-log(sum(exp(dlog[,1])))

sigstab=diag(rep(sum(diag(cov))/ncol(cov),ncol(cov)))*10
sigrem=solve(cov)-solve(sigstab)

getstab<-function(muin,sigin){
  sapply(1:ncol(dlog),function(i){
    lprvecx=-0.5*(intpt-muin)^2/sigin+dlog[,i]-log(sigin)*0.5-0.5*log(2*pi)
    prvecx=exp(lprvecx-max(lprvecx))
    newmux=sum(intpt*prvecx)/sum(prvecx)
    newvarx=sum(intpt^2*prvecx)/sum(prvecx)-newmux^2
    opr=(-0.5*(intpt-muin)^2/sigin-log(sigin)*0.5-0.5*log(2*pi))
    lrat=dlog[,i]-(opr-log(sum(exp(opr))))
    kld=sum(lrat*exp(dlog[,i]))/sum(exp(dlog[,i]))
    kld2=sum(-lrat*exp(opr))/sum(exp(opr))
    c(newmux,newvarx,kld2,sum(exp(lprvecx)))
  })
}

getstab2<-function(muin,sigin){
    lprvecx=-0.5*(intpt-muin)^2/sigin+rowSums(dlog)-log(sigin)*0.5-0.5*log(2*pi)
    prvecx=exp(lprvecx-max(lprvecx))
    newmux=sum(intpt*prvecx)/sum(prvecx)
    newvarx=sum(intpt^2*prvecx)/sum(prvecx)-newmux^2
    c(newmux,newvarx)
}

prestab=getstab(mu[1],eigen(cov)$values[1])

svs=seq(0.000001,1.5,length=3000)
dset=sapply(svs,function(i){
  print(i)
  sigstab=diag(rep(i,ncol(cov)))
  sigrem=solve(cov)-solve(sigstab)
  srrem=solve(sigrem)
  dorig=-0.5*determinant(sigstab+srrem)$modulus
  stabout=getstab(mu[1],sigstab[1,1])
  detpart=-0.5*determinant(diag(stabout[2,])+solve(sigrem))$modulus-dorig
  #fullp=sum(log(stabout[4,]))+detpart
  c(diag(solve(diag(1/stabout[2,])+sigrem)),stabout[2,])
  #c(1/stabout[2,]-1/i)
})

diag(solve(diag(1/stabout[2,])+sigrem))

cnot=1/(solve(cov)[1,1])

cnot=1
plot(svs,dset[1,],type='l',ylim=c(-1,1))
varall=dset[,which.min(abs(cnot-svs))]
(1/varall[1:3]-1/0.2)-(1/varall[4:6]-1/cnot)
precdiff=1/varall[1:3]-1/0.2
#posterior variance (varall)
nprec=1/(1/varall[1:3]-(1/varall[4:6]-cnot))
#pick a sigma such that KL (sigmastab+prior(marginal) -> posterior)


pradd=(1/dset[4,]-1/svs)
cnot=1/((1/dset[1,]-pradd)[which.min(abs(cnot-svs))])
posind=which.min(abs(dset[1,]-varall[1]))
svs[posind]
cnot=posind
1/varall[1:3]-1/diag(cov)



vprim=eigen(cov)$values[1]*sqrt(ncol(cov))

vs2=sapply(seq(0.01,10,length=1000),function(i){
  gs2=getstab2(mu[1],i)
  c(gs2,1/gs2[2]-1/i)
})
which.min(abs(vs2[3,]))

gs2=getstab2(mu[1],vprim)

tss=ncol(cov)*(1/dset[4,]-1/svs)-(1/gs2[2]-1/vprim)
plot(svs,tss,ylim=c(-1,1))
svs[which.min(abs(tss))]
getstab(mu[1],svs[which.min(abs(tss))])[2,]

marprec=dset[4,]-1/svs
plot(marprec,ylim=c(-10,0),type='l');abline(h=addprec*3)

plot(svs,dset[4,]-1/svs,ylim=c(-3,0))
points(svs,1/0.4-1/svs,ylim=c(-3,0),col='red')

plot(svs,dset[4,]-1/svs+1/0.4,ylim=c(-3,0))

par(mfrow=c(3,1));plot(svs,dset[4,],type='l');plot(svs,dset[1,],type='l',ylim=c(-2,2))
abline(v=0.4)
abline(h=0.93)
plot(svs,1/dset[4,]-1/svs+1/0.4,ylim=c(-3,0))
abline(h=0)

wmind=which.min(abs(1/dset[4,]-1/svs+1/0.4))
c(svs[wmind],dset[4,wmind])
c(svs[wmind],dset[1,wmind])

dset[2,which.max(dset[5,])]
dset[1,which.max(dset[5,])]

dset[1,which.max(dset[5,])]

####

eps=0.001

tausl=rep(1,length(muone))

scov=solve(cov)

object=function(x,y,z){
  sum((diag(solve(diag(x)+z))-y)^2)
}

trymap=function(muin,sigin){
  lprvecx=-0.5*(intpt-muin)^2/sigin+log(densfun[[1]](intpt))
  prvecx=exp(lprvecx-max(lprvecx))
  newmux=sum(intpt*prvecx)/sum(prvecx)
  newvarx=sum(intpt^2*prvecx)/sum(prvecx)-newmux^2
  #
  #
  #
  #newgradmu=sum(dlprvec*intpt)/sum(prvecx)-sum(intpt*prvecx)/sum(prvecx)^2*sum(dlprvec)
  #dlprvectwo=-1/sigin+((muin-intpt)/sigin)^2
  #newtgradmu=sum(dlprvectwo*prvecx)/sum(prvecx)
  dlprvec=-(muin-intpt)/sigin*prvecx
  a=sum(prvecx)
  b=sum(dlprvec)
  ci=sum(dlprvec*intpt)
  dlprvectwo=(-1/sigin+((muin-intpt)/sigin)^2)*prvecx
  d=sum(dlprvectwo*intpt)
  e=sum(dlprvectwo)
  newgradmu=ci/a-b/a*newmux
  newtgradmu=d/a-ci*b/a^2-newmux*e/a-ci*b/a^2+2*b^2*newmux/a^2
  c(newmux,newvarx,newgradmu,newtgradmu)
}

tes=sapply(seq(-1,1,length=100),function(i){trymap(i,0.1)})
plot(seq(-1,1,length=100),tes[1,],type='l')

iloc=0.5
abline(v=iloc)
beta=trymap(iloc,0.1)[3]
ofs=trymap(iloc,0.1)[1]-iloc*beta
abline(ofs,beta)
xin2=seq(-1,1,length=100)
cv=trymap(iloc,0.1)[4]/2
points(xin2,trymap(iloc,0.1)[1]+beta*(xin2-iloc)+cv*(xin2-iloc)^2)



plot(seq(-1,1,length=100),tes[2,],type='l')

tes2=sapply(seq(0.01,1,length=100),function(i){trymap(0,i)})

plot(1/seq(0.01,1,length=100),tes2[2,],type='l')

plot(1/seq(0.01,1,length=100),tes2[1,],type='l')

#acl=apply(datin,1,function(i){logConDens(i,gam=0.05,xs=intpt)})
#aclfun=sapply(acl,function(i){approxfun(i$xs,i$f.smoothed,rule=2)})
for(i in 1:50){
  taucav=taupri
  mucav=mupri
  newev=sapply(1:nrow(datin),function(i){
    lprvecx=-0.5*(intpt-mucav[i])^2*taucav[i]+log(densfun[[i]](intpt))
    prvecx=exp(lprvecx-max(lprvecx))
    newmux=sum(intpt*prvecx)/sum(prvecx)
    newvarx=sum(intpt^2*prvecx)/sum(prvecx)-newmux^2
    c(newmux,newvarx)
  })
  scov=solve(cov)
  indout=newuoa(diag(cov),object,y=newev[2,],z=scov)
  #
  taudat=indout$par
  postprec=diag(taudat)+solve(cov)
  mudat=as.vector((newev[1,]%*%postprec-mu%*%scov)/taudat)
  postcov=solve(diag(taudat)+solve(cov))
  postmu=postcov%*%(mudat*taudat+solve(cov)%*%mu)
  taupri=1/diag(postcov)-taudat
  mupri=(postmu/diag(postcov)-mudat*taudat)/taupri
  print(paste0('it',i))
  print(postmu)
  print(postcov)
}


for(i in 1:50){
  taucav=taupri
  mucav=mupri
  newev=sapply(1:nrow(datin),function(i){
    lprvecx=-0.5*(intpt-mucav[i])^2*taucav[i]+log(densfun[[i]](intpt))
    prvecx=exp(lprvecx-max(lprvecx))
    newmux=sum(intpt*prvecx)/sum(prvecx)
    newvarx=sum(intpt^2*prvecx)/sum(prvecx)-newmux^2
    c(newmux,newvarx)
  })
  newcov=??
  #evcov=solve(diag(1/newev[2,])-scov)
  #evmu=(newev[1,]/newev[2,]-mu%*%scov)%*%solve(evcov)
  #postcov=diag(newev[2,])#solve(solve(evcov)+solve(cov))
  #postmu=newev[1,]#postcov%*%(mudat*taudat+solve(cov)%*%mu)
  #newuoa(diag(postcov),object,y=diag(postcov),z=diag(1/newev[2,]))
  #taupri=1/diag(postcov)-1/diag(evcov[2,])
  #mupri=(postmu/diag(postcov)-mudat*taudat)/taupri
  scov=solve(cov)
  indout=newuoa(diag(cov),object,y=newev[2,],z=scov)#  allcovs=sapply(xlis,function(i){
   # diag(solve(diag(1/rep(i,3))+scov))
  #})
  taudat=indout$par#1/xlis[indset]
  #plot(allcovs[1,],col='red',type='l',ylim=c(-5,5));abline(h=newev[2,1]);abline(v=3000);a+bline(h=diag(cov))
  #dgg=diag(1/newev[2,])-solve(cov)
  #taudat=diag(dgg)
  #taudat=1/newev[2,]-taucav
  postprec=diag(taudat)+solve(cov)
  mudat=as.vector((newev[1,]%*%postprec-mu%*%scov)/taudat)
  #mudat=(newev[1,]/newev[2,]-mucav*taucav)/taudat
#
#taudat[taudat< -eccov]= -eccov
#
  postcov=solve(diag(taudat)+solve(cov))
  postmu=postcov%*%(mudat*taudat+solve(cov)%*%mu)
  taupri=1/diag(postcov)-taudat
  #mupri=(postmu/diag(postcov)-mudat*taudat)/taupri
  mupri=(postprec%*%postprec-mudat*taudat)/taupri
  #taupri[taupri<0.001]=0.001
  print(paste0('iter',i))
  print(taudat)
  print(tausl)
  print(taupri)
  print(mupri)  
}

require('minqa')

for(i in 1:50){
  taucav=taupri
  mucav=mupri
  newev=sapply(1:nrow(datin),function(i){
    lprvecx=-0.5*(intpt-mucav[i])^2*taucav[i]+log(densfun[[i]](intpt))#*dnorm(intpt,mucav[i],1/sqrt(taucav[i]))*dnorm(intpt,musl[i],1/sqrt(tausl[i]))
    prvecx=exp(lprvecx-max(lprvecx))
    newmux=sum(intpt*prvecx)/sum(prvecx)
    newvarx=sum(intpt^2*prvecx)/sum(prvecx)-newmux^2
    c(newmux,newvarx)
  })
  #evcov=solve(diag(1/newev[2,])-scov)
  #evmu=(newev[1,]/newev[2,]-mu%*%scov)%*%solve(evcov)
  #postcov=diag(newev[2,])#solve(solve(evcov)+solve(cov))
  #postmu=newev[1,]#postcov%*%(mudat*taudat+solve(cov)%*%mu)
  #newuoa(diag(postcov),object,y=diag(postcov),z=diag(1/newev[2,]))
  #taupri=1/diag(postcov)-1/diag(evcov[2,])
  #mupri=(postmu/diag(postcov)-mudat*taudat)/taupri
  scov=solve(cov)
  indout=newuoa(diag(cov),object,y=newev[2,],z=scov)#  allcovs=sapply(xlis,function(i){
   # diag(solve(diag(1/rep(i,3))+scov))
  #})
  taudat=indout$par#1/xlis[indset]
  #plot(allcovs[1,],col='red',type='l',ylim=c(-5,5));abline(h=newev[2,1]);abline(v=3000);a+bline(h=diag(cov))
  #dgg=diag(1/newev[2,])-solve(cov)
  #taudat=diag(dgg)
  #taudat=1/newev[2,]-taucav
  postprec=diag(taudat)+solve(cov)
  mudat=as.vector((newev[1,]%*%postprec-mu%*%scov)/taudat)
  #mudat=(newev[1,]/newev[2,]-mucav*taucav)/taudat
#
#taudat[taudat< -eccov]= -eccov
#
  postcov=solve(diag(taudat)+solve(cov))
  postmu=postcov%*%(mudat*taudat+solve(cov)%*%mu)
  taupri=1/diag(postcov)-taudat
  #mupri=(postmu/diag(postcov)-mudat*taudat)/taupri
  mupri=(postprec%*%postprec-mudat*taudat)/taupri
  #taupri[taupri<0.001]=0.001
  print(paste0('iter',i))
  print(taudat)
  print(tausl)
  print(taupri)
  print(mupri)  
}

postcov

1/(taudat+taupri)






for(i in 1:50){
taucav=taupri-taudat
mucav=(mupri*taupri-mudat*taudat)/taucav
prvecx=dfunx(intpt)*dnorm(intpt,mucav[1],1/sqrt(taucav[1]))
newmux=sum(intpt*prvecx)/sum(prvecx)
newvarx=sum(intpt^2*prvecx)/sum(prvecx)-newmux^2
prvecy=dfuny(intpt)*dnorm(intpt,mucav[2],1/sqrt(taucav[2]))
newmuy=sum(intpt*prvecy)/sum(prvecy)
newvary=sum(intpt^2*prvecy)/sum(prvecy)-newmuy^2
taudat=1/c(newvarx,newvary)-taucav
mudat=(c(newmux,newmuy)/c(newvarx,newvary)-mucav*taucav)/taudat
print(c(newvarx,newvary))
#
postcov=solve(diag(taudat)+solve(cov))
postmu=postcov%*%(mudat*taudat+solve(cov)%*%mu)
taupri=1/diag(postcov)-taudat
mupri=(postmu/diag(postcov)-mudat*taudat)/taupri
#
print(taudat)
}



require('mvtnorm')

ds=lapply(densfun,function(i){i(intpt)})

nsamp=10000000

samples=do.call(cbind,lapply(1:length(densfun),function(i){sample(1:length(intpt),nsamp,replace=T,prob=exp(dlog[,i]))}))
samppt=apply(samples,2,function(i){intpt[i]})
#prset=Reduce('*',lapply(1:length(ds),function(i){ds[[i]][samples[,i]]}))*
prset=dmvnorm(samppt,mu,cov)

musamp=colSums(prset*samppt)/sum(prset)
varsamp=colSums(prset*samppt^2)/sum(prset)-musamp^2
musamp;varsamp


#####
# Log-pois

sz=50
Covtrue = 2*exp(-outer(1:sz,1:sz,'-')^2/(100^2))+diag(rep(1e-2,sz))
evv=eigen(Covtrue)
vars=evv$values
rotation=evv$vectors

mutrue=rep(0,nrow(Covtrue))
muoffs=-4

snum=20000
irep=t(sapply(1:snum,function(i){rnorm(nrow(Covtrue),mutrue,sqrt(vars))}))
simdatmu=irep%*%t(rotation)+muoffs

#plot(coefficients(los,s=800),type='l')
#lmf=lm.fit(simdatmu[,-1],simdatmu[,1])
#plot(lmf$coefficients,type='l')

rpp=rpois(length(simdatmu),as.vector(exp(simdatmu)))
simdat=matrix(rpp,nrow(simdatmu),ncol(simdatmu))
simdat=simdat[rowSums(simdat)>0,]

stabval=4

require('gsl')
getLapLocSV<-function(mu,sig,d,cutv=20){
  l=rep(0,length(d))
  l[d<cutv]=d[d<cutv]*sig+mu-lambert_W0(exp((d[d<cutv]*sig+mu))*sig)
  l[d>cutv]=(log(d[d>cutv])*d[d>cutv]+mu/sig)/(d[d>cutv]+1/sig)
  sh=1/(exp(l)+1/sig)
  pr=sqrt(2*pi*sh)*dpois(d,exp(l))*dnorm(l,mu,sqrt(sig))
  list(l,sh,pr)
}

require('statmod')
gknots = gauss.quad(9000,kind="hermite")
poiscomp <- function(mu,sig,dat){
  t=mu+sqrt(2*sig)*gknots$nodes
  dp=dpois(dat,exp(t))*gknots$weights/sqrt(pi)
  evs=dp%*%cbind(1,t,t^2)
  un=evs[2]/evs[1]
  vn=evs[3]/evs[1]-evs[2]^2/evs[1]^2
  c(un,vn,evs[1])
}

maxval=max(simdat)

maxcut=10

llmu=sapply(0:maxval,function(i){
  if(i<maxcut){
    v=poiscomp(muoffs,stabval,i)
    (v[1]/v[2]-muoffs/stabval)/(1/v[2]-1/stabval)
  }else{
    (log(i)*i+muoffs/stabval)/(i+1/stabval)
    #log(i)
  }
})
llprec=sapply(0:maxval,function(i){
  if(i<maxcut){
    1/poiscomp(muoffs,stabval,i)[2]-1/stabval
  }else{
    me=(log(i)*i+muoffs/stabval)/(i+1/stabval)
    exp(me)-1/stabval
  }
})
#llprec=(1/sapply(0:maxval,function(i){poiscomp(muoffs,stabval,i)})[2,]-1/stabval)
ll=llmu[simdat+1]
dim(ll)=dim(simdat)

llu=llprec[simdat+1]
dim(llu)=dim(simdat)

covin=cov(ll)
sci=solve(covin)
mupri=rep(muoffs,sz)%*%sci

#ll=apply(simdat,1,function(i){getLapLocSV(muoffs,stabval,i,cutv=5)[[1]]})

minrat <- function(x){
  max(x)/min(x)
}

grs <- function(x,precb,b,precon,A,Am){
  (Am%*%x)-precb
}

funs <- function(x,precb,b,precon,A,Am){
  #x%*%(precon%*%(A%*%x)/2-precb)
  #(x%*%A%*%x)/2-b%*%x
  x%*%(Am%*%x-precb)
}

func <- function(x,precb,b,precon,A,Am){
  gh=(Am%*%x)-precb
  ret=x%*%gh
  attr(ret,'gradient')=gh
  ret
}

scid=solve(sci+diag(rep(llprec[1],sz)))

par(mfrow=c(2,2))
for(g in 1:50){
covsum=matrix(0,ncol(covin),ncol(covin))
muall=matrix(0,nrow(simdat),ncol(simdat))
for(j in 1:nrow(simdat)){
  #print(j)
  at<-Sys.time()
  datin=simdat[j,]
  datind=which(datin>0)
  if(length(datind)>0){
  sigadjust=sigadjusts[datin[datind]+1]
  #
  dss=scid[datind,datind,drop=F]*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  #
  sccm=t(scid[datind,,drop=F])
  gh=(solve(dss+diag(length(datind))))
  cminternal=(solve(dss+diag(length(datind))))*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  a1=sccm%*%cminternal%*%t(sccm)
  allinv=(scid-a1)
  vh=as.vector(ll[j,]*llu[j,]+mupri)
  uinv=scid%*%vh-sccm%*%(cminternal%*%(t(sccm)%*%vh))
  #dinv=diag(scid)-rowSums((sccm^2)%*%cminternal)
  #print(Sys.time()-at)
  muall[j,]=uinv
  covsum=covsum+allinv
  }
}
cch=cov(muall)
cpost=covsum/nrow(simdat)+cch
#
bandavg=sapply(1:nrow(cpost),function(i){
  ctsm=0;
  num=0;
  offs=i-1
  for(j in 1:(nrow(cpost)-offs)){
    ctsm=ctsm+cpost[j,j+offs]
    num=num+1
  }
  ctsm/num
})
#
#
print(eigen(covin,only.values=T)$values[1:10])
plot(covsum[1,]/nrow(simdat)+cch[1,],type='l',ylim=range(covin[1,]))
points(covin[1,],type='l',col='red')
points(Covtrue[1,],type='l',col='green')
points(bandavg,type='l',col='blue')
#
covin=covsum/nrow(simdat)+cch
ecc=eigen(covin)
ecv=ecc$values
ecv=ecv-0.5
ecv[ecv<0.1]=0.1
covin=ecc$vectors%*%diag(ecv)%*%t(ecc$vectors)
sci=solve(covin)
mupri=rep(muoffs,sz)%*%sci
scid=solve(sci+diag(rep(llprec[1],sz)))
#
plot(muall[j,],type='l');rug(which(simdat[j,]>0))
points(simdatmu[j,],col='red',type='l')
}

source('gpmodelCommon.R')

ecin=eigen(covin)
gknots = gauss.quad(15,kind="hermite")
for(g in 1:10){
  ecvec=ecin$vectors
  decvec=diag(ecin$values)%*%t(ecin$vectors)
  sparsebt=ecin$vectors%*%decvec%*%ecvec
epfit=lapply(1:nrow(simdat),function(i){
  fez=fitGP.ez(simdat[i,],rep(muoffs,sz),ecin$values,t(ecvec),1e-5)
  ias=invapproxsol(fez[[4]],ecvec,decvec,sparsebt)
  fcd=t(ecvec*fez[[4]])%*%ias
  list(fez[[1]],fcd)
})
allmu=do.call(cbind,lapply(epfit,function(i){i[[1]]}))
covmu=cov(t(allmu))
  covsig=ecvec%*%(Reduce('+',lapply(epfit,function(i){i[[2]]}))/length(epfit))%*%t(ecvec)
#allsig=do.call(cbind,lapply(epfit,function(i){i[[2]]}))
#covsig=ecin$vectors%*%diag(rowMeans(allsig))%*%t(ecin$vectors)
newcov=covmu+covsig
ecin=eigen(newcov)
print((ecin$values)[1:10])
}

epfit[[1]]
plot(epfit[[1]][[1]],ylim=c(-5,3));points(simdatmu[10,],col='red')


plot(covin[1,],col='blue',ylim=c(min(newcov),max(covin)))
points(newcov[1,])
points(Covtrue[1,],col='red')

#smf <- function(A,c,v){
#  A[
#}

fftmult <- function(A,v){
  x=A[1,]
  clv=c(x,0,rev(x[-1]))
  f=fft(c(as.vector(v),rep(0,length(v))))/length(clv)
  g=fft(clv)
  as.double(fft(g*f,inverse=T)[1:length(x)])
}

grsfft <- function(x,precb,b,precon,A,Am){
  #x+fftmult(precon,x*A)-precb
  (Am%*%x)-precb
}

funsfft <- function(x,precb,b,precon,A,Am){
  (x+fftmult(precon,x*A)-precb)
  #(x%*%x)/2+(x%*%fftmult(precon,x*A))/2-x%*%precb
  #x%*%(precon%*%(A%*%x)/2-precb)
  #crossprod(Am%*%x-precb)
  #(x%*%A%*%x)/2-x%*%b
}


drm=rowMeans(exp(ll))
precon=covin#solve(sci+diag(rowMeans(exp(ll))))

#precon=diag(sz)

pccin=precon%*%sci
for(i in 1:10){
b=(as.vector(mupri)+ll[,3]*exp(ll[,3]))
#a=(sci+diag(exp(ll[,3])))
a=exp(ll[,3])
am=pccin+t(t(precon)*exp(ll[,3]))
ooi=as.vector((ll[,3]*drm+mupri)%*%precon)
#ooi=as.vector(mupri)
at<-Sys.time()
oop=ooi
ooy=ooi
a=1
for(i in 1:200){
grd=grsfft(ooy,precon%*%b,b,precon,a,am)
oopprev=oop
oop=ooy-grd*(0.00002)
an=(1+sqrt(4*a^2+1))/2
ooy=oop+(a-1)/an*(oop-oopprev)
a=an
plot(ooy)
}
a=(Sys.time()-at)
plot(oop)
plot(oo2[1,])
oo=optim(ooi,funsfft,gr=grsfft,precb=precon%*%b,precon=precon,A=a,Am=am,b=b,method='CG',control=list(maxit=1000,type=2))
#funsfft(oo$par,precon%*%b,b,precon,a,am)
#funsfft(oo2[1,],precon%*%b,b,precon,a,am)
#oo=ucminf(ooi,funsfft,gr=grsfft,precb=precon%*%b,precon=precon,A=a,Am=am,b=b,control=list(grtol=0.001))
#oo=nlm(func,ooi,precb=precon%*%b,precon=precon,A=a,Am=am,b=b,check.analyticals=F)
at<-Sys.time()
oo2=(mupri+ll[,3]*(exp(ll[,3])))%*%solve(sci+diag(exp(ll[,3])))
b=(Sys.time()-at)
print(c(a,b))
}

oo$counts
plot(oo$par[1,])


multimin(i,

hh=apply(ll,2,function(i){(mupri+i*(exp(i)))%*%solve(sci+diag(exp(i)))})

i=ll[,3]
newpcov=solve(solve(covcav)+diag(exp(i)+1/stabval))
newu=newpcov%*%as.vector(mupri+i*(exp(i)+1/stabval))

ecvv=ecin$vectors

projmu=i%*%ecvv
projsig=(1/(exp(i)))%*%(ecvv^2)

primu=rep(muoffs,sz)%*%ecvv
prisig=diag(t(ecvv)%*%covin%*%ecvv)

postvals=(projmu/projsig+primu/prisig)/(1/projsig+1/prisig)
plot(as.vector(postvals%*%t(ecvv)))

plot(newu[1,])

plot(simdatmu[3,])
plot(newu[1,])



rowMeans(ll)
