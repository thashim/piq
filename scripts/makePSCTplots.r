#!/usr/bin/Rscript

source('initec2.r')
require("Rcpp")
require("inline")
require('KernSmooth')

args=as.double(commandArgs(trailingOnly=TRUE))
j=args[1]

pdf(paste('summaries/psct-',j,'.pdf',sep=''))
load(paste('callout/allfact-',j,'.RData',sep=''))
allcalls=sapply(0:8,function(ofs){
  bdun=isoreg(ccmat[,23+ofs],ccmat[,5+ofs])$yf[rank(ccmat[,23+ofs])]
  psun=isoreg(ccmat[,4],ccmat[,5+ofs])$yf[rank(ccmat[,4])]+(ccmat[,4]-min(ccmat[,4]))*0.5
  #gg=(ccmat[,5+ofs]/5+bdun+psun)/4
  ccmat[,5+ofs]
})
for(ofs in 0:8){
ir=isoreg(ccmat[,4],allcalls[,1+ofs])
ker=bkde2D(cbind(ccmat[,4],allcalls[,1+ofs]),c(0.1,0.3),gridsize=c(200,200),range.x=list(range(ccmat[,4]),range(allcalls[,1+ofs])))
image(ker$x1,ker$x2,log(ker$fhat+1e-6),xlab='pssm',ylab='binding')#,xlim=range(ker$x1),ylim=range(ker$x2))
points(sort(ccmat[,4]),ir$yf,pch='.',type='l',lwd=2)
}
for(ofs in 0:8){
  ir=isoreg(ccmat[,23+ofs],allcalls[,1+ofs])
  ker=bkde2D(cbind(ccmat[,23+ofs],allcalls[,1+ofs]),c(0.1,0.3),gridsize=c(200,200),range.x=list(range(ccmat[,23+ofs]),range(allcalls[,1+ofs])))
  image(ker$x1,ker$x2,log(ker$fhat+1e-6),xlab='counts',ylab='binding')#,xlim=range(ker$x1),ylim=range(ker$x2))
  points(sort(ccmat[,23+ofs]),ir$yf,pch='.',type='l',lwd=2)
}
dev.off()