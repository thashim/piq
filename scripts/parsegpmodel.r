#!/usr/bin/Rscript

source('initec2.r')
require("Rcpp",quietly=T)
require("inline",quietly=T)

args=as.double(commandArgs(trailingOnly=TRUE))
day=args[1]
tfdelt=args[2]

load('/dnase/params.RData')

tfstart=as.double(Sys.getenv('SGE_TASK_ID'))
tfend=min(tfstart+tfdelt-1,maxtf)

print(c(tfstart,tfend,day,tfdelt))


blocksz=100
tfmax=maxtf

mlss=lapply(1:chrlen,function(chr){
  print(chr)
  lf=list.files(pwmout,pattern=paste0(chrstr[chr],'-'))
  if(length(lf)>0){
  batches=makeBatch(1,length(lf),by=20)
  lchr=do.call(c,lapply(1:length(batches),function(bns){
    spos=min(batches[[bns]])
    #readLines(paste0(calltmp,0,'/',chr,'-',spos,'-',tfstart,'.out.gz'))
    load(paste0(calltmp,0,'/',chrstr[chr],'-',spos,'-',tfstart,'.out'))
    str
  }))
  gc()
  lchr
  }else{
    NULL
  }
})
mlsr=lapply(1:(tfend-tfstart+1),function(foffs){
  lapply(1:chrlen,function(chr){
    if(!is.null(mlss[[chr]])){
      tseq=seq(foffs,length(mlss[[chr]]),by=(tfend-tfstart+1))
      sss=strsplit(mlss[[chr]][tseq],'[;,]')
      rcoord=read.csv(paste(datain,chrstr[chr],'rcoord.csv',sep=''))[,2:3]
      do.call(c,lapply(1:length(sss),function(i){
        mg=as.double(sss[[i]][-c(1,which(nchar(sss[[i]])==0))])
        if(length(mg)>0){
          rcint=100*(i-1)
          x1=mg[seq(1,length(mg),by=3)]
          x2=mg[seq(2,length(mg),by=3)]
          x3=mg[seq(3,length(mg),by=3)]
          x4=(rcoord[rcint+x1,1]+abs(x2))*sign(x2)
          as.vector(rbind(chr,i,x1,x2,x3,x4))
        }else{
          mg
        }
      }))
    }else{
      integer(0)
    }
  })
})
rm(mlss);gc()

clss=lapply(1:chrlen,function(chr){
  print(chr)
  lf=list.files(pwmout,pattern=paste0(chrstr[chr],'-'))
  if(length(lf)>0){
    batches=makeBatch(1,length(lf),by=20)
    lchr=do.call(c,lapply(1:length(batches),function(bns){
      spos=min(batches[[bns]])
      load(paste0(calltmp,day,'/',chrstr[chr],'-',spos,'-',tfstart,'.out'))
      str
    }))
    gc()
    lchr
  }else{
    NULL
  }
})
clsr=lapply(1:(tfend-tfstart+1),function(foffs){
  lapply(1:chrlen,function(chr){
    if(!is.null(clss[[chr]])){
      tseq=seq(foffs,length(clss[[chr]]),by=(tfend-tfstart+1))
      do.call(c,lapply(strsplit(clss[[chr]][tseq],'[;,]'),function(i){as.double(i[-c(1,which(nchar(i)==0))])}))
    }else{
      integer(0)
    }
  })
})
rm(clss);gc()



tfsubs=tfstart:tfend
for(tfnum in 1:length(tfsubs)){
  mlss=do.call(c,mlsr[[tfnum]])
  clss=do.call(c,clsr[[tfnum]])
  mlsr[[tfnum]]<- list(NULL)
  clsr[[tfnum]]<- list(NULL)
  gc()
  dim(mlss)=c(6,length(mlss)/6)
  dim(clss)=c(2,length(clss)/2)
  #
  picall=clss[1,]-median(clss[1,])#-3
  ctir=isoreg(clss[2,],picall)
  psir=isoreg(mlss[5,],picall)
  #
  picmarg=picall+ctir$yf[rank(clss[2,])]*0.2+psir$yf[rank(mlss[5,])]*5+(mlss[5,]-median(mlss[5,]))*0.2-3
  outmat=cbind(t(mlss[1:6,]),signif(picmarg,digits=4),signif(picall,digits=4))
  #
  mct=min(max(sum(picmarg>0.1),10000),floor(nrow(outmat)/2))
  opm=order(picmarg)
  oup=outmat[rev(opm)[1:mct],]
  odn=outmat[opm[1:mct],]
  gzfc=gzfile(paste0(callout,day,'/',tfsubs[tfnum],'.tfout.gz'),open='wb')
  write.table(rbind(oup),file=gzfc,row.names=F,col.names=F,sep=',')
  close(gzfc)
  gzfc=gzfile(paste0(callout,day,'/',tfsubs[tfnum],'.tfout.control.gz'),open='wb')
  write.table(rbind(odn),file=gzfc,row.names=F,col.names=F,sep=',')
  close(gzfc)
}
