setwd('/dnase')
source('initec2.r')

require("gsl")
lambseq=seq(-40,40,length=10^6)
lamlookup=lambert_W0(exp(lambseq))
lambdiff=lambseq[2]-lambseq[1]

require("statmod")
gknots = gauss.quad(15,kind="hermite")
nruns=c(1e-2,40)

tfmax=722
numdays=9

load('kernout/kernreduced.RData')
lz=(creduce$d/20)[1:30]
tsb=t(creduce$u)[1:30,]

expv=colSums(tsb^2*creduce$d)
truev=do.call(c,lapply(1:numdays,function(i){rep(marginals[2,i],2000)}))
vadj=truev-expv

lmupri=as.vector(tsb%*%do.call(c,lapply(1:numdays,function(i){rep(marginals[1,i],2000)})))[1:30]

load('kernout/profs.RData')
kmatSS=lapply(1:9,function(i){profs[[i]]$kmatSS})
kmatOS=lapply(1:9,function(i){profs[[i]]$kmatOS})

save(lz,tsb,vadj,lmupri,kmatSS,kmatOS,tfmax,numdays,nruns,lambseq,lamlookup,lambdiff,gknots,file='tmp/callcommon.RData',compress='bzip2')

for(chr in 1:21){
  lf=list.files('pwmout',pattern=paste('-',chr,'-',sep=''))
  batches=makeBatch(1,length(lf),by=2)
  for(i in 1:length(batches)){
    submitJob('gpmodelqscript.r',c(chr,9,min(batches[[i]]),max(batches[[i]])))
  }
}

while(length(system('/opt/sge6/bin/linux-x86/qstat',intern=T))>2){print('waiting for sge to clear');Sys.sleep(10)}
iss=system('ec2din -K pk.pem -C cert.pem  |grep INSTANCE |awk {\'print $2\'}',intern=T)
#system(paste('ec2-terminate-instances ',paste(iss[-1],collapse=' '),' -K pk.pem -C cert.pem ',sep=''))


#pin
#lz
#vadj
#tsb
#gknots
#lambseq
#lamlookup