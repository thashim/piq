#!/usr/bin/Rscript
setwd('/dnase')
.libPaths('/dnase/libs/')

require("Rcpp",quietly=T)
require("inline",quietly=T)

decodeRLE <- function(str,nrows=9){
  ss=strsplit(str,';',fixed=T)[[1]]
  ssi=strsplit(ss,',',fixed=T)
  rlein=list()
  rlein$lengths=as.integer(ssi[[1]])
  rlein$values=as.integer(ssi[[2]])
  class(rlein)='rle'
  rlevec=matrix(inverse.rle(rlein),nrow=nrows*2)
}

convKernST = '
Rcpp::NumericVector cdeltaPOS(deltaPOS);
Rcpp::NumericVector cdeltaNEG(deltaNEG);
Rcpp::NumericVector ctfid(tfid);
Rcpp::NumericVector cblocus(blocus);
Rcpp::NumericVector coffset(offset);
Rcpp::NumericMatrix cprofvec(profvec);
Rcpp::NumericMatrix csigvec(sigvec);
Rcpp::NumericVector ctct(tct);
for(int i =0; i < cblocus.size(); i++){
int locus = abs(cblocus[i])-1+coffset[0];
int halfwidth = (cprofvec.nrow()-2)/4;
int startpt = std::max(0,locus-halfwidth);
int kstartpt = std::max(0,halfwidth-locus);
int endpt = std::min(((int)cdeltaPOS.size())-1,locus+halfwidth);
int ktype = (ctfid[i]-1);
ctct[ktype]++;
Rcpp::NumericVector SSmat = cdeltaPOS;
Rcpp::NumericVector OSmat = cdeltaNEG;
if(cblocus[i]<0){
SSmat=cdeltaNEG;
OSmat=cdeltaPOS;
}
for(int j=0; j<=endpt-startpt;j++){
//ckmat(kstartpt+j,ktype-1)+=(SSmat[startpt+j]);
if(cblocus[i]<0){
cprofvec(kstartpt+j,ktype)+=SSmat[startpt+j];
cprofvec(kstartpt+j+halfwidth*2+1,ktype)+=OSmat[startpt+j];
csigvec(kstartpt+j,ktype)+=SSmat[startpt+j]*SSmat[startpt+j];
csigvec(kstartpt+j+halfwidth*2+1,ktype)+=OSmat[startpt+j]*OSmat[startpt+j];
}else{
cprofvec(kstartpt+j,ktype)+=SSmat[endpt-j];
cprofvec(kstartpt+j+halfwidth*2+1,ktype)+=OSmat[endpt-j];
csigvec(kstartpt+j,ktype)+=SSmat[endpt-j]*SSmat[endpt-j];
csigvec(kstartpt+j+halfwidth*2+1,ktype)+=OSmat[endpt-j]*OSmat[endpt-j];
}
}
}
return coffset;
'
convtest <- cxxfunction(signature(deltaPOS="numeric",deltaNEG='numeric',tfid='numeric',blocus='numeric',offset='numeric',profvec="numeric",sigvec='numeric',tct='numeric'),convKernST,plugin="Rcpp",includes="#include <numeric>\n #include<math.h>")






args=as.double(commandArgs(trailingOnly=TRUE))
#tfn=args[1]
wsize=args[2]
tfmax=args[3]

tfn=as.double(Sys.getenv('SGE_TASK_ID'))

load('/dnase/params.RData')

zs=1e-20

lao=lapply(1:length(exptsub),function(i){
  list(profvec=matrix(zs,wsize*2,tfmax),
       sigvec=matrix(zs,wsize*2,tfmax),
       totct=rep(zs,tfmax),
       bgsum=zs,
       bgn=zs,
       covsum=zs)
})

laoct=lapply(1:length(exptsub),function(i){
  list(profvec=matrix(zs,wsize*2,tfmax),
       sigvec=matrix(zs,wsize*2,tfmax),
       totct=rep(zs,tfmax),
       bgsum=zs,
       bgn=zs,
       covsum=zs)
})


bindoffset=500

#saca=t(sapply(strsplit(readLines(paste0(calltmp,'0/',tfn,'.tfout.gz')),','),as.double))
tfmat=do.call(rbind,lapply(1:length(exptsub),function(i){
rl=readLines(paste0(calltmp,i,'/',tfn,'.tfout.gz'))
x=t(sapply(strsplit(rl,','),as.double))[,c(1:4,7)]
if(any(!is.na(x))){
cbind(x[!is.na(x)],1,i)
}else{
  integer(0)
}
}))

#saca=t(sapply(strsplit(readLines(paste0(calltmp,'0/',tfn,'.tfout.gz')),','),as.double))
tfmatct=do.call(rbind,lapply(1:length(exptsub),function(i){
  rl=readLines(paste0(calltmp,i,'/',tfn,'.tfout.control.gz'))
  x=t(sapply(strsplit(rl,','),as.double))[,c(1:4,7)]
if(any(!is.na(x))){
  cbind(x[!is.na(x)],-1,i)
}else{
  integer(0)
}
}))

if(ncol(tfmatct)>0){

tfall=rbind(tfmat,tfmatct)
tfall=tfall[order(tfall[,1]*1000^2+tfall[,2]*1000+tfall[,3]),]


i=1
chr= -1 
ppar = -1
j=-1
while(i <= nrow(tfall)){
  newrow=tfall[i,]
  if(newrow[2]!=ppar | newrow[1]!=chr){
    chr=newrow[1];ppar=newrow[2]
    load(paste(pwmout,'outfile-',chr,'-',ppar,'.RData',sep=''))
    load(paste(bgout,'chr-',chr,'-',ppar,'.RData',sep=''))
    j=newrow[3]
    dtestPOS=fgset[[j]][[1]][[1]]
    dtestNEG=fgset[[j]][[1]][[2]]
  }else{
  if(newrow[3]!=j){
    j=newrow[3]
    dtestPOS=fgset[[j]][[1]][[1]]
    dtestNEG=fgset[[j]][[1]][[2]]
  }}
  k=newrow[7]
  if(newrow[6]>0){
  convtest(dtestPOS[newrow[7],],dtestNEG[newrow[7],],tfn,newrow[4],bindoffset,lao[[k]]$profvec,lao[[k]]$sigvec,lao[[k]]$totct)
  }else{
  convtest(dtestPOS[newrow[7],],dtestNEG[newrow[7],],tfn,newrow[4],bindoffset,laoct[[k]]$profvec,laoct[[k]]$sigvec,laoct[[k]]$totct)
  }
  mct=min(sapply(lao,function(m){min(m$totct[tfn])}),sapply(laoct,function(m){min(m$totct[tfn])}))
  if(mct>5000){
    break;
  }
  #print(mct)
  i=i+1;
}
}

save(lao,laoct,file=paste(tmpfile,'kernew-',tfn,'.RData',sep=''))


