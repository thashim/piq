#source("http://www.bioconductor.org/biocLite.R")
#biocLite("BSgenome.Mmusculus.UCSC.mm9")
#biocLite("BSgenome.Hsapiens.UCSC.hg19")
require("BSgenome.Mmusculus.UCSC.mm9")
require("BSgenome.Hsapiens.UCSC.hg19")
#biocLite("MotIV")
require("MotIV")



#install.packages("rJava")
require("rJava")
##### IMPORTANT - Remeber to copy password files to home dir
.jinit(parameters=c("-Xmx4G","-Xrs"),force.init=T)
jbasedir="/scratch//Gifford/"
basedir="/scratch/Gifford/"
gsepass=paste(jbasedir,"gse/passwords",sep="")
cgspath=paste(jbasedir,"cgs/build/classes",sep="")
cgslib =paste(jbasedir,"cgs/lib",sep="")
gsepath=paste(jbasedir,"gse/build/classes",sep="")
gselib=paste(jbasedir,"gse/lib/ojdbc/ojdbc14.jar",sep="")
.jaddClassPath(c(paste(basedir,"DNAse/bin",sep=""),gsepass,cgspath,cgslib,gsepath,gselib))

#dnased7=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D5(endoderm)+48hr(A8301+Dorsomorphin) DnaseSeq p2A;2-29-12;bowtie2_pair_single"))
#exdnased7=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased7)

#d5 mesoderm - new one (second to newest)
#d6 pancreatic
#d7

#human stuff
if(FALSE){
crawH1hESC=.jarray(c("--species", "Homo sapiens;hg19","--expt","Crawford H1-hESC DNaseSeq H1-hESC;1;bowtie_unique"))
excrawH1hESC=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",crawH1hESC)
crawH9ES=.jarray(c("--species", "Homo sapiens;hg19","--expt","Crawford H9ES DNaseSeq H9ES;1;bowtie_unique"))
excrawH9ES=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",crawH9ES)
crawGM19238=.jarray(c("--species", "Homo sapiens;hg19","--expt","Crawford GM19238 DNaseSeq GM19238;1;bowtie_unique"))
excrawGM19238=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",crawGM19238)
crawGM19239=.jarray(c("--species", "Homo sapiens;hg19","--expt","Crawford GM19239 DNaseSeq GM19239;1;bowtie_unique"))
excrawGM19239=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",crawGM19239)

dnaseexpt=list(excrawH1hESC,excrawH9ES,excrawGM19238,excrawGM19239)
#bonusexpt=list(exh3k27ac,exh3k4me1,exh3k4me3,exp300)
bonusexpt=NULL
}

#encode
#encd0=.jarray(c('--species','Mus musculus;mm9','--expt','ENCm-UWm-Stam ES-CJ7-E0 DnaseSeq ES-CJ7;1;bowtie_unique'))
#exencd0=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",encd0)
#sysd0=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D0 DnaseSeq p2A;6-17-11;bowtie_unique"))
#exsysd0=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",sysd0)
#sysd0bad=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D0 DnaseSeq p2A;4-17-12;bowtie_unique"))
##exsysd0bad=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",sysd0bad)
##sysd0nr1=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D0 DnaseSeq p2A;7-10-12_rep1;bowtie_unique"))
#exsysd0nr1=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",sysd0nr1)
#sysd0nr2=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D0 DnaseSeq p2A;7-10-12_rep2;bowtie_unique"))
#exsysd0nr2=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",sysd0nr2)

#dnaseexpt=list(exencd0,exsysd0,exsysd0nr1)

if(TRUE){
dnased7=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D5(endoderm)+48hr(A8301+Dorsomorphin) DnaseSeq p2A;2-29-12;bowtie2_pair_single"))
exdnased7=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased7)

dnased6panc=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D5(endoderm)+24hr(RA+Bmp4+A8301) DnaseSeq p2A;3-11-11;bowtie_unique"))
exdnased6panc=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased6panc)

dnased6=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D5(endoderm)+24hr(GSK3) DnaseSeq p2A;4-17-12;bowtie2_pair_single"))
exdnased6=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased6)

dnased5mes=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D5(mesoderm) DnaseSeq p2A;2-29-12;bowtie2_pair_single"))
exdnased5mes=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased5mes)

dnased5=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D5(endoderm) DnaseSeq p2A;4-17-12;bowtie2_pair_single"))
exdnased5=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased5)

dnased3=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D3(mesendoderm) DnaseSeq p2A;10-17-11;bowtie_unique"))
exdnased3=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased3)

dnased0=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D0 DnaseSeq p2A;6-17-11;bowtie_unique"))
exdnased0=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased0)

dnased01=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D0 DnaseSeq p2A;7-10-12_rep1;bowtie2_pair_single"))
exdnased01=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased01)

dnased02=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D0 DnaseSeq p2A;7-10-12_rep2;bowtie2_pair_single"))
exdnased02=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased02)

h3k27ac=.jarray(c("--species", "Mus musculus;mm9","--expt","YL ES H3K27ac 129JAE-C57BL6;2_pmid21106759;bowtie_unique"))
exh3k27ac=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",h3k27ac)

h3k4me1=.jarray(c("--species", "Mus musculus;mm9","--expt","YL ES H3K4me1 129JAE-C57BL6;1_pmid21106759;bowtie_unique"))
exh3k4me1=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",h3k4me1)

h3k4me3=.jarray(c("--species", "Mus musculus;mm9","--expt","YL ES H3K4me3 129JAE-C57BL6;1_pmid21106759;bowtie_unique"))
exh3k4me3=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",h3k4me3)

p300=.jarray(c("--species", "Mus musculus;mm9","--expt","YL ES p300 129JAE-C57BL6;1_pmid21106759;bowtie_unique"))
exp300=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",p300)

#dnased0new=.jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D0 DnaseSeq p2A;4-17-12;bowtie2_pair_single"))
#exdnased0new=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",dnased0new)

dnaseexpt=list(exdnased0,exdnased01,exdnased02,exdnased3,exdnased5,exdnased5mes,exdnased6,exdnased6panc,exdnased7)
#bonusexpt=list(exh3k27ac,exh3k4me1,exh3k4me3,exp300)
bonusexpt=NULL
}

#chipseqsox2=.jarray(c("--species", "Mus musculus;mm9","--expt","YL ES Sox2 V6.5_129-C57BL6;1;bowtie_unique"))
#exchipseqsox2=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",chipseqsox2)

#chipseqoct4=.jarray(c("--species", "Mus musculus;mm9","--expt","YL ES Oct4 V6.5_129-C57BL6;1;bowtie_unique"))
#exchipseqoct4=.jnew("edu/mit/csail/cgs/thashim/ReadExperiment",chipseqoct4)


#r2=getRegionSPEC(exdnased0,"1",rcoord[2,1],rcoord[2,2])



#exps=read.table("../dat/D0.exons.hits",sep="\t",header=TRUE)

#expmap=sapply(substring(names(pwmtable),10),function(i){
#  grep(i,format(exps[,1]),ignore.case=TRUE)[1]
#})

#tfwithexpr = !is.na(expmap)
#tfsub=which(tfwithexpr)



                                        #rt=read.table("../dat/tcf7-2_gps_readdist.txt",sep="\t")
#pos = read.table("../dat/tcf7-2_gps_sig.txt",sep="\t",header=TRUE)
#pos = read.table("../dat/Sox2_YL.GPS",sep="\t",header=TRUE)
#pos = read.table("../dat/YL_ES_Oct4_2_GPS_significant.txt",sep="\t",header=TRUE)
#posvec=strsplit(format(pos[,1]),":")
#frames=data.frame(chrom=sapply(posvec,function(i){i[1]}),pos=sapply(posvec,function(i){as.integer(i[2])}))

####
# expression data
####
#exps=read.table("../dat/D0.exons.hits",sep="\t",header=TRUE)

#expmap=sapply(substring(names(pwmtable),10),function(i){
#  grep(i,format(exps[,1]),ignore.case=TRUE)[1]
#})

#tfwithexpr = !is.na(expmap)
#tfexprs=exps[expmap[tfwithexpr],5]/exps[expmap[tfwithexpr],6]




#chipseqtcf7 = .jarray(c("--species", "Mus musculus;mm9","--expt","SysCODE D5(endoderm)+24hr(GSK3) iTcf7|2(V5) p2A;1_08-04-11;bowtie_unique"))
#exchipseqtcf7 = .jnew("edu/mit/csail/cgs/thashim/ReadExperiment",chipseqtcf7)




getRegion <- function(reader,loc,start,end,pile=10){
  buffer=50
  neg=J(reader,"getReads",.jnew("java/lang/String",as.character(format(loc[1]))),as.integer(start+loc[2]),as.integer(end+loc[2]),as.integer(0),as.integer(pile))
  pos=J(reader,"getReads",.jnew("java/lang/String",as.character(format(loc[1]))),as.integer(start+loc[2]),as.integer(end+loc[2]),as.integer(1),as.integer(pile))
  rbind(pos,neg)
}
#r1=getRegion(exdnased0,data.frame("1",rcoord[2,1]),0,as.double(rcoord[2,2]-rcoord[2,1]))

getRegionSPEC <- function(reader,chr,start,end,pile=10){
  #neg=J(reader,"getReads",.jnew("java/lang/String",as.character(chr)),as.integer(start),as.integer(end),as.integer(0),as.integer(pile))
  #pos=J(reader,"getReads",.jnew("java/lang/String",as.character(chr)),as.integer(start),as.integer(end),as.integer(1),as.integer(pile))
  neg=J(reader,"getMetaReads",.jnew("java/lang/String",as.character(chr)),as.integer(start),as.integer(end),as.integer(1),as.integer(0))
  pos=J(reader,"getMetaReads",.jnew("java/lang/String",as.character(chr)),as.integer(start),as.integer(end),as.integer(1),as.integer(1))
  k=rbind(pos,neg)
  k[k>=pile]=pile
  k
}


getMetaRegion <- function(reader,loc,start,end,by=10){
  J(reader,"getMetaReads",.jnew("java/lang/String",as.character(format(loc[1]))),as.integer(start+loc[2]),as.integer(end+loc[2]),as.integer(by),as.integer(0))+J(reader,"getMetaReads",.jnew("java/lang/String",as.character(format(loc[1]))),as.integer(start+loc[2]),as.integer(end+loc[2]),as.integer(by),as.integer(1))
}

getAllRegions <- function(reader,chrom,by=100000){
  J(reader,"getAllRegions",.jnew("java/lang/String",as.character(chrom)),as.integer(by),as.integer(0))+J(reader,"getAllRegions",.jnew("java/lang/String",as.character(chrom)),as.integer(by),as.integer(1))
}


cshift<- function(x,shift,const=NA){
  tr=x
  if(!is.na(const)){
    if(shift>0){
      tr=c(rep(const,shift),x[(1:(length(x)-shift))])
    }
    if(shift<0){
      tr=c(x[(1-shift):length(x)],rep(const,-shift))
    }
  }else{
    if(shift>0){
      tr=c(rep(x[1],shift),x[(1:(length(x)-shift))])
    }
    if(shift<0){
      tr=c(x[(1-shift):length(x)],rep(x[length(x)],-shift))
    }
  }
  tr
}
