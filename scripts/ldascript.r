#!/usr/bin/Rscript

source('initec2.r')
require('GenomicRanges')
require('lda')

require("Rcpp")
require("inline")
mytabST='
    Rcpp::NumericVector cvec1(vec1);
Rcpp::NumericVector cmaxval(maxval);
Rcpp::NumericVector toret(cmaxval[0]);
for(int i =0; i < cvec1.size();i++){
int a = cvec1[i];
toret[a-1]++;
}
return toret;'
  mytab <- cxxfunction(signature(vec1='numeric',maxval='numeric'),mytabST,plugin='Rcpp')

args=as.double(commandArgs(trailingOnly=TRUE))
i=args[1]

load('/dnase/params.RData')
load(file=paste(tmpfile,'ldatmp.RData'))

print(i)
fos=findOverlaps(flank(ll[[i]],width=50,both=T),allgr)
qh=queryHits(fos)
sh=subjectHits(fos)
typ=values(allgr[sh])[,1]
gh=c(0,findInterval(unique(qh),qh))
rld=lapply(1:(length(gh)-1),function(j){
  lm=((gh[j]+1):(gh[j+1]))
  tabhat=mytab(typ[lm],1331)
  wth=which(tabhat>0)
  rbind(as.integer(wth-1),as.integer(tabhat[wth]))
})
llas=sapply(ll,length)
boundtf=which(sapply(ll,length)>0)
mt=mytab(alltfid[sh],1331)[boundtf]/(length(ll[[i]])*as.double(llas[boundtf]))
#lgs=lda.collapsed.gibbs.sampler(rld,5,names(lgpwm),num.iterations=1000,alpha=0.05,eta=0.5,trace=1L)
#top.topic.words(lgs$topics)
#plot((table(c(1:1331,typ))-1)/(allfreq+1e-10))

save(mt,file=paste(tmpfile,i,'-ctout.RData',sep=''))

