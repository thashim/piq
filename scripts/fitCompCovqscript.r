#!/usr/bin/Rscript

.libPaths('/dnase/libs/')
require('Rcpp',quietly=T)
require('inline',quietly=T)
require("statmod",quietly=T)
gknots = gauss.quad(10,kind="hermite")

poisAllIncludes='
#define cast_uint32_t (uint32_t)
#include <stdint.h>
#include <numeric>
#include <algorithm>
#include<time.h>
/* approximate exponential function */
static inline float
fastpow2 (float p)
{
  float offset = (p < 0) ? 1.0f : 0.0f;
  float clipp = (p < -126) ? -126.0f : p;
  int w = clipp;
  float z = clipp - w + offset;
  union { uint32_t i; float f; } v = { cast_uint32_t ( (1 << 23) * (clipp + 121.2740575f + 27.7280233f / (4.84252568f - z) - 1.49012907f * z) ) };
  return v.f;
}
//
static inline float
fastexp (float p)
{
  return fastpow2 (1.442695040f * p);
}
'


poisNorm2DST ='
Rcpp::NumericVector clambmuone(lambmuone);
Rcpp::NumericVector clambsigone(lambsigone);
Rcpp::NumericVector clambmutwo(lambmutwo);
Rcpp::NumericVector clambsigtwo(lambsigtwo);
Rcpp::NumericVector clambrhos(lambrhos);
Rcpp::NumericMatrix ctablein(tablein);
Rcpp::NumericVector cdfact(dfact);
Rcpp::NumericVector cquadpts(quadpts);
Rcpp::NumericVector cquadwts(quadwts);
Rcpp::NumericVector toret(clambrhos.size());
for(int ri = 0; ri < clambrhos.size();ri++){
Rcpp::NumericVector vvv(3);
for(int i=0; i < ctablein.nrow();i++){
for(int j=0; j < ctablein.ncol();j++){
vvv[0]=0;
for(int k=0; k < cquadpts.size();k++){
for(int l=0; l < cquadpts.size();l++){
//double ti=clambmu[0]+sqrt(clambsig[0]*(1+clambrhos[ri]))*cquadpts[k];
//double tj=sqrt(clambsig[0]*(1-clambrhos[ri]))*cquadpts[l];
//double lamb1 = exp(ti+tj);
//double lamb2 = exp(ti-tj);
double ti=sqrt((1+clambrhos[ri]))*cquadpts[k];
double tj=sqrt((1-clambrhos[ri]))*cquadpts[l];
double lamb1 = exp((ti+tj)*sqrt(clambsigone[0])+clambmuone[0]);
double lamb2 = exp((ti-tj)*sqrt(clambsigtwo[0])+clambmutwo[0]);
double d1 = pow(lamb1,i)*exp(-1*lamb1)/cdfact(i)*cquadwts[k];
double d2 = pow(lamb2,j)*exp(-1*lamb2)/cdfact(j)*cquadwts[l];
vvv[0]+=d1*d2/2;
}
}
if(vvv[0]>0){
toret[ri]+=(ctablein(i,j))*log(vvv[0]);
vvv[1]+=vvv[0];
vvv[2]+=ctablein(i,j);
}
}
}
toret[ri]-=log(vvv[1])*vvv[2];
vvv[1]=0;
vvv[2]=0;
}
return toret;
'
pois2DNorm <- cxxfunction(signature(lambmuone='numeric',lambsigone='numeric',lambmutwo='numeric',lambsigtwo='numeric',lambrhos='numeric',tablein='numeric',quadpts='numeric',quadwts='numeric',dfact='numeric'),poisNorm2DST,plugin="Rcpp",includes=poisAllIncludes)

enumeratePars <- function(tt){
  pois2DNorm(muone,sigone,mutwo,sigtwo,seq(0,1,length=100),tt,gknots$nodes,gknots$weights,factorial(0:(ncol(tt)-1)))
}

###
# main code

args=as.double(commandArgs(trailingOnly=TRUE))
numin=args[1]
offsetst=args[2]
offseted=args[3]

load('/dnase/params.RData')

load(paste(tmpfile,'jointpars-',numin,'.RData',sep=''))

muone=marginals[1,dayone]
sigone=marginals[2,dayone]
mutwo=muone#marginals[1,daytwo]
sigtwo=sigone#marginals[2,daytwo]

SSL=lapply(offsetst:offseted,function(i){enumeratePars(SS[i,,])})
OSL=lapply(offsetst:offseted,function(i){enumeratePars(OS[i,,])})
OSnegL=lapply(offsetst:offseted,function(i){enumeratePars(OSneg[i,,])})
if(offsetst <= dim(DD)[1]){
DDL=lapply(offsetst:min(offseted,dim(DD)[1]),function(i){enumeratePars(DD[i,,])})
}else{
  DDL=NULL
}

save(SSL,OSL,OSnegL,DDL,file=paste(tmpfile,'jointout-',numin,'-',offsetst,'.RData',sep=''))


