#!/usr/bin/Rscript

setwd('/dnase')
source('initec2.r')
#require("Rcpp",quietly=T,warn.conflicts=F)
#require("inline",quietly=T,warn.conflicts=F)

decodeRLE <- function(str,ncols=9){
  ss=strsplit(str,';',fixed=T)[[1]]
  ssi=strsplit(ss,',',fixed=T)
  rlein=list()
  rlein$lengths=as.integer(ssi[[1]])
  rlein$values=as.integer(ssi[[2]])
  class(rlein)='rle'
  rlevec=matrix(inverse.rle(rlein),ncol=ncols*2)
}

args=as.double(commandArgs(trailingOnly=TRUE))
day=args[1]

load('/dnase/params.RData')

blocksz=100
wid=wsize
mulfact=floor(50000/sum(sapply(chrstr,function(i){nrow(read.csv(paste0(datain,i,'rcoord.csv')))})))+1

for(chr in 1:chrlen){
  print(chr)
fconn=file(paste(datain,chrstr[chr],'datin.txt',sep=''),open='rt')
rls=readLines(fconn)
close(fconn)
epos=length(list.files(pwmout,pattern=paste0('outfile-',chrstr[chr],'-')))
if(epos>0){
rcd=read.csv(paste0(datain,chrstr[chr],'rcoord.csv'))
if(flankbg==F){
    dtestrow= 1000 + 2*flank
    shiftoff= c(1,sample(1:(dtestrow-2*flank),(mulfact-1),replace=T))+flank-wsize-1
}else{
    shiftoff= c(1,sample(1:flank,(mulfact-1),replace=T))+flank
}
chrc=do.call(c,lapply(1:length(rls),function(i){
    str=rls[i]
    slen=rcd[i,3]-rcd[i,2]
    dtest=decodeRLE(str,numdays)
    lapply(shiftoff,function(j){
        do=rle(c(dtest[1:(2*wid+1)+j,2*(day-1)+1],dtest[1:(2*wid+1)+j,2*(day-1)+2]))
    })
}))
save(chrc,file=paste0(tmpfile,day,'-',chrstr[chr],'-bgmat.RData'))
}
}

