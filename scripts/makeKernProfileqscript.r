#!/usr/bin/Rscript

.libPaths('/dnase/libs/')

require("Rcpp")
require("inline")

decodeRLE <- function(str,nrows=9){
  ss=strsplit(str,';',fixed=T)[[1]]
  ssi=strsplit(ss,',',fixed=T)
  rlein=list()
  rlein$lengths=as.integer(ssi[[1]])
  rlein$values=as.integer(ssi[[2]])
  class(rlein)='rle'
  rlevec=matrix(inverse.rle(rlein),nrow=nrows*2)
}

convKern = '
Rcpp::NumericVector cdeltaPOS(deltaPOS);
Rcpp::NumericVector ctype(type);
Rcpp::NumericVector cblocus(blocus);
Rcpp::NumericMatrix ckmat(kmat);
Rcpp::NumericVector ctc(totct);
  //After new probability is generated, accumulate the kernel
  for(int i =0; i < ctype.size(); i++){
    int ktype = ctype(i);
    int locus = cblocus(i)-1;
    int halfwidth = (ckmat.nrow()-1)/2;
    int startpt = std::max(0,locus-halfwidth);
    int kstartpt = std::max(0,halfwidth-locus);
    int endpt = std::min(((int)cdeltaPOS.size())-1,locus+halfwidth);
    ctc[ktype-1]++;
    for(int j=0; j<=endpt-startpt;j++){
      ckmat(kstartpt+j,ktype-1)+=(cdeltaPOS[startpt+j]);
    }
  }
return type;
'
convtest <- cxxfunction(signature(deltaPOS="numeric",type="numeric",blocus='numeric',kmat="numeric",totct='numeric'),convKern,plugin="Rcpp",includes="#include <numeric>\n #include<math.h>")

args=as.double(commandArgs(trailingOnly=TRUE))
chr=args[1]
numdays=args[2]
spos=args[3]
epos=args[4]
wsize=args[5]
tfmax=args[6]

lao=lapply(1:numdays,function(i){
  list(kmatPP = matrix(0,wsize,tfmax),
       kmatPN = matrix(0,wsize,tfmax),
       kmatNN = matrix(0,wsize,tfmax),
       kmatNP = matrix(0,wsize,tfmax),
       totctPP = rep(0,tfmax),
       totctPN = rep(0,tfmax),
       totctNP = rep(0,tfmax),
       totctNN = rep(0,tfmax),
       bgsum=0,
       bgn=0)
})

load('kernout/minvals.RData')
bindoffset=500

fconn=file(paste('data/chr-',chr,'-datin.txt',sep=''),open='rt')
offsets=c(0,scan(paste('data/chr-',chr,'-datin.txt.index',sep=''))+1)
seekpos=cumsum(offsets)[(spos-1)*20+1]
seek(fconn,where=seekpos)
for(ppar in spos:epos){
  load(paste('pwmout/outfile-',chr,'-',ppar,'.RData',sep=''))
  for(i in 1:length(pwmscores)){
    ptemp=pwmscores[[i]]
    str=readLines(fconn,1)
    dtest=decodeRLE(str,numdays)
    subsel=ptemp[,3]>=minvals[ptemp[,1]]
    ptemp1=ptemp[subsel,]
    ptemp2=ptemp[subsel,]
    ptemp1[,2]=ptemp1[,2]*-1
    pmask = which(ptemp1[,2]>0)
    for(k in 1:numdays){
      if(length(pmask)>0){
        ctt=convtest(dtest[k,],ptemp1[pmask,1],ptemp1[pmask,2]+bindoffset,lao[[k]]$kmatPP,lao[[k]]$totctPP)
        ctt=convtest(dtest[k+numdays,],ptemp1[pmask,1],ptemp1[pmask,2]+bindoffset,lao[[k]]$kmatPN,lao[[k]]$totctPN)
      }
      if(length(pmask)<nrow(ptemp2)){
        ctt=convtest(dtest[k+numdays,],ptemp2[-pmask,1],ptemp2[-pmask,2]+bindoffset,lao[[k]]$kmatNN,lao[[k]]$totctNN)
        ctt=convtest(dtest[k,],ptemp2[-pmask,1],ptemp2[-pmask,2]+bindoffset,lao[[k]]$kmatNP,lao[[k]]$totctNP)
      }
      lao[[k]]$bgsum=lao[[k]]$bgsum+sum(dtest[k,])
      lao[[k]]$bgn=lao[[k]]$bgn+ncol(dtest)
    }
  }
}
save(lao,file=paste('tmp/ker-',chr,'-',spos,'.RData',sep=''))



