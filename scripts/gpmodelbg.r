setwd('/dnase')
source('initec2.r')

require("statmod")
gknots = gauss.quad(10,kind="hermite")
tol=1e-5

load('/dnase/params.RData')

#numdays=22

#exptstring=c("exdnased0","exdnased01","exdnased02","exdnased01pair","exdnased02pair","exdnasestames1","exdnasestames2","exdnased3","exdnased5","exdnased5old","exdnased5mes","exdnased6","exdnased6panc","exdnased7","exdnaseaza50","exdnaseaza50chase","exdnaseaza175","exdnaseaza175chase","exdnasee1101","exdnasee1102","exdnasestamwhole8wks","exdnasestame14")
#exptsub=c(1,2,3,6,7,8,9,10,11,12,13,14)

load(paste(kernout,'covset.RData',sep=''))

subdays=length(exptsub)
ddcov=matrix(0,subdays,subdays)
for(i in 1:length(exptsub)){
  for(j in 1:length(exptsub)){
    ei=exptsub[i]
    ej=exptsub[j]
    ddcov[i,j]=covDD[min(ei,ej),max(ei,ej)]*sqrt(marginals[2,exptsub[i]]*marginals[2,exptsub[j]])
    if(i!=j){ddcov[i,j]=ddcov[i,j]*0.95}
  }
}

require('glasso')
#sdcov=solve(ddcov)
sdcov=glasso(ddcov,0.1,maxit=10,trace=T)$wi
precdaydz=diag(diag(1/sdcov))%*%sdcov
diag(precdaydz)=0
dayprec=diag(sdcov)

selfdiag=t(t(covSS[exptsub,])*exp(-(0:149)/30))#covSS[(2*numdays-(1:9))*(1:9-1)/2+(1:9),]
#selfdiag[,1]=1
selfos=covOSneg[exptsub,]#covOSneg[(2*numdays-(1:9))*(1:9-1)/2+(1:9),]
cutval=20
imatkPOS=matrix(0,subdays,cutval*2+1)
ncons=rep(0,subdays)
ostprec=rep(0,subdays)
ostreg=rep(0,subdays)
if(length(subdays)>1){
  for(i in 1:nrow(selfdiag)){
  acar=acf2AR(selfdiag[i,])
  arord=rev(which(rowSums(acar[1:cutval,]<0)==0))[1]
  imatkPOS[i,]=-c(rev(acar[arord,1:cutval]),0,acar[arord,1:cutval])/2
  temmat=rbind(c(rep(0,cutval),1,rep(0,cutval)),imatkPOS[i,])
  sdmat=matrix(c(selfdiag[i,]*marginals[2,exptsub[i]],rep(0,cutval*2+1))[abs(outer(1:(2*cutval+1),1:(2*cutval+1),'-'))+1],2*cutval+1)
  tem=temmat%*%sdmat%*%t(temmat)
  ncons[i]=1/(tem[1,1]-tem[1,2]^2/tem[2,2])
  ostreg[i]=-max(selfos[i,])
  #ostprec[i]=1/((1-ostreg[i]^2)*tem[1,1])
  ostprec[i]=1/((1-ostreg[i]^2)*tem[1,1])
}}
else{
    ostreg=0
    ostprec=1/sdmat[1,1]
    ncons=1/sdmat[1,1]
}

divec=dayprec
nivec=ncons
imatkPOS=imatkPOS
imatksqPOS=imatkPOS^2
precdaydz=precdaydz
divecPOS=divec
divecNEG=divec

save(exptsub,divec,nivec,precdaydz,imatkPOS,imatksqPOS,marginals,numdays,ostreg,ostprec,gknots,file=paste(tmpfile,'callcommon-bg.RData',sep=''),compress='bzip2')

for(chr in 1:chrlen){
  lf=list.files(pwmout,pattern=paste(chrstr[chr],'-',sep=''))
  if(length(lf)>0){
  batches=makeBatch(1,length(lf),by=5)
  for(i in 1:length(batches)){
    submitJob('gpmodelbgqscript.R',c(chr,numdays,min(batches[[i]]),max(batches[[i]])))
  }
  }
}

while(length(system('/opt/sge6/bin/linux-x86/qstat',intern=T))>2){print('waiting for sge to clear');Sys.sleep(10)}


for(chr in 1:chrlen){
  lf=list.files(pwmout,pattern=paste(chrstr[chr],'-',sep=''))
  if(length(lf)>0){
  batches=makeBatch(1,length(lf),by=5)
  for(i in 1:length(batches)){
    g=sapply(min(batches[[i]]):max(batches[[i]]),function(j){
      file.exists(paste(bgout,chrstr[chr],'-',j,'.RData',sep=''))
    })
    if(!all(g)){
      print(c(chr,i,which(!g)))
    }
  }}
}

#system(paste('ec2-terminate-instances ',paste(iss[-1],collapse=' '),' -K pk.pem -C cert.pem ',sep=''))


#run kernel part


#run caller
