
####
source('initec2.r')

system('qconf -rattr queue slots 4 all.q@master')
strin=strsplit(system('qhost',intern=T),' ',fixed=T)
sapply(sapply(strin[5:length(strin)],function(i){i[1]}),function(j){
    system(paste0('qconf -rattr queue slots 8 all.q@',j))
})

checkinitf <- function(){
    strin=strsplit(system('qhost',intern=T),' ',fixed=T)
    sapply(sapply(strin[5:length(strin)],function(i){i[1]}),function(j){
        system(paste0('qconf -rattr queue slots 32 all.q@',j))
    })
    flag=F
    dtf=matrix(F,nrow=maxtf,ncol=numdays)
    for(i in seq(1,maxtf,by=2)){
        print(i)
        for(day in 1:numdays){
            if(!file.exists(paste0(callout,i,'-',day,'-diagnostics.pdf'))){
                submitJob('checkinitf.r',c(day,2),T,i,i+1,2)
            }
        }
    }
}

checkinitf2 <- function(){
    flag=F
    dtf=matrix(F,nrow=maxtf,ncol=numdays)
    t(sapply(seq(1,maxtf,by=2),function(i){
        sapply(1:numdays,function(day){
            all(sapply(chrstr,function(j){
                file.exists(paste0(tmpfile,day,'-',j,'-',i,'-factmat.RData'))
            }))
        })
    }))
}

bgset=lapply(1:numdays,function(day){
    load(paste0('tmp/filterpar-',day,'.RData'))
    transvec=(0:2)
    filtermat=diag(ncol(filtermat))
    load(paste0('tmp/normparams-',day,'.RData'))
    load(paste(pwmout,'commonfiles.RData',sep=''))
    chrcbg=do.call(c,lapply(1:length(chrstr),function(i){
        get(load(paste0('/dnase/tmp/',day,'-',chrstr[i],'-bgmat.RData')))
    }))
    datinbg=t(sapply(chrcbg[1:min(50000,length(chrcbg))],function(i){
        tr=inverse.rle(i)
        tr[tr>(length(transvec)-1)]=(length(transvec)-1)
        transvec[tr+1]
    }))%*%filtermat
    covbg=cov(datinbg)
    list(datinbg,covbg)
})

save(bgset,file='tmp/bgset.RData')

load('params.RData')
flag=TRUE
maxiter=5
iter=1
while(flag==TRUE & iter<=maxiter){
submitJob('docallsnewqscript.r',c(2),T,1,maxtf,2)
checkq()
fmat=sapply(1:maxtf,function(i){
  all(sapply(1:numdays,function(day){
    file.exists(paste0(callout,i,'-',day,'-diagnostics.pdf'))
  }))
})
if(sum(!fmat)>0){
checkinitf()
checkq()
tfcheck = (checkinitf2()==FALSE)
if(sum(tfcheck)>0){
    strin=strsplit(system('qhost',intern=T),' ',fixed=T)
    sapply(sapply(strin[5:length(strin)],function(i){i[1]}),function(j){
        system(paste0('qconf -rattr queue slots 4 all.q@',j))
    })
    rerun=which(tfcheck==1,arr.ind=T)
    nsep=2
    for(i in 1:nrow(rerun)){
        submitJob('makeindtffile.r',c(rerun[i,2],nsep),T,rerun[i,1]*2-1,rerun[i,1]*2,nsep)
    }
    checkq()
}
print(which(!fmat))
erls=readLines('/dnase/err.out')
gerr=grep('error encountered',erls)
gerralloc=grep('cannot allocate',erls[gerr+1])
truefail=length(gerr)-length(gerralloc)
flag=(sum(!fmat)-truefail)>0
iter=iter+1
if(iter > 1){
    system('qconf -rattr queue slots 2 all.q@master')
    strin=strsplit(system('qhost',intern=T),' ',fixed=T)
    sapply(sapply(strin[5:length(strin)],function(i){i[1]}),function(j){
        system(paste0('qconf -rattr queue slots 2 all.q@',j))
    })
}
}else{
    flag=F
}
}

for(day in 1:numdays){
df=t(sapply(1:maxtf,function(tfnum){
    if(file.exists(paste0(callout,tfnum,'-',day,'-chropen.txt'))){
        rl=readLines(paste0(callout,tfnum,'-',day,'-chropen.txt'))
        if(length(rl)>0){
            c(tfnum,strsplit(rl,',')[[1]][1])
        }else{
            integer(0)
        }
    }else{
        integer(0)
    }
}))
write.csv(df,paste0(callout,day,'-openindex.csv'))
}

for(day in 1:numdays){
    load(paste0('tmp/filterpar-',day,'.RData'))
    load(paste0(tmpfile,day,'-',chrstr[1],'-bgmat.RData'))
    di=sapply(chrc,inverse.rle)
    di[di>(length(transvec)-1)]=(length(transvec)-1)
    tin=transvec[di+1]
    profs=do.call(rbind,lapply(1:maxtf,function(i){
        fi = file.info(paste0(callout,i,'-',day,'-nonmotifeffect.RData'))$size
        if((!is.na(fi)) & (fi>0)){
            load(paste0(callout,i,'-',day,'-nonmotifeffect.RData'))
            varexp= 2*var(tin)/(numrows)
            c(i,mean(((sfv[nonmotif]-mean(sfv[nonmotif]))^2))/varexp,numrows,pcor,debugvec,toprat)
                      }else{
                          integer(0)
                      }
    }))
    load(paste(pwmout,'commonfiles.RData',sep=''))
    dfout=data.frame(tfname=names(lgpwm)[profs[,1]],id=profs[,1],ratio.not.seqbias=profs[,2],num.pwm.hits=profs[,3],pwm.cor=profs[,4],num.cands=profs[,5],num.prcut=profs[,6],num.95p=profs[,7],num.fdr=profs[,8],num.fdr1=profs[,9],top.rat=profs[,10])
    write.csv(dfout,file=paste0(callout,day,'-reliability.csv'))
}

