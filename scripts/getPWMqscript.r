#!/usr/bin/Rscript

.libPaths('/dnase/libs/')

require('Biostrings',quietly=T,warn.conflicts=F)
require('BSgenome',quietly=T,warn.conflicts=F)
require('IRanges',quietly=T,warn.conflicts=F)

#evaluates PWMs over a DNAStringSet: requires entries table, globally sequence should be defined
#this can be made more efficient by concatenating sequences.
clusterPWM <- function(parlist){
#  sequence=parlist$sequence
    lapply(parlist,function(parsub){
      tseq=parsub
      pwmscores=list()
      for(j in 1:length(lgpwm)){
        #print(j)
        ust = lgpwm[[j]]
        rst = reverseComplement(lgpwm[[j]])
        cutl = tfcut[j]
        ps1=matchPWM(ust,tseq,min.score=cutl)
        ps1mp=(start(ps1)+end(ps1))/2
        sc1=PWMscoreStartingAt(ust,tseq,starting.at=start(ps1))
        ps2=matchPWM(rst,tseq,min.score=cutl)
        ps2mp=(start(ps2)+end(ps2))/2
        sc2=PWMscoreStartingAt(rst,tseq,starting.at=start(ps2))
        ps1sub = which(ps1mp>80 & length(tseq)-ps1mp>80)
        ps2sub = which(ps2mp>80 & length(tseq)-ps2mp>80)
        tr=c(ps1mp[ps1sub]-80,-(ps2mp[ps2sub]-80))
        sc=c(sc1[ps1sub],sc2[ps2sub])
        scsub=sc!=0
        if(length(tr)>0){
          pwmscores[[j]]=data.frame(as.integer(rep(j,length(tr))[scsub]),tr[scsub],signif(sc[scsub],digits=3))#,str)
        }
      }
      if(!all(sapply(pwmscores,is.null))){
        kd=do.call(rbind,pwmscores[!sapply(pwmscores,is.null)])
      }else{
        kd=integer(0)
      }
      kd
    })
}

args=as.double(commandArgs(trailingOnly=TRUE))
#myid=args[2]
mychr=args[1]
myid=as.double(Sys.getenv('SGE_TASK_ID'))
print(c(mychr,myid))
load('/dnase/params.RData')
if(file.exists(paste(tmpfile,'infile-',chrstr[mychr],'-',myid,'.RData',sep='')) & (!file.exists(paste(pwmout,'outfile-',chrstr[mychr],'-',myid,'.RData',sep='')))){
load(paste(tmpfile,'commonfiles.RData',sep=''))
load(paste(tmpfile,'infile-',chrstr[mychr],'-',myid,'.RData',sep=''))
pwmscores=clusterPWM(parlist)
save(pwmscores,file=paste(pwmout,'outfile-',chrstr[mychr],'-',myid,'.RData',sep=''))
system(paste('rm ',tmpfile,'infile-',chrstr[mychr],'-',myid,'.RData',sep=''))
}
