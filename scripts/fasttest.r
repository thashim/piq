##
# code
source('gpmodelCommon.R')
require('statmod')
gknotLG = gauss.quad(9000,kind="hermite")
poiscompLG <- function(mu,sig,dat){
  t=mu+sqrt(2*sig)*gknotLG$nodes
  dp=dpois(dat,exp(t))*gknotLG$weights/sqrt(pi)
  evs=dp%*%cbind(1,t,t^2)
  un=evs[2]/evs[1]
  vn=evs[3]/evs[1]-evs[2]^2/evs[1]^2
  c(un,vn,evs[1])
}

getLapLoc<-function(mu,sig,d,cut=20){
  l=rep(0,length(d))
  l[d<cut]=d[d<cut]*sig[d<cut]+mu[d<cut]-lambert_W0(exp((d*sig+mu)[d<cut])*sig[d<cut])
  l[d>cut]=(log(d[d>cut])*d[d>cut]+mu[d>cut]/sig[d>cut])/(d[d>cut]+1/sig[d>cut])
  sh=1/(exp(l)+1/sig)
  pr=sqrt(2*pi*sh)*dpois(d,exp(l))*dnorm(l,mu,sqrt(sig))
  list(l,sh,pr)
}

makeTable <- function(data,muoffs,stabval,maxcut=10){
  maxval=max(data)
  llmu=sapply(0:maxval,function(i){
    if(i<maxcut){
      v=poiscompLG(muoffs,stabval,i)
      (v[1]/v[2]-muoffs/stabval)/(1/v[2]-1/stabval)
    }else{
      (log(i)*i+muoffs/stabval)/(i+1/stabval)
    }
  })
  llprec=sapply(0:maxval,function(i){
    if(i<maxcut){
      1/poiscompLG(muoffs,stabval,i)[2]-1/stabval
    }else{
      me=(log(i)*i+muoffs/stabval)/(i+1/stabval)
      exp(me)-1/stabval
    }
  })
  list(llmu,llprec)
}

fastFit <- function(data,mupri,muadjusts,sigadjusts,scid){
  datind=which(data>0)
  if(length(datind)>0){
  sigadjust=sigadjusts[data[datind]+1]-sigadjusts[1]
  #
  dss=scid[datind,datind,drop=F]*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  sccm=t(scid[datind,,drop=F])
  cminternal=(solve(dss+diag(length(datind))))*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  a1=sccm%*%cminternal%*%t(sccm)
  allinv=(scid-a1)
  vh=as.vector(muadjusts[data+1]*sigadjusts[data+1]+mupri)
  uinv=scid%*%vh-sccm%*%(cminternal%*%(t(sccm)%*%vh))
  list(uinv,allinv)
  }else{
    list(scid%*%as.vector(mupri+muadjusts[1]*sigadjusts[1]),scid)
  }
}

fastMu <- function(data,mupri,muadjusts,sigadjusts,scid){
  datind=which(data>0)
  if(length(datind)>0){
  sigadjust=sigadjusts[data[datind]+1]-sigadjusts[1]
  #
  dss=scid[datind,datind,drop=F]*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  sccm=t(scid[datind,,drop=F])
  cminternal=(solve(dss+diag(length(datind))))*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  #a1=sccm%*%cminternal%*%t(sccm)
  #allinv=(scid-a1)
  sapply(mupri,function(mu){
    vh=as.vector(muadjusts[data+1]*sigadjusts[data+1]+mu)
    uinv=scid%*%vh-sccm%*%(cminternal%*%(t(sccm)%*%vh))
  })
  }else{
    sapply(mupri,function(mu){
      scid%*%as.vector(mu+muadjusts[1]*sigadjusts[1])
    })
  }
}

#cin = solve(covpost+zerovar)
calcPR <- function(data,mupris,muadjusts,sigadjusts,cin){
  datind=which(data>0)
  dv= sapply(mupris,function(mu){
      as.vector((muadjusts[data+1]-mu)%*%cin%*%(muadjusts[data+1]-mu))
  })
  if(length(datind)>0){
  sigadjust=1/(sigadjusts[data[datind]+1]-sigadjusts[1])
  #
  dss=cin[datind,datind,drop=F]*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  sccm=t(cin[datind,,drop=F])
  cminternal=(solve(dss+diag(length(datind))))*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  da=sapply(mupris,function(mu){
    sum((((muadjusts[data+1]-mu)%*%sccm)^2)%*%cminternal)
    #%*%(t(sccm)%*%(muadjusts[data+1]-mu))
    #as.vector((muadjusts[data+1]-mu)%*%cin%*%(muadjusts[data+1]-mu))
  })
  #a1=sccm%*%cminternal%*%t(sccm)
  #allinv=(cin-a1)
  dv-da
  }else{
    dv
  }

}

  



########
# test runtime vs dimension.

testseq=c(50,200,500,1000,2000)
snum=100
timefast=matrix(0,snum,length(testseq))
mufast=matrix(0,snum,length(testseq))
timefull=matrix(0,snum,length(testseq))
mufull=matrix(0,snum,length(testseq))

for(g in 1:length(testseq)){
  print(g)
  #
sz=testseq[g]
Covtrue = 2*exp(-outer(1:sz,1:sz,'-')^2/(100^2))+diag(rep(1e-2,sz))
evv=eigen(Covtrue)
vars=evv$values
rotation=evv$vectors
#
mutrue=rep(0,nrow(Covtrue))
muoffs=-4
#
irep=t(sapply(1:snum,function(i){rnorm(nrow(Covtrue),mutrue,sqrt(vars))}))
simdatmu=irep%*%t(rotation)+muoffs
#
rpp=rpois(length(simdatmu),as.vector(exp(simdatmu)))
simdat=matrix(rpp,nrow(simdatmu),ncol(simdatmu))
##
# fast
#
covin=Covtrue
#
stabval=1
mustab=-3.2
muin=muoffs
datin=simdat
sz=ncol(datin)
#
tabs = makeTable(datin,mustab,stabval)
sci=solve(covin)
mupri=rep(muin,sz)%*%sci
scid = solve(sci+diag(rep(tabs[[2]][1],sz)))
for(i in 1:nrow(datin)){
  at<-Sys.time()
  ff=fastFit(datin[i,],mupri,tabs[[1]],tabs[[2]],scid)
  mufast[i,g]=sum((ff[[1]]-simdatmu[i,])^2)
  td=Sys.time()-at
  timefast[i,g]=td
}
ecin=eigen(covin)
ecvec=ecin$vectors
decvec=diag(ecin$values)%*%t(ecin$vectors)
sparsebt=ecin$vectors%*%decvec%*%ecvec
for(i in 1:nrow(datin)){
  print(i)
  at<-Sys.time()
  fez=fitGP.ez(datin[i,],rep(muoffs,sz),ecin$values,t(ecvec),1e-5)
  mufull[i,g]=sum((fez[[1]]-simdatmu[i,])^2)
  td=Sys.time()-at
  timefull[i,g]=td
}
}

load('runtimes.RData')

boxplot(log(timefull,10))
boxplot(log(timefast,10))

boxplot(log(mufull,10))
boxplot(log(mufast,10))

gmat = t(matrix(testseq,length(testseq),snum))

dfall=rbind(data.frame(error=as.vector(mufull),time=as.vector(timefull),dim=as.vector(gmat),method='dense'),data.frame(error=as.vector(mufast),time=as.vector(timefast),dim=as.vector(gmat),method='sparse'))

require(ggplot2)
pdf('simulations.pdf')
ggplot(dfall,aes(as.factor(dim),time))+geom_boxplot(aes(fill=factor(method)))+scale_y_log10()+xlab('dimension')+scale_fill_discrete(name='method')+ylab('time(s)')

ggplot(dfall,aes(as.factor(dim),error))+geom_boxplot(aes(fill=factor(method)))+scale_y_log10()+xlab('dimension')+scale_fill_discrete(name='method')+ylab('squared error')
dev.off()


save(timefull,timefast,mufull,mufast,testseq,snum,file='runtimes.RData')

####
# covariance estimate test.

testseq=c(10,50,100)
snum=5000
timefast=matrix(0,snum,length(testseq))
mufast=matrix(0,snum,length(testseq))
timefull=matrix(0,snum,length(testseq))
mufull=matrix(0,snum,length(testseq))

for(g in 1:length(testseq)){
  print(g)
  #
  sz=testseq[g]
  Covtrue = 2*exp(-outer(1:sz,1:sz,'-')^2/(100^2))+diag(rep(1e-2,sz))
  evv=eigen(Covtrue)
  vars=evv$values
  rotation=evv$vectors
  #
  mutrue=rep(0,nrow(Covtrue))
  muoffs=-4
  #
  irep=t(sapply(1:snum,function(i){rnorm(nrow(Covtrue),mutrue,sqrt(vars))}))
  simdatmu=irep%*%t(rotation)+muoffs
  #
  rpp=rpois(length(simdatmu),as.vector(exp(simdatmu)))
  simdat=matrix(rpp,nrow(simdatmu),ncol(simdatmu))
  ##
  # fast
  #
  stabval=1
  mustab=-3.2
  muin=muoffs
  datin=simdat
  sz=ncol(datin)
  #
  tabs = makeTable(datin,mustab,stabval)
  covin=cov(matrix(tabs[[1]][datin+1],nrow(datin)))
  sci=solve(covin)
  mupri=rep(muin,sz)%*%sci
  scid = solve(sci+diag(rep(tabs[[2]][1],sz)))
  for(m in 1:20){
  covall=matrix(0,nrow(covin),ncol(covin))
  muall=matrix(0,nrow(datin),ncol(datin))
  for(i in 1:nrow(datin)){
    ff=fastFit(datin[i,],mupri,tabs[[1]],tabs[[2]],scid)
    muall[i,]=ff[[1]]
    covall=covall+ff[[2]]
  }
  covin=covall/nrow(datin)+cov(muall)
  sci=solve(covin)
  mupri=colMeans(muall)%*%sci
  scid = solve(sci+diag(rep(tabs[[2]][1],sz)))
  }
  #slow
  covin=cov(matrix(tabs[[1]][datin+1],nrow(datin)))
  ecin=eigen(covin)
  ecvec=ecin$vectors
  decvec=diag(ecin$values)%*%t(ecin$vectors)
sparsebt=ecin$vectors%*%decvec%*%ecvec
  muall=matrix(0,nrow(datin),ncol(datin))
  covall=matrix(0,nrow(covin),ncol(covin))
for(i in 1:nrow(datin)){
  print(i)
  fez=fitGP.ez(datin[i,],rep(muoffs,sz),ecin$values,t(ecvec),1e-5)
  ias=invapproxsol(fez[[4]],ecvec,decvec,sparsebt)
  fcd=t(ecvec*fez[[4]])%*%ias
  list(fez[[1]],fcd)
}
}


##############################
# Cpvariance Simulations

#generate data
sz=50

sdset = c(10,20,50,100)
source('gpmodelCommon.R')

allsds=lapply(1:5,function(rep){
  lapply(sdset,function(sdnum){
  print(sdnum)
  #generate data
Covtrue = 2*exp(-outer(1:sz,1:sz,'-')^2/(sdnum^2))+diag(rep(1e-2,sz))
evv=eigen(Covtrue)
vars=evv$values
rotation=evv$vectors
#
mutrue=rep(0,nrow(Covtrue))
muoffs=-4
#
snum=20000
irep=t(sapply(1:snum,function(i){rnorm(nrow(Covtrue),mutrue,sqrt(vars))}))
simdatmu=irep%*%t(rotation)+muoffs
#
rpp=rpois(length(simdatmu),as.vector(exp(simdatmu)))
simdat=matrix(rpp,nrow(simdatmu),ncol(simdatmu))
#####
# fast runner code
stabval=4
muin=-4
datin=simdat
sz=ncol(datin)
#
tabs = makeTable(datin,muin,stabval)
covin=cov(matrix(tabs[[1]][datin+1],nrow(datin)))
sci=solve(covin)
mupri=rep(muin,sz)%*%sci
scid = solve(sci+diag(rep(tabs[[2]][1],sz)))
#
for(g in 1:20){
covsum=matrix(0,ncol(covin),ncol(covin))
muall=matrix(0,nrow(simdat),ncol(simdat))
muevs=matrix(0,nrow(simdat),ncol(simdat))
devs=matrix(0,nrow(simdat),ncol(simdat))
ta=rep(0,nrow(simdat))
for(i in 1:nrow(datin)){
  at<-Sys.time()
ff=fastFit(datin[i,],mupri,tabs[[1]],tabs[[2]],scid)
muall[i,]=ff[[1]]
covsum=covsum+ff[[2]]
devs[i,]=1/(1/diag(ff[[2]])+tabs[[2]][datin[i,]+1])
muevs[i,]=(ff[[1]]/diag(ff[[2]])-tabs[[1]][datin[i,]+1]*tabs[[2]][datin[i,]+1])*devs[i,]
  td=Sys.time()-at
  ta[i]=td
}
cch=cov(muall)
cpost=covsum/nrow(datin)+cch
cpost=cpost*outer(1/sqrt(diag(cpost)),1/sqrt(diag(cpost)),'*')*mean(diag(cpost))
muhat=colMeans(muall)
stabval=mean(devs)#mean(diag(cpost))
muin=mean(muevs)
print(c(stabval,muin))
tabs = makeTable(datin,muin,stabval)
plot(cpost[1,],ylim=c(0,max(2.5,max(cpost[1,]))))
points(covin[1,],col='green')
points(Covtrue[1,],type='l',col='red')
covin=cpost
sci=solve(covin)
mupri=muhat%*%sci
scid = solve(sci+diag(rep(tabs[[2]][1],sz)))
}
#######
# slow runner code.
covin2=cov(matrix(tabs[[1]][datin+1],nrow(datin)))
ecin=eigen(covin2)
gknots = gauss.quad(15,kind="hermite")
#
for(g in 1:10){
  ecvec=ecin$vectors
  decvec=diag(ecin$values)%*%t(ecin$vectors)
  sparsebt=ecin$vectors%*%decvec%*%ecvec
  epfit=lapply(1:nrow(datin),function(i){
    at<-Sys.time()
    fez=fitGP.ez(datin[i,],rep(muoffs,sz),ecin$values,t(ecvec),1e-5)
    ias=invapproxsol(fez[[4]],ecvec,decvec,sparsebt)
    fcd=t(ecvec*fez[[4]])%*%ias
    td=Sys.time()-at
    list(fez[[1]],fcd,td)
  })
  tdm=do.call(c,lapply(epfit,function(i){i[[3]]}))
  allmu=do.call(cbind,lapply(epfit,function(i){i[[1]]}))
  covmu=cov(t(allmu))
  covsig=ecvec%*%(Reduce('+',lapply(epfit,function(i){i[[2]]}))/length(epfit))%*%t(ecvec)
  #
  plot((covmu[1,]+covsig[1,]),ylim=c(0,max(2.5,max(covmu[1,]+covsig[1,]))))
  newcov=covmu+covsig
  points(Covtrue[1,],type='l',col='red')
  points(newcov[1,],col='green',type='l')
  ecin=eigen(newcov)
  print((ecin$values)[1:10])
}
  list(newcov,covin)
})
})

save(allsds,file='allsds.RData')

load('allsds.RData')

cornorm <- function(x){diag(1/sqrt(diag(x)))%*%x%*%diag(1/sqrt(diag(x)))}
sdset = c(10,20,50,100)
sz=50
allsdserr=lapply(allsds,function(sdpack){
  sapply(1:length(sdset),function(sdi){
  print(sdi)
  Covtrue = 2*exp(-outer(1:sz,1:sz,'-')^2/(sdset[sdi]^2))+diag(rep(1e-2,sz))
  ce1=Covtrue-sdpack[[sdi]][[1]]
  ce2=Covtrue-sdpack[[sdi]][[2]]
  core1=sum((cornorm(Covtrue)-cornorm(sdpack[[sdi]][[1]]))^2)
  core2=sum((cornorm(Covtrue)-cornorm(sdpack[[sdi]][[2]]))^2)
  log(c(sum(diag(ce1)^2),sum(diag(ce2)^2),core1,core2))
})})

allsdserrsum=Reduce('+',allsdserr)/5
allsdserrvar=sqrt(Reduce('+',lapply(1:length(allsdserr),function(i){(allsdserr[[i]]-allsdserrsum)^2}))/10)

require('gplots')
pdf('simcov.pdf')
plotCI(sdset,allsdserrsum[1,],uiw=allsdserrvar[1,],type='o',ylim=range(allsdserrsum[1:2,])+c(-1,0.5),ylab='variance error',xlab='bandwidth')
plotCI(sdset,allsdserrsum[2,],uiw=allsdserrvar[2,],type='o',col='red',add=T)

plotCI(sdset,allsdserrsum[3,],uiw=allsdserrvar[3,],type='o',ylim=range(allsdserrsum[3:4,]),ylab='correlation error',xlab='bandwidth')
plotCI(sdset,allsdserrsum[4,],uiw=allsdserrvar[4,],type='o',col='red',add=T)
dev.off()


##########################
# cluster test


fstab=matrix(c('output-106.csv',"../dat/YL_ES_Oct4_2_GPS_significant.txt",'Oct4','output-103.csv',"../dat/ES_Ctcf_GEM_events.txt",'CTCF',"output-110.csv","../dat/ES_Zfx_GEM_events.txt","zfx",'output-105.csv',"../dat/ES_Esrrb_GEM_events.txt",'Esrrb','output-120.csv',"../dat/ES_n-Myc_GEM_events.txt",'n-Myc','output-116.csv',"../dat/ES_Klf4_GEM_events.txt",'Klf4','output-111.csv',"../dat/ES_c-Myc_GEM_events.txt","c-Myc"),ncol=3,byrow=T)

fnum=7
css = read.csv(paste(fstab[fnum,1],sep=""),header=F)
pos = read.table(fstab[fnum,2],sep='\t',header=T,skip=1)
factorname=fstab[fnum,3]
datain2=as.matrix(css[,-(1:3)])
pssms=css[,3]

dat=datain2[,c(1:401,(1:401)+1604)]
sz=ncol(dat)
datbg=dat[order(pssms)[1:5000],]
datbgl=dat[order(pssms)[1:5000],]

require('snow')
closeAllConnections()
cl <- makeCluster(4)

makeblocks <- function(xs,bln){
  bind=do.call(c,lapply(1:bln,function(i){
    do.call(c,lapply(1:bln,function(j){
      ofx=length(xs)*(i-1)
      ofy=length(xs)*(j-1)
      c(list(cbind(xs+ofx,xs+ofy)),
        lapply(1:(length(xs)-1),function(i){
          cbind(ofx+xs[-(length(xs):(length(xs)-i+1))],ofy+xs[-(1:i)])
        }))
    }))
  }))
}

bind=makeblocks(1:401,2)

toepvals <- function(x,bind){
  sapply(bind,function(i){mean(x[i])})
}

toeptomat <- function(x,bind,sz){
  cmt=matrix(0,sz,sz)
  for(i in 1:length(bind)){
    cmt[bind[[i]]]=x[i]
  }
  cmt[cmt==0]=t(cmt)[cmt==0]
  cmt
}

topproj <- function(x,bind){
  spp=toepvals(x,bind)
  toeptomat(spp,bind,ncol(x))
}


stabval=4
stabmu=-3
datin=datbgl
sz=ncol(datin)
#
tabs = makeTable(datin,stabmu,stabval)
covin=cov(matrix(tabs[[1]][datin+1],nrow(datin)))
covin=topproj(covin,bind)
sci=solve(covin)
mupri=rep(stabmu,sz)%*%sci
scid = solve(sci+diag(rep(tabs[[2]][1],sz)))
#
nd=nrow(datin)
parin=lapply(1:nrow(datin),function(i){list(i,datin[i,])})
clusterExport(cl,c('fastFit'))
for(g in 1:20){
  covsum=matrix(0,ncol(covin),ncol(covin))
  muall=matrix(0,nd,ncol(covin))
  muevs=matrix(0,nd,ncol(covin))
  devs=matrix(0,nd,ncol(covin))
  ta=rep(0,nd)
  clusterExport(cl,c('covsum','muall','muevs','devs','scid','tabs','mupri'))
  clbs=clusterApplyLB(cl,parin,function(i){
    ff=fastFit(i[[2]],mupri,tabs[[1]],tabs[[2]],scid)
    muall[i[[1]],]<<-ff[[1]]
    covsum<<-covsum+ff[[2]]
    devs[i[[1]],]<<-1/(1/diag(ff[[2]])+tabs[[2]][i[[2]]+1])
    muevs[i[[1]],]<<-(ff[[1]]/diag(ff[[2]])-tabs[[1]][i[[2]]+1]*tabs[[2]][i[[2]]+1])*devs[i[[1]],]
  })
  muall=Reduce('+',clusterCall(cl,function(){muall}))
  covsum=Reduce('+',clusterCall(cl,function(){covsum}))
  muevs=Reduce('+',clusterCall(cl,function(){muevs}))
  devs=Reduce('+',clusterCall(cl,function(){devs}))
  gc();
  cch=cov(muall)
  cpost=covsum/nrow(datin)+cch
  cpost=topproj(cpost,bind)
  ecp=eigen(cpost)
  ecv=ecp$values
  while(min(ecv)< -1e-2){
    print(min(ecv))
    cpost=ecp$vectors%*%diag(ecv*(ecv>0))%*%t(ecp$vectors)  
    cpost=topproj(cpost,bind)
    ecp=eigen(cpost)
    ecv=ecp$values
  }
  cpost=cpost+diag(ncol(cpost))*0.01
  muhat=colMeans(muall)
                                        #
  stabval=mean(devs)#mean(diag(cpost))
  stabmu=mean(muevs)
                                        #print(c(stabval,muin))
  tabs = makeTable(datin,stabmu,stabval,100)
  print(tabs)
                                        #tabs=list(tabi[1,],tabi[2,])
  plot(cpost[1,],ylim=c(0,max(cpost[1,])))
  covin=cpost
  sci=solve(covin)
  mupri=muhat%*%sci
  scid = solve(sci+diag(rep(tabs[[2]][1],sz)))
}



##################
# slow alg

covin = cov(log(datbg+0.1))
muin = colMeans(log(datbg+0.1))
renorm=diag(sqrt(mean(diag(covin))/diag(covin)))
covin=renorm%*%covin%*%renorm

nvec=10
ecin=eigen(covin)
ecvecs=ecin$vectors[,1:nvec]
ecvals=ecin$values[1:nvec]
pmu=muin%*%ecvecs
sparsebt=ecvecs%*%diag(ecvals)%*%t(ecvecs)%*%ecvecs
scovin=solve(covin)
decvec=diag(ecvals)%*%t(ecvecs)

source('gpmodelCommon.R')
g=readLines("gpmodelCommon.R")
clusterExport(cl,"g")
tn=clusterCall(cl,function(){base::eval(parse(text=g),.GlobalEnv)})

for(m in 1:20){
  #clusterExport(cl,c('ecvals','muin','ecvecs','tsa','delst'))
  clusterExport(cl,c('ecvals','muin','ecvecs','decvec','sparsebt'))
  parlist=lapply(1:nrow(datbg),function(i){datbg[i,]})
  prsout=do.call(cbind,clusterApplyLB(cl,parlist,function(i){
    ctin=i
    fez=fitGP.ez(ctin,muin,ecvals,t(ecvecs),1e-5)
    ias=invapproxsol(fez[[4]],ecvecs,decvec,sparsebt)
    fcd=t(ecvecs*fez[[4]])%*%ias
    list(fez[[1]],fcd)
  }))
#newmean=colMeans(do.call(rbind,prsout[1,]))
  newmean=rep(mean(colMeans(do.call(rbind,prsout[1,]))),ncol(datbg))
  #plot(newmean)
  newcovp1=cov(do.call(rbind,prsout[1,]))
  newcovp2=ecvecs%*%(Reduce('+',prsout[2,])/ncol(prsout))%*%t(ecvecs)
  newcov=newcovp1+newcovp2
  normal=diag(sqrt(mean(diag(newcov))/diag(newcov)))
  newcov=normal%*%newcov%*%normal
  #
  muin=newmean
  ecin=eigen(newcov)
  ecvecs=ecin$vectors[,1:nvec]
  ecvals=ecin$values[1:nvec]
  pmu=muin%*%ecvecs
  sparsebt=ecvecs%*%diag(ecvals)%*%t(ecvecs)%*%ecvecs
  scovin=solve(covin)
  decvec=diag(ecvals)%*%t(ecvecs)
  print(ecvals)
}

pdf('correlations.pdf')
plot(cpost[100,-100]/cpost[100,100],type='l',ylab='correlation',xlab='coordinate')
abline(v=100)
abline(v=500)
plot(topproj(ecvecs%*%decvec,bind)[100,-100],type='l',ylab='correlation',xlab='coordinate')
abline(v=100)
abline(v=500)
dev.off()

pdf('unittest.pdf')

fnum=7

for(fnum in 1:7){

css = read.csv(paste(fstab[fnum,1],sep=""),header=F)
pos = read.table(fstab[fnum,2],sep='\t',header=T,skip=1)
factorname=fstab[fnum,3]
datain2=as.matrix(css[,-(1:3)])
pssms=css[,3]
#
#
posvec=strsplit(format(pos[,1]),":")
frames=data.frame(chrom=sapply(posvec,function(i){i[1]}),pos=sapply(posvec,function(i){as.integer(i[2])}))
bdelt=do.call(c,lapply(1:19,function(chr){
  fsub=frames[frames[,1]==chr,2]
  dsub=css[css[,1]==chr,2]
  sfsub = sort(fsub)
  fis=findInterval(dsub,sfsub)
  fis[fis==0]=1
  abs(sfsub[fis]-dsub)
}))
rm(css);gc();

dat=datain2[,c(1:401,(1:401)+1604)]

sz=ncol(dat)
#
lrd=log(rowSums(dat)+1)
rpss=log(pssms)

#

getAUC <- function(outcome, proba){
 N = length(proba)
 N_pos = sum(outcome)
 df = data.frame(out = outcome, prob = proba)
 df = df[order(-df$prob),]
 df$above = (1:N) - cumsum(df$out)
 return( 1- sum( df$above * df$out ) / (N_pos * (N-N_pos) ) )
}

getROC <- function(eval,truth){
  reval=rank(eval)
  t(sapply(seq(min(reval),max(reval),length=100),function(i){
    c(sum((1-truth)[reval>i])/sum(1-truth),
      sum(truth[reval>i])/sum(truth)
      )
  }))
}

datsub=dat[1:length(bdelt),]
psssub=pssms[1:length(bdelt)]
require(CENTIPEDE)
fcpout=fitCentipede(list(datsub))#,cbind(1,psssub))
fcpoutPS=fitCentipede(list(datsub),cbind(1,psssub))
res=bdelt[1:nrow(datsub)]<10

pdf('profile-zfp161.pdf')
plotProfile(fcpoutPS$LambdaParList[[1]],Mlen=10)
abline(h=1/801)
x1=log(colMeans(datsub[res,]))#/mean(datsub[-res,]))
plotProfile(exp(x1),Mlen=10)
abline(h=mean(datsub[-res,]))
dev.off()

adjust=1

tabs=makeTable(datsub,stabmu,stabval,100)

dsm=matrix(tabs[[1]][datsub+1],nrow(datsub))
psm=matrix(tabs[[2]][datsub+1],nrow(datsub))
sdown=muhat
sup=colMeans(dsm[order(-psssub)[1:1000],])+0.5

#stes=sapply(c(order(-psssub)[1:1000],order(psssub[1:1000])),function(i){
#  ff=fastFit(datsub[i,],mupri,tabs[[1]],tabs[[2]],scid)
#  ff[[1]]
#})

#cdm=log(colMeans(datsub[order(-psssub)[1:1000],]))

#sup=rowMeans(stes[,1:1000])+1
#sdown=rep(mean(stes[,1001:2000]),nrow(stes))
#sup=cdm
#plot(sup-mean(sdown),type='l')

cin=solve(cpost+diag(ncol(cpost))/tabs[[2]][1])

for(i in 1:3){
clusterExport(cl,c('calcPR','tabs','sup','sdown','cin'))
datpar=lapply(1:nrow(datsub),function(i){datsub[i,]})
prss=do.call(cbind,clusterApplyLB(cl,datpar,function(i){
  print(i)
  calcPR(i,list(sup,sdown),tabs[[1]],tabs[[2]],cin)
}))
#newmuset=list(sup%*%sci,sdown%*%sci)
#muset=sapply(1:nrow(datsub),function(i){
#  print(i)
#  as.vector(fastMu(datsub[i,],newmuset,tabs[[1]],tabs[[2]],scid))
#})
lgd=(prss[1,]-prss[2,])*4
pssmu=(psssub-median(psssub))/(sd(psssub))*4
#lgprobs=1/(1+exp(lgd))
#psprobs=1/(1+exp(-pssmu))
probs=1/(1+exp(lgd+2-pssmu))

#sup=colSums(probs*t(muset[1:802,]))/sum(probs)
#sdown=colSums((1-probs)*t(muset[803:nrow(muset),]))/sum(1-probs)


delold=sup-sdown

sup=colSums(probs*dsm)/sum(probs)
sdown=rep(mean(colSums((1-probs)*dsm)/sum((1-probs))),ncol(datsub))
#plot(delold,type='l')
#points(sup-sdown,type='l',col='red')
}

prsinit=sapply(1:nrow(datsub),function(i){
  #print(i)
  ctin=datsub[i,]
  fez=fitGP.ez(ctin,muin,ecvals*adjust,t(ecvecs),1e-5)
  ias=invapproxsol(fez[[4]],ecvecs,decvec,sparsebt)
  fcd=t(ecvecs*fez[[4]])%*%ias
  list(fez[[1]],fez[[2]])
})

allmat=do.call(rbind,prsinit[1,])
allsig=do.call(rbind,prsinit[2,])
#allres=allmat-allmat%*%(ecvecs%*%t(ecvecs))
delst=log((colSums(datsub[order(-psssub)[1:1000],])+0.1)/(colSums((exp(allmat+allsig/2))[order(-psssub)[1:1000],])+0.1))

#plot(delst,type='l')


adjust=1
tsa=diag(ecvals)%*%t(ecvecs)
vvv=diag(ecvecs%*%tsa)

parlist=lapply(1:nrow(datsub),function(i){datsub[i,]})


grfun<-function(dsum,expsum,par,k1,k2,g=0){
  negpen=-(par<g)*k1
  pdiff=sign(diff(par))
  diffder=k2*(c(0,pdiff)-c(pdiff,0))
  dsum-expsum*exp(par)-negpen-diffder
}

dsum=colSums(datsub[order(-psssub)[1:1000],])
expsum=colSums((exp(allmat+allsig/2))[order(-psssub)[1:1000],])
par=delst
eps=0.01
for(i in 1:2000){
  grs=grfun(dsum,expsum,par,5,5,0)
  par=par+eps/sqrt(i)*grs
  #plot(par,main=i,type='l')
}
delst=par+2

adjust=1



#plot(getROC(fcpout$PostPr,res))
#points(getROC(fcpoutPS$PostPr,res),col='purple')
#points(getROC(psssub,res),col='red')
#points(getROC(rowSums(datsub),res),col='blue')
#points(getROC(datsub%*%delst,res),col='blue',pch=20)


for(m in 0:3){
clusterExport(cl,c('ecvals','muin','ecvecs','tsa','delst','adjust'))
tsa=decvec
prsout=do.call(cbind,clusterApplyLB(cl,parlist,function(i){
  ctin=i
  fez=fitGP.ez(ctin,muin,ecvals*adjust,t(ecvecs),1e-5)
  fezbd=fitGP.ez(ctin,muin+delst,ecvals*adjust,t(ecvecs),1e-5)
  adjust=sum(log(fez[[7]])-dnorm(fez[[3]],fez[[5]],sqrt(fez[[4]]+fez[[6]]),log=T))
  adjustbd=sum(log(fezbd[[7]])-dnorm(fezbd[[3]],fezbd[[5]],sqrt(fezbd[[4]]+fezbd[[6]]),log=T))
  #
  #sum(log(fez[[7]]))-sum(log(fezbd[[7]]))
  #
  #dm1=dmvnorm(fez[[3]],muin,diag(fez[[4]])+ecvecs%*%tsa,log=T)
  #dm2=dmvnorm(fezbd[[3]],muin+delst,diag(fezbd[[4]])+ecvecs%*%tsa,log=T)
  #
  inva=invapproxsol(fez[[4]],ecvecs,tsa,fez[[3]]-muin)
  k1=-((fez[[3]]-muin)%*%inva)[1,1]/2
  invb=invapproxsol(fezbd[[4]],ecvecs,tsa,fezbd[[3]]-muin-delst)
  k2=-((fezbd[[3]]-muin-delst)%*%invb)[1,1]/2
  d1=sum(log(fez[[4]]))+determinant(diag(nrow(tsa))+tsa%*%(ecvecs/fez[[4]]),logarithm=T)$modulus
  d2=sum(log(fezbd[[4]]))+determinant(diag(nrow(tsa))+tsa%*%(ecvecs/fezbd[[4]]),logarithm=T)$modulus
  list(fez[[1]],fez[[2]],k1-d1/2,k2-d2/2,adjust,adjustbd,fezbd[[1]],fezbd[[2]])
}))
pr1=do.call(c,prsout[3,])-do.call(c,prsout[4,])
pr2=do.call(c,prsout[5,])-do.call(c,prsout[6,])
prh=pr1+pr2#-median(pr1-pr2)
pr3=isoreg(psssub,-prh)
pr4=isoreg(rowSums(datsub),-prh)
pssmu=(psssub-median(psssub))/sd(psssub)*2
#pr=prh-5*pr3$yf[rank(psssub)]#-pr4$yf[rank(rowSums(datsub))]
pr=prh-pssmu
wadj=log(sum(1/(1+exp(pr)))/length(pr))-log(sum(1-1/(1+exp(pr)))/length(pr))
wt=1/(1+exp(pr+2-wadj))#-pr2))
#cm1=colSums(do.call(rbind,prsout[1,])*wt)/sum(wt)
#cm2=rep(mean(colSums(do.call(rbind,prsout[1,])*(1-wt))/sum(1-wt)),ncol(datsub))
#cm1=log((colSums(datsub*wt)+1)/(colSums(exp(do.call(rbind,prsout[1,])+do.call(rbind,prsout[2,])/2)*wt)+1))
dsum=colSums(datsub*wt)
expsum=colSums(exp(t(t(do.call(rbind,prsout[7,])+do.call(rbind,prsout[8,])/2)-delst))*wt)
par=delst
eps=0.001#*mean(dsum)
delt=Inf
i=1
while(delt>1){
  grs=grfun(dsum,expsum,par,1,1,0)
  delt=(crossprod(grs)/i)
  i=i+1
  par=par+eps/sqrt(i)*grs
}
delst=par
#
#wt=1/(1+exp(pr+2))#-pr2))
#points(getROC(log(wt2)+log(rowSums(datsub)),res),col='yellow',pch=format(m))
#points(getROC(log(wt)+log(rowSums(datsub)),res),col='maroon',pch=format(m))
#points(getROC(-pr,res),col='green',pch=format(m))
}
plot(getROC(fcpout$PostPr,res),type='o')
points(getROC(fcpoutPS$PostPr,res),col='purple',type='o')
points(getROC(psssub,res),col='red',type='o')
points(getROC(rowSums(datsub),res),col='blue',type='o')
points(getROC(probs,res),col='green',type='o')
points(getROC(log(probs)+log(rowSums(datsub)),res),col='yellow',type='o')
points(getROC(log(wt)+log(rowSums(datsub)),res),col='maroon',type='o')
}
dev.off()
