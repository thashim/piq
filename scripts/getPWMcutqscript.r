#!/usr/bin/Rscript
.libPaths('/dnase/libs/')

require('Rcpp')
require('inline')

resortMat = '
//inputs
Rcpp::List cpwms(pwms);
Rcpp::NumericMatrix cpslist(pslist);
Rcpp::NumericVector cass(ass);
Rcpp::NumericVector cminvals(minvals);
Rcpp::NumericVector toret(cpslist.ncol());
Rcpp::NumericVector cupperbd(upperbd);
for(int i = 0; i< cpwms.size(); i++){
Rcpp::List pwmlist = cpwms[i];
if(pwmlist.size()>0){
Rcpp::NumericVector indlist = pwmlist(0);
Rcpp::NumericVector sclist=pwmlist(2);
for(int j = 0; j < indlist.size(); j++){
int targind = indlist[j];
double score = sclist[j];
if(score > cminvals[targind-1] && targind==cass[0]){
bool flag = true;
int counter = 0;
while(flag && counter < cpslist.ncol()){
double targ = cpslist(targind-1,counter);
if(targ < score){
double tmp = cpslist(targind-1,counter);
if((counter+1) < cpslist.ncol()){
for(int k=(counter+1); k < cpslist.ncol();k++){
double tmp2=cpslist(targind-1,k);
cpslist(targind-1,k)=tmp;
tmp=tmp2;
}}
cpslist(targind-1,counter)=score;
flag=false;
}
counter++;
}
}}
}}
for(int i =0; i <cpslist.ncol();i++){
toret[i]=cpslist(cass[0]-1,i);
}
return toret;'
resortMatf <- cxxfunction(signature(pwms="list",pslist="matrix",ass='numeric',minvals='numeric',upperbd='numeric'),resortMat,plugin="Rcpp",includes="#include <numeric>\n #include<math.h> \n #include<string.h>")

###

args=as.double(commandArgs(trailingOnly=TRUE))
tfnum=args[1]
topnum=args[2]
tfmax=args[3]

load('/dnase/params.RData')

pslist=matrix(-Inf,tfmax,topnum)
minvals=rep(-Inf,tfmax)
lset=list.files(pwmout,pattern='outfile')

for(i in lset){
  print(i)
  #if(file.exists(paste(tmpfile,i,sep=''))){
  #  load(paste(tmpfile,i,sep=''))
  #  file.remove(paste(tmpfile,i,sep=''))
  #}else{
    load(paste(pwmout,i,sep=''))
  #}
  rf=resortMatf(pwmscores,pslist,tfnum,minvals,0)
  minvals[tfnum]=rf[topnum]
}
save(rf,file=paste(tmpfile,'pslist-',tfnum,'.RData',sep=''))

