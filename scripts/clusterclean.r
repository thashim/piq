####
#common
##
#

getAUC <- function(outcome, proba){
 N = length(proba)
 N_pos = sum(outcome)
 df = data.frame(out = outcome, prob = proba)
 df = df[order(-df$prob),]
 df$above = (1:N) - cumsum(df$out)
 return( 1- sum( df$above * df$out ) / (N_pos * (N-N_pos) ) )
}

require("gsl")
lambseq=seq(-40,40,length=10^6)
lamlookup=lambert_W0(exp(lambseq))
lambdiff=lambseq[2]-lambseq[1]
lamctrl=c(min(lambseq),lambdiff)

require("statmod")
gknots = gauss.quad(15,kind="hermite")

source("gpmodelCommon.R")

####
#load

fstab=matrix(c('output-106.csv',"../dat/YL_ES_Oct4_2_GPS_significant.txt",'Oct4','output-103.csv',"../dat/ES_Ctcf_GEM_events.txt",'CTCF',"output-110.csv","../dat/ES_Zfx_GEM_events.txt","zfx",'output-105.csv',"../dat/ES_Esrrb_GEM_events.txt",'Esrrb','output-120.csv',"../dat/ES_n-Myc_GEM_events.txt",'n-Myc','output-116.csv',"../dat/ES_Klf4_GEM_events.txt",'Klf4','output-111.csv',"../dat/ES_c-Myc_GEM_events.txt","c-Myc"),ncol=3,byrow=T)

pdf('unittest.pdf')

for(fnum in 1:7){

css = read.csv(paste(fstab[fnum,1],sep=""),header=F)
pos = read.table(fstab[fnum,2],sep='\t',header=T,skip=1)
factorname=fstab[fnum,3]
datain2=as.matrix(css[,-(1:3)])
pssms=css[,3]

#
posvec=strsplit(format(pos[,1]),":")
frames=data.frame(chrom=sapply(posvec,function(i){i[1]}),pos=sapply(posvec,function(i){as.integer(i[2])}))
bdelt=do.call(c,lapply(1:19,function(chr){
  fsub=frames[frames[,1]==chr,2]
  dsub=css[css[,1]==chr,2]
  sfsub = sort(fsub)
  fis=findInterval(dsub,sfsub)
  fis[fis==0]=1
  abs(sfsub[fis]-dsub)
}))
rm(css);gc();

dat=datain2[,c(1:401,(1:401)+1604)]

sz=ncol(dat)
#
lrd=log(rowSums(dat)+1)
rpss=log(pssms)

#

getAUC <- function(outcome, proba){
 N = length(proba)
 N_pos = sum(outcome)
 df = data.frame(out = outcome, prob = proba)
 df = df[order(-df$prob),]
 df$above = (1:N) - cumsum(df$out)
 return( 1- sum( df$above * df$out ) / (N_pos * (N-N_pos) ) )
}

getROC <- function(eval,truth){
  reval=rank(eval)
  t(sapply(seq(min(reval),max(reval),length=100),function(i){
    c(sum((1-truth)[reval>i])/sum(1-truth),
      sum(truth[reval>i])/sum(truth)
      )
  }))
}

datsub=dat[1:length(bdelt),]
psssub=pssms[1:length(bdelt)]
require(CENTIPEDE)
fcpout=fitCentipede(list(datsub))#,cbind(1,psssub))
fcpoutPS=fitCentipede(list(datsub),cbind(1,psssub))
res=bdelt[1:nrow(datsub)]<10

datbg=dat[order(pssms)[1:5000],]
covin = cov(log(datbg+0.1))
muin = colMeans(log(datbg+0.1))
renorm=diag(sqrt(mean(diag(covin))/diag(covin)))
covin=renorm%*%covin%*%renorm

nvec=10
ecin=eigen(covin)
ecvecs=ecin$vectors[,1:nvec]
ecvals=ecin$values[1:nvec]
pmu=muin%*%ecvecs
sparsebt=ecvecs%*%diag(ecvals)%*%t(ecvecs)%*%ecvecs
scovin=solve(covin)
decvec=diag(ecvals)%*%t(ecvecs)



require('snow')
closeAllConnections()
cl <- makeCluster(4)
g=readLines("gpmodelCommon.R")
clusterExport(cl,"g")
tn=clusterCall(cl,function(){base::eval(parse(text=g),.GlobalEnv)})

for(i in 1:10){
  at<-Sys.time()
  fez=fitGP.ez(ctin,muin,ecvals,t(ecvecs),1e-5)
  print(Sys.time()-at)
}

sigtest=runif(ncol(covin),0.5,1)
covs=solve(diag(1/sigtest)+solve(covin))
covs2=solve(diag(1/sigtest)%*%covin+diag(rep(1,ncol(covin))))

for(m in 1:20){
  #clusterExport(cl,c('ecvals','muin','ecvecs','tsa','delst'))
  clusterExport(cl,c('ecvals','muin','ecvecs','decvec','sparsebt'))
  parlist=lapply(1:nrow(datbg),function(i){datbg[i,]})
  prsout=do.call(cbind,clusterApplyLB(cl,parlist,function(i){
    ctin=i
    fez=fitGP.ez(ctin,muin,ecvals,t(ecvecs),1e-5)
    ias=invapproxsol(fez[[4]],ecvecs,decvec,sparsebt)
    fcd=t(ecvecs*fez[[4]])%*%ias
    list(fez[[1]],fcd)
  }))
#newmean=colMeans(do.call(rbind,prsout[1,]))
  newmean=rep(mean(colMeans(do.call(rbind,prsout[1,]))),ncol(datsub))
  #plot(newmean)
  newcovp1=cov(do.call(rbind,prsout[1,]))
  newcovp2=ecvecs%*%(Reduce('+',prsout[2,])/ncol(prsout))%*%t(ecvecs)
  newcov=newcovp1+newcovp2
  normal=diag(sqrt(mean(diag(newcov))/diag(newcov)))
  newcov=normal%*%newcov%*%normal
  #
  muin=newmean
  ecin=eigen(newcov)
  ecvecs=ecin$vectors[,1:nvec]
  ecvals=ecin$values[1:nvec]
  pmu=muin%*%ecvecs
  sparsebt=ecvecs%*%diag(ecvals)%*%t(ecvecs)%*%ecvecs
  scovin=solve(covin)
  decvec=diag(ecvals)%*%t(ecvecs)
  print(ecvals)
}


adjust=1

prsinit=sapply(1:nrow(datsub),function(i){
  #print(i)
  ctin=datsub[i,]
  fez=fitGP.ez(ctin,muin,ecvals*adjust,t(ecvecs),1e-5)
  ias=invapproxsol(fez[[4]],ecvecs,decvec,sparsebt)
  fcd=t(ecvecs*fez[[4]])%*%ias
  list(fez[[1]],fez[[2]])
})

allmat=do.call(rbind,prsinit[1,])
allsig=do.call(rbind,prsinit[2,])
#allres=allmat-allmat%*%(ecvecs%*%t(ecvecs))
delst=log((colSums(datsub[order(-psssub)[1:1000],])+0.1)/(colSums((exp(allmat+allsig/2))[order(-psssub)[1:1000],])+0.1))

#plot(delst,type='l')


adjust=1
tsa=diag(ecvals)%*%t(ecvecs)
vvv=diag(ecvecs%*%tsa)

parlist=lapply(1:nrow(datsub),function(i){datsub[i,]})


grfun<-function(dsum,expsum,par,k1,k2,g=0){
  negpen=-(par<g)*k1
  pdiff=sign(diff(par))
  diffder=k2*(c(0,pdiff)-c(pdiff,0))
  dsum-expsum*exp(par)-negpen-diffder
}

dsum=colSums(datsub[order(-psssub)[1:1000],])
expsum=colSums((exp(allmat+allsig/2))[order(-psssub)[1:1000],])
par=delst
eps=0.01
for(i in 1:2000){
  grs=grfun(dsum,expsum,par,5,5,0)
  par=par+eps/sqrt(i)*grs
  #plot(par,main=i,type='l')
}
delst=par+2

adjust=1



plot(getROC(fcpout$PostPr,res))
points(getROC(fcpoutPS$PostPr,res),col='purple')
points(getROC(psssub,res),col='red')
points(getROC(rowSums(datsub),res),col='blue')
points(getROC(datsub%*%delst,res),col='blue',pch=20)


for(m in 0:3){
clusterExport(cl,c('ecvals','muin','ecvecs','tsa','delst','adjust'))
tsa=decvec
prsout=do.call(cbind,clusterApplyLB(cl,parlist,function(i){
  ctin=i
  fez=fitGP.ez(ctin,muin,ecvals*adjust,t(ecvecs),1e-5)
  fezbd=fitGP.ez(ctin,muin+delst,ecvals*adjust,t(ecvecs),1e-5)
  adjust=sum(log(fez[[7]])-dnorm(fez[[3]],fez[[5]],sqrt(fez[[4]]+fez[[6]]),log=T))
  adjustbd=sum(log(fezbd[[7]])-dnorm(fezbd[[3]],fezbd[[5]],sqrt(fezbd[[4]]+fezbd[[6]]),log=T))
  #
  #sum(log(fez[[7]]))-sum(log(fezbd[[7]]))
  #
  #dm1=dmvnorm(fez[[3]],muin,diag(fez[[4]])+ecvecs%*%tsa,log=T)
  #dm2=dmvnorm(fezbd[[3]],muin+delst,diag(fezbd[[4]])+ecvecs%*%tsa,log=T)
  #
  inva=invapproxsol(fez[[4]],ecvecs,tsa,fez[[3]]-muin)
  k1=-((fez[[3]]-muin)%*%inva)[1,1]/2
  invb=invapproxsol(fezbd[[4]],ecvecs,tsa,fezbd[[3]]-muin-delst)
  k2=-((fezbd[[3]]-muin-delst)%*%invb)[1,1]/2
  d1=sum(log(fez[[4]]))+determinant(diag(nrow(tsa))+tsa%*%(ecvecs/fez[[4]]),logarithm=T)$modulus
  d2=sum(log(fezbd[[4]]))+determinant(diag(nrow(tsa))+tsa%*%(ecvecs/fezbd[[4]]),logarithm=T)$modulus
  list(fez[[1]],fez[[2]],k1-d1/2,k2-d2/2,adjust,adjustbd,fezbd[[1]],fezbd[[2]])
}))
pr1=do.call(c,prsout[3,])-do.call(c,prsout[4,])
pr2=do.call(c,prsout[5,])-do.call(c,prsout[6,])
prh=pr1+pr2#-median(pr1-pr2)
pr3=isoreg(psssub,-prh)
pr4=isoreg(rowSums(datsub),-prh)
pssmu=(psssub-median(psssub))/sd(psssub)*2
#pr=prh-5*pr3$yf[rank(psssub)]#-pr4$yf[rank(rowSums(datsub))]
pr=prh-pssmu
wadj=log(sum(1/(1+exp(pr)))/length(pr))-log(sum(1-1/(1+exp(pr)))/length(pr))
wt=1/(1+exp(pr+2-wadj))#-pr2))
#cm1=colSums(do.call(rbind,prsout[1,])*wt)/sum(wt)
#cm2=rep(mean(colSums(do.call(rbind,prsout[1,])*(1-wt))/sum(1-wt)),ncol(datsub))
#cm1=log((colSums(datsub*wt)+1)/(colSums(exp(do.call(rbind,prsout[1,])+do.call(rbind,prsout[2,])/2)*wt)+1))
dsum=colSums(datsub*wt)
expsum=colSums(exp(t(t(do.call(rbind,prsout[7,])+do.call(rbind,prsout[8,])/2)-delst))*wt)
par=delst
eps=0.001#*mean(dsum)
delt=Inf
i=1
while(delt>1){
  grs=grfun(dsum,expsum,par,1,1,0)
  delt=(crossprod(grs)/i)
  i=i+1
  par=par+eps/sqrt(i)*grs
}
delst=par
#
#wt=1/(1+exp(pr+2))#-pr2))
#points(getROC(log(wt2)+log(rowSums(datsub)),res),col='yellow',pch=format(m))
points(getROC(log(wt)+log(rowSums(datsub)),res),col='maroon',pch=format(m))
points(getROC(-pr,res),col='green',pch=format(m))
}

points(getROC(log(fcpout$PostPr)+log(rowSums(datsub)),res),col='yellow')
}
dev.off()
