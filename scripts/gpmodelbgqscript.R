#!/usr/bin/Rscript

source('initec2.r')
require("Rcpp",quietly=T)
require("inline",quietly=T)

args=as.double(commandArgs(trailingOnly=TRUE))
chr=args[1]
numdays=args[2]
spos=args[3]
epos=args[4]

decodeRLE <- function(str,nrows=9){
  ss=strsplit(str,';',fixed=T)[[1]]
  ssi=strsplit(ss,',',fixed=T)
  rlein=list()
  rlein$lengths=as.integer(ssi[[1]])
  rlein$values=as.integer(ssi[[2]])
  class(rlein)='rle'
  rlevec=matrix(inverse.rle(rlein),nrow=nrows*2)
}

uniconv ='
Rcpp::NumericVector cvadd(vadd);
Rcpp::NumericVector cvret(cvadd.size());
int cksize = as<int>(ksize);
int halfsz = (cksize-1)/2;
double temp=0;
for(int i=0; i < (halfsz);i++){
temp+=cvadd[i];
}
for(int i=0; i < cvadd.size();i++){
if((i+halfsz)< cvadd.size()){
temp+=cvadd[i+halfsz];
}
cvret[i]=temp/cksize;
if((i-halfsz)>=0){
temp-=cvadd[i-halfsz];
}
}
return cvret;
'

uconv <- cxxfunction(signature(vadd="numeric",ksize="integer"),uniconv,plugin="Rcpp",includes="#include <numeric>")

poisAllIncludes='
#define cast_uint32_t (uint32_t)
#include <stdint.h>
#include <numeric>
#include <algorithm>
#include<time.h>
/* approximate exponential function */
static inline float
fastpow2 (float p)
{
  float offset = (p < 0) ? 1.0f : 0.0f;
  float clipp = (p < -126) ? -126.0f : p;
  int w = clipp;
  float z = clipp - w + offset;
  union { uint32_t i; float f; } v = { cast_uint32_t ( (1 << 23) * (clipp + 121.2740575f + 27.7280233f / (4.84252568f - z) - 1.49012907f * z) ) };
  return v.f;
}
//
static inline float
fastexp (float p)
{
  return fastpow2 (1.442695040f * p);
}
'

poisNormST ='
Rcpp::NumericMatrix clambmus(lambmus);
Rcpp::NumericMatrix clambsigs(lambsigs);
Rcpp::NumericMatrix cdatain(datain);
Rcpp::NumericMatrix lcdatain(ldatain);
Rcpp::NumericMatrix cdfact(dfact);
Rcpp::NumericVector cquadpts(quadpts);
Rcpp::NumericVector cquadptsq(cquadpts.size());
for(int i=0; i<cquadpts.size();i++){
  cquadptsq[i]=cquadpts[i]*cquadpts[i];
}
Rcpp::NumericVector cquadwts(quadwts);
Rcpp::NumericMatrix cnewlambP(clambmus.nrow(),clambmus.ncol());
Rcpp::NumericMatrix cnewlambgr(clambmus.nrow(),clambmus.ncol());
Rcpp::NumericMatrix cnewlambmu(clambmus.nrow(),clambmus.ncol());
Rcpp::NumericMatrix cnewlambsig(clambsigs.nrow(),clambsigs.ncol());
for(int j=0; j < clambmus.ncol();j++){
for(int i=0; i < clambmus.nrow();i++){
Rcpp::NumericVector vvv(4);
double cdf=cdfact(i,j);
double cdi=cdatain(i,j);
double cls=clambsigs(i,j);
double clm=clambmus(i,j);
double clse=sqrt(2*cls);
double clme=clm;
if(cdi>3){
   clse=1/(1/cls+cdi);
   clme=(clm/cls+cdi*lcdatain(i,j))*clse;
   clse=sqrt(2*clse);
}
for(int k=0; k < cquadpts.size();k++){
  double ti=clme+clse*cquadpts[k];
  double lamb = fastexp(ti);
  //double dp = fastexp(-1*lamb)/cdf*cquadwts[k];
  double lpr = cquadwts[k]-cdf-lamb+cdi*ti+cquadptsq[k]-(ti-clm)*(ti-clm)/(2*cls);
  //if(cdi>0){
  //  dp*=pow(lamb,cdi);
  //}
  double dp = fastexp(lpr);
  vvv[0]+=dp;
  vvv[1]+=dp*ti;
  vvv[2]+=dp*ti*ti;
  vvv[3]+=dp*lamb;
}
  cnewlambgr(i,j)=vvv[3]/vvv[0];
  cnewlambP(i,j)=vvv[0]*clse;
  cnewlambmu(i,j)=vvv[1]/vvv[0];
  cnewlambsig(i,j)=vvv[2]/vvv[0]-vvv[1]*vvv[1]/(vvv[0]*vvv[0]);
}
}
Rcpp::List toret;
toret[\"mu\"]=cnewlambmu;
toret[\"sig\"]=cnewlambsig;
toret[\"muexp\"]=cnewlambgr;
toret[\"P\"]=cnewlambP;
return toret;'
poisNorm <- cxxfunction(signature(lambmus='numeric',lambsigs='numeric',datain='numeric',ldatain='numeric',quadpts='numeric',quadwts='numeric',dfact='numeric'),poisNormST,plugin="Rcpp",includes=poisAllIncludes)

cconvST = '
Rcpp::NumericMatrix cconvkern(convkern); //ODD kernel size (row is matched to cconvtarg)
Rcpp::NumericMatrix cconvtarg(convtarg);// convolves along each row
Rcpp::NumericMatrix returnvec(cconvtarg.nrow(),cconvtarg.ncol());//(cconvtarg.nrow(),cconvtarg.ncol());
//int perlen = cconvtarg.ncol()+cconvkern.ncol()-1;
//Rcpp::NumericVector returnvec(perlen*cconvtarg.nrow());//(cconvtarg.nrow(),cconvtarg.ncol());
int halfw=floor((cconvkern.ncol()-1)/2);
for(int bnum=0;bnum < cconvkern.nrow();bnum++){
Rcpp::NumericVector xa = cconvtarg.row(bnum);
Rcpp::NumericVector xb = cconvkern.row(bnum);
int n_xa = xa.size(), n_xb = xb.size();
Rcpp::NumericVector xab(n_xa + n_xb-1, 0.0);
Rcpp::Range r( 0, n_xb-1 );
for (int i=0; i<n_xa; i++, r++)
xab[ r ] += Rcpp::noNA(xa[i]) * Rcpp::noNA(xb);
//Rcpp::Range rn(perlen*(bnum),perlen*(bnum+1)-1);
//returnvec[rn]=xab;
Rcpp::NumericVector::iterator ia=xab.begin();
for (int i=0; i<cconvtarg.ncol();i++) returnvec(bnum,i)=Rcpp::noNA(ia[i+halfw]);
}
return returnvec;
'
cconv <- cxxfunction(signature(convtarg='matrix',convkern='matrix'),cconvST,plugin='Rcpp',includes='#include <numeric>')

applyconv <- function(x,y){
  cconv(x,y)
}

fitBG<- function(dd,lpri,spri,rpt=NULL){
  at<-Sys.time()
  sz=ncol(lpri)/2
  dptPOS=t(dd)[,1:sz]#matrix(inverse.rle(paramday[[1]]$din)[1:sz],1)
  dptNEG=t(dd)[,(sz+1):(2*sz)]#matrix(inverse.rle(paramday[[1]]$din)[(sz+1):(2*sz)],1)
  #fdptPOS=factorial(dptPOS)
  fdptPOS=lgamma(dptPOS+1)
  #fdptNEG=factorial(dptNEG)
  fdptNEG=lgamma(dptNEG+1)
  #
  lpriorPOS=lpri[,1:sz,drop=F]
  dsbPOS=spri[,1:sz,drop=F]
  lpriorNEG=lpri[,(sz+1):(2*sz),drop=F]
  dsbNEG=spri[,(sz+1):(2*sz),drop=F]
  #
  if(is.null(rpt)){
    #
    lambmutPOS=matrix(0,nrow(lpri),ncol(lpri)/2)#lpri[,1:sz,drop=F]
    lambsigtPOS=matrix(Inf,nrow(lpri),ncol(lpri)/2)#rep(Inf,sz)spri[,1:sz,drop=F]
    lambmutdatPOS=matrix(0,nrow(lpri),ncol(lpri)/2)
    lambsigtdatPOS=matrix(Inf,nrow(lpri),ncol(lpri)/2)#rep(Inf,sz)
    lambmuPOS=lpri[,1:sz,drop=F]
    lambsigPOS=spri[,1:sz,drop=F]
    #
    lambmutNEG=matrix(0,nrow(lpri),ncol(lpri)/2)#lpri[,(sz+1):(2*sz),drop=F]
    lambsigtNEG=matrix(Inf,nrow(lpri),ncol(lpri)/2)#rep(Inf,sz)#spri[,(sz+1):(2*sz),drop=F]
    lambmutdatNEG=matrix(0,nrow(lpri),ncol(lpri)/2)
    lambsigtdatNEG=matrix(Inf,nrow(lpri),ncol(lpri)/2)#rep(Inf,sz)
    lambmuNEG=lpri[,(sz+1):(2*sz),drop=F]
    lambsigNEG=spri[,(sz+1):(2*sz),drop=F]
    #
    lambmutdayPOS=lambmuPOS
    lambmutdayNEG=lambmuNEG
    lambsigtdayPOS=lambsigPOS
    lambsigtdayNEG=lambsigNEG
    #
    lambmutstrPOS=lambmutPOS
    lambmutstrNEG=lambmutNEG
    lambsigtstrPOS=lambsigtPOS
    lambsigtstrNEG=lambsigtNEG
  }else{
    lambmutPOS=rpt$lambmutPOS
    lambsigtPOS=rpt$lambsigtPOS
    lambmutdatPOS=rpt$lambmutdatPOS
    lambsigtdatPOS=rpt$lambsigtdatPOS
    lambsigPOS=1/(1/spri[,1:sz,drop=F]+1/lambsigtdatPOS+1/lambsigtPOS)
    lambmuPOS=(lpri[,1:sz,drop=F]/spri[,1:sz,drop=F]+lambmutdatPOS/lambsigtdatPOS+lambmutPOS/lambsigtPOS)*lambsigPOS
    lambmutNEG=rpt$lambmutNEG
    lambsigtNEG=rpt$lambsigtNEG
    lambmutdatNEG=rpt$lambmutdatNEG
    lambsigtdatNEG=rpt$lambsigtdatNEG
    lambsigNEG=1/(1/spri[,(sz+1):(2*sz),drop=F]+1/lambsigtdatNEG+1/lambsigtNEG)
    lambmuNEG=(lpri[,(sz+1):(2*sz),drop=F]/spri[,(sz+1):(2*sz),drop=F]+lambmutdatNEG/lambsigtdatNEG+lambmutNEG/lambsigtNEG)*lambsigNEG
  }
  #
  tol=0.05
  maxrun=30
  terr=Inf
  itct=0
  #
  lgknots=log(gknots$weights)
  tmvfft=0
  tpn=0
  ldptPOS=log(dptPOS)
  ldptNEG=log(dptNEG)
  while(terr>tol && itct < maxrun){
    # update lambda BG according to observed data. (POS str)
    ttpn=Sys.time()
    cavsigPOS=1/(1/lambsigPOS-1/lambsigtdatPOS)
    cavmuPOS=(lambmuPOS/lambsigPOS-lambmutdatPOS/lambsigtdatPOS)*cavsigPOS
    newlambPOS=poisNorm(cavmuPOS,cavsigPOS,dptPOS,ldptPOS,gknots$nodes,lgknots,fdptPOS)
    newsigPOS=newlambPOS[[2]]
    newmuPOS=newlambPOS[[1]]
    lambsigtdatPOS=1/(1/newsigPOS-1/cavsigPOS)
    lambmutdatPOS=(newmuPOS/newsigPOS-cavmuPOS/cavsigPOS)*lambsigtdatPOS
    lambsigPOS=newsigPOS
    lambmuPOS=newmuPOS
    #Neg str
    cavsigNEG=1/(1/lambsigNEG-1/lambsigtdatNEG)
    cavmuNEG=(lambmuNEG/lambsigNEG-lambmutdatNEG/lambsigtdatNEG)*cavsigNEG
    newlambNEG=poisNorm(cavmuNEG,cavsigNEG,dptNEG,ldptNEG,gknots$nodes,lgknots,fdptNEG)
    newsigNEG=newlambNEG[[2]]
    newmuNEG=newlambNEG[[1]]
    lambsigtdatNEG=1/(1/newsigNEG-1/cavsigNEG)
    lambmutdatNEG=(newmuNEG/newsigNEG-cavmuNEG/cavsigNEG)*lambsigtdatNEG
    lambsigNEG=newsigNEG
    lambmuNEG=newmuNEG
    tpn=tpn+difftime(Sys.time(),ttpn,units='secs')
    # update lambda BG according to prior
    ttmvfft=Sys.time()
    cavsigPOS=1/(1/lambsigPOS-1/lambsigtPOS);cavsigNEG=1/(1/lambsigNEG-1/lambsigtNEG)
    cavmuPOS=(lambmuPOS/lambsigPOS-lambmutPOS/lambsigtPOS)*cavsigPOS
    cavmuNEG=(lambmuNEG/lambsigNEG-lambmutNEG/lambsigtNEG)*cavsigNEG
    #(prior term)+(delta from conditioning on POS)+(delta from condition on NEG)
    #restmuPOS=lpriorPOS-t(imymvfft(mymvfft(t(cavmuPOS-lpriorPOS),pln)*t(filtimatPOS),pln))-precdaydz%*%(cavmuPOS-lpriorPOS)
    restmuPOS=lpriorPOS-applyconv(cavmuPOS-lpriorPOS,imatkPOS)#-precdaydz%*%(cavmuPOS-lpriorPOS)
    #restsigPOS=1/divecPOS+t(imymvfft(mymvfft(t(cavsigPOS),pln)*t(filtimatsqPOS),pln))+(precdaydz^2)%*%(cavsigPOS)
    restsigPOS=1/nivec+applyconv(cavsigPOS,imatksqPOS)#+(precdaydz^2)%*%(cavsigPOS)
    #for neg now
    #restmuNEG=lpriorNEG-t(imymvfft(mymvfft(t(cavmuNEG-lpriorNEG),pln)*t(filtimatPOS),pln))-precdaydz%*%(cavmuNEG-lpriorNEG)
    restmuNEG=lpriorNEG-applyconv(cavmuNEG-lpriorNEG,imatkPOS)#-precdaydz%*%(cavmuNEG-lpriorNEG)
    #restsigNEG=1/divecNEG+t(imymvfft(mymvfft(t(cavsigNEG),pln)*t(filtimatsqPOS),pln))+(precdaydz^2)%*%(cavsigNEG)
    restsigNEG=1/nivec+applyconv(cavsigNEG,imatksqPOS)#+(precdaydz^2)%*%(cavsigNEG)
    #todo - integrate days
    tmvfft=tmvfft+difftime(Sys.time(),ttmvfft,units='secs')
    #
    #newsigPOS=1/(1/restsigPOS+1/cavsigPOS)
    #newsigNEG=1/(1/restsigNEG+1/cavsigNEG)
    #newmuPOS=(restmuPOS/restsigPOS+cavmuPOS/cavsigPOS)*newsigPOS
    #newmuNEG=(restmuNEG/restsigNEG+cavmuNEG/cavsigNEG)*newsigNEG
    #lambsigtPOS=restsigPOS;lambsigtNEG=restsigNEG
    #lambmutPOS=(restmuPOS);lambmutNEG=(restmuNEG)
    #lambsigPOS=newsigPOS;lambsigNEG=newsigNEG
    #terr=abs(crossprod(as.vector(newmuNEG)-as.vector(lambmuNEG)))
    #print(terr)
    #lambmuPOS=newmuPOS;lambmuNEG=newmuNEG
    #
    #newsigPOS=1/(1/restsigPOS+1/cavsigPOS-1/dsbPOS);newsigNEG=1/(1/restsigNEG+1/cavsigNEG-1/dsbNEG)
    #newmuPOS=(restmuPOS/restsigPOS+cavmuPOS/cavsigPOS-lpriorPOS/dsbPOS)*newsigPOS
    #newmuNEG=(restmuNEG/restsigNEG+cavmuNEG/cavsigNEG-lpriorNEG/dsbNEG)*newsigNEG
    newsigPOS=1/(1/restsigPOS+1/cavsigPOS);newsigNEG=1/(1/restsigNEG+1/cavsigNEG)
    newmuPOS=(restmuPOS/restsigPOS+cavmuPOS/cavsigPOS)*newsigPOS
    newmuNEG=(restmuNEG/restsigNEG+cavmuNEG/cavsigNEG)*newsigNEG
    lambsigtPOS=1/(1/newsigPOS-1/cavsigPOS);lambsigtNEG=1/(1/newsigNEG-1/cavsigNEG)
    lambmutPOS=(newmuPOS/newsigPOS-cavmuPOS/cavsigPOS)*lambsigtPOS;lambmutNEG=(newmuNEG/newsigNEG-cavmuNEG/cavsigNEG)*lambsigtNEG
    lambsigPOS=newsigPOS;lambsigNEG=newsigNEG
    terr=abs(crossprod(as.vector(newmuNEG)-as.vector(lambmuNEG)))
    #print(terr)
    lambmuPOS=newmuPOS;lambmuNEG=newmuNEG
    #plot(lambmuPOS[1,],type='l',col='blue')
    #points(lambmuNEG[1,],type='l',col='green')
    #day to day integrate
    cavsigPOS=1/(1/lambsigPOS-1/lambsigtdayPOS);cavsigNEG=1/(1/lambsigNEG-1/lambsigtdayNEG)
    cavmuPOS=(lambmuPOS/lambsigPOS-lambmutdayPOS/lambsigtdayPOS)*cavsigPOS
    cavmuNEG=(lambmuNEG/lambsigNEG-lambmutdayNEG/lambsigtdayNEG)*cavsigNEG
    #
    restmuPOS=lpriorPOS-precdaydz%*%(cavmuPOS-lpriorPOS)
    restmuNEG=lpriorNEG-precdaydz%*%(cavmuNEG-lpriorNEG)
    restsigPOS=1/(divec)+(precdaydz^2)%*%(cavsigPOS)
    restsigNEG=1/(divec)+(precdaydz^2)%*%(cavsigNEG)
    #
    #newsigPOS=1/(1/restsigPOS+1/cavsigPOS)
    #newsigNEG=1/(1/restsigNEG+1/cavsigNEG)
    #newmuPOS=(restmuPOS/restsigPOS+cavmuPOS/cavsigPOS)*newsigPOS
    #newmuNEG=(restmuNEG/restsigNEG+cavmuNEG/cavsigNEG)*newsigNEG
    #lambsigtdayPOS=restsigPOS;lambsigtdayNEG=restsigNEG
    #lambmutdayPOS=(restmuPOS);lambmutdayNEG=(restmuNEG)
    #newsigPOS=1/(1/restsigPOS+1/cavsigPOS-1/dsbPOS)
    #newsigNEG=1/(1/restsigNEG+1/cavsigNEG-1/dsbNEG)
    #newmuPOS=(restmuPOS/restsigPOS+cavmuPOS/cavsigPOS-lpriorPOS/dsbPOS)*newsigPOS
    #newmuNEG=(restmuNEG/restsigNEG+cavmuNEG/cavsigNEG-lpriorNEG/dsbNEG)*newsigNEG
    newsigPOS=1/(1/restsigPOS+1/cavsigPOS)
    newsigNEG=1/(1/restsigNEG+1/cavsigNEG)
    newmuPOS=(restmuPOS/restsigPOS+cavmuPOS/cavsigPOS)*newsigPOS
    newmuNEG=(restmuNEG/restsigNEG+cavmuNEG/cavsigNEG)*newsigNEG
    lambsigtdayPOS=1/(1/newsigPOS-1/cavsigPOS);lambsigtdayNEG=1/(1/newsigNEG-1/cavsigNEG)
    lambmutdayPOS=(newmuPOS/newsigPOS-cavmuPOS/cavsigPOS)*lambsigtdayPOS;lambmutdayNEG=(newmuNEG/newsigNEG-cavmuNEG/cavsigNEG)*lambsigtdayNEG
    lambsigPOS=newsigPOS;lambsigNEG=newsigNEG
    lambmuPOS=newmuPOS;lambmuNEG=newmuNEG
    #
    #strand based bs
    #
    cavsigPOS=1/(1/lambsigPOS-1/lambsigtstrPOS);cavsigNEG=1/(1/lambsigNEG-1/lambsigtstrNEG)
    cavmuPOS=(lambmuPOS/lambsigPOS-lambmutstrPOS/lambsigtstrPOS)*cavsigPOS
    cavmuNEG=(lambmuNEG/lambsigNEG-lambmutstrNEG/lambsigtstrNEG)*cavsigNEG
    #
    restmuPOS=lpriorPOS-(cavmuNEG-lpriorNEG)*ostreg
    restmuNEG=lpriorNEG-(cavmuPOS-lpriorPOS)*ostreg
    restsigPOS=1/(ostprec)+(ostreg^2)*cavsigNEG
    restsigNEG=1/(ostprec)+(ostreg^2)*cavsigPOS
    #
    newsigPOS=1/(1/restsigPOS+1/cavsigPOS);newsigNEG=1/(1/restsigNEG+1/cavsigNEG)
    newmuPOS=(restmuPOS/restsigPOS+cavmuPOS/cavsigPOS)*newsigPOS
    newmuNEG=(restmuNEG/restsigNEG+cavmuNEG/cavsigNEG)*newsigNEG
    lambsigtstrPOS=1/(1/newsigPOS-1/cavsigPOS);lambsigtstrNEG=1/(1/newsigNEG-1/cavsigNEG)
    lambmutstrPOS=(newmuPOS/newsigPOS-cavmuPOS/cavsigPOS)*lambsigtstrPOS;lambmutstrNEG=(newmuNEG/newsigNEG-cavmuNEG/cavsigNEG)*lambsigtstrNEG
    lambsigPOS=newsigPOS;lambsigNEG=newsigNEG
    lambmuPOS=newmuPOS;lambmuNEG=newmuNEG
    #
    itct=itct+1
  }
  ttot=difftime(Sys.time(),at,units='secs')
  rpt=list(lambmuPOS=lambmuPOS,lambmuNEG=lambmuNEG,lambsigPOS=lambsigPOS,lambsigNEG=lambsigNEG,
           lambsigtPOS=lambsigtPOS,lambmutPOS=lambmutPOS,
           lambsigtdatPOS=lambsigtdatPOS,lambmutdatPOS=lambmutdatPOS,
           lambsigtdayPOS=lambsigtdayPOS,lambmutdayPOS=lambmutdayPOS,
           lambsigtNEG=lambsigtNEG,lambmutNEG=lambmutNEG,
           lambsigtdatNEG=lambsigtdatNEG,lambmutdatNEG=lambmutdatNEG,
           lambsigtdayNEG=lambsigtdayNEG,lambmutdayNEG=lambmutdayNEG)
  grds=list(posgr=newlambPOS[[3]],neggr=newlambNEG[[3]])
  list(lambmuPOS,lambmuNEG,lambsigPOS,lambsigNEG,grds,rpt,newlambPOS[[4]],newlambNEG[[4]],tpn,tmvfft,ttot)
}


#load('tmp/callcommon.RData')

#if lpri exists, load it.

#else load callcommon
load('/dnase/params.RData')

load(paste(tmpfile,'callcommon-bg.RData',sep=''))

interleave<-function(x){g=matrix(x,length(x)/2,2);as.vector(t(g))}
bindoffset=flank
load(paste(pwmout,'outfile-',chrstr[1],'-',1,'.RData',sep=''))
pwmbatchsz=length(pwmscores)

fconn=file(paste(datain,chrstr[chr],'datin.txt',sep=''),open='rt')
offsets=c(0,scan(paste(datain,chrstr[chr],'datin.txt.index',sep=''),quiet=T)+1)
seekpos=cumsum(offsets)[(spos-1)*pwmbatchsz+1]
g=seek(fconn,where=seekpos)
for(ppar in spos:epos){
  print(ppar)
  #print(ppar)
  load(paste(pwmout,'outfile-',chrstr[chr],'-',ppar,'.RData',sep=''))
  #bgset=list()
  #grset=list()
  fgset=list()
  at<-Sys.time()
  for(i in 1:length(pwmscores)){
    str=readLines(fconn,1)
    #print(i)
    #ptemp=pwmscores[[i]]
    dtest=t(decodeRLE(str,numdays))
    din=t(cbind(t(dtest[,1:numdays]),t(dtest[,-(1:numdays)])))
    din=din[,exptsub]
    if(FALSE){
    }else{
    #lpri=t(sapply(1:numdays,function(i){rep(marginals[1,i],nrow(din))}))
    #spri=t(sapply(1:numdays,function(i){rep(marginals[2,i],nrow(din))}))
      lpri=t(sapply(exptsub,function(i){rep(marginals[1,i],nrow(din))}))
      spri=t(sapply(exptsub,function(i){rep(marginals[2,i],nrow(din))}))
    }
    bg=fitBG(din,lpri,spri)
    rpn=bg[1:4]
    ucr=uconv(rep(1,nrow(dtest)),200)
    smrate=apply(dtest[,exptsub]+dtest[,exptsub+numdays],2,function(i){signif(uconv(i,200)/ucr,digits=3)})
    rpround=lapply(rpn,signif,digits=4)
    fgset[[i]]=list(rpround=rpround,smrate=smrate)
    rm(bg);rm(dtest);gc()
    #bgset[[i]]=list(rpround=rpround,smrate=smrate)
    #grset[[i]]=list(grround=lapply(grn,signif,digits=3))
  }
  rt=Sys.time()-at;
  save(fgset,rt,file=paste(bgout,chrstr[chr],'-',ppar,'.RData',sep=''),compress='bzip2')
  #save(bgset,rt,file=paste('bgout/chr-',chr,'-',ppar,'.RData',sep=''),compress='bzip2')
  #save(grset,rt,file=paste('bgout/chr-',chr,'-',ppar,'-grd.RData',sep=''),compress='bzip2')
}
close(fconn)


