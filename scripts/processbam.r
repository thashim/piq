const_quantile_selection=0.95
const_window_sz=1000
const_padding=500  #must be >200... flank bp added to every window edge
const_maxcut=15
const_numbin=3
const_blockmax=10000

################################
################################
################################

#bamlist=list.files('../input/bams/',pattern='.bam$',full.name=T)
#bamn=list.files('../input/bams/',pattern='.bam$')

#implicitely requires .bai file (same name, .bam.bai)
bamlist=c('/cluster/cwo/dnase_seq/bams/PPG_D0_HBG3_50-100.bwa.mapq20.mm10.bam',
          '/cluster/cwo/dnase_seq/bams/PPG_D4_HBG3_50-100.bwa.mapq20.mm10.bam',
          '/cluster/cwo/dnase_seq/bams/PPG_D6_HBG3_50-100.bwa.mapq20.mm10.bam',
          '/cluster/cwo/dnase_seq/bams/PPG_D4_iNIL_50-100.bwa.mapq20.mm10.bam')
bamn=c('D0_HBG3',
       'D4_HBG3',
       'D6_HBG3',
       'D4_iNIL')


##############################



rclist=list.files('../input/data/',pattern='.csv$',full.name=T)

source("http://bioconductor.org/biocLite.R")
biocLite("Rsamtools",suppressUpdates=T)
require('Rsamtools')
require('GenomicRanges')


print('loading bams')
allsbPLUS=lapply(bamlist,function(i){
  print(i)
  sb=readBamGappedAlignments(i,param=ScanBamParam(flag=scanBamFlag(isUnmappedQuery=F,isMinusStrand=F,isDuplicate=F,isValidVendorRead=T)))
})

allsbMINUS=lapply(bamlist,function(i){
  print(i)
  sb=readBamGappedAlignments(i,param=ScanBamParam(flag=scanBamFlag(isUnmappedQuery=F,isMinusStrand=T,isDuplicate=F,isValidVendorRead=T)))
})

#from bam generates an input params for PIQ -- correspondence of experiment number to name
writeLines(bamn,'../input/data/exptlist.txt')   

#from bam generates an input param for PIQ - correspondence of chrom sequential number to chrom string (eg X->21)

chrnoms=unique(do.call(c,c(lapply(allsbPLUS,function(i){levels(seqnames(i))}),lapply(allsbMINUS,function(i){levels(seqnames(i))}))))

if(length(rclist)==0){
  #generate candidate coord set
print('computing 1kb coverage')
coverageP=sapply(allsbPLUS,function(sb){
  gr=GRanges(seqnames(sb),IRanges(floor(end(sb)/const_window_sz+1),width=1))
  cgr=coverage(gr)
})
coverageM=sapply(allsbMINUS,function(sb){
  gr=GRanges(seqnames(sb),IRanges(floor(end(sb)/const_window_sz+1),width=1))
  cgr=coverage(gr)
})

csum=RleList(lapply(chrnoms,function(i){
    scp=sapply(coverageP,function(j){j[[i]]})
    scm=sapply(coverageM,function(j){j[[i]]})
    scpi=!sapply(scp,is.null)
    scmi=!sapply(scm,is.null)
    tr=0
    if(sum(scpi)>0){
        tr=tr+Reduce('+',scp[scpi])
    }
    if(sum(scmi)>0){
        tr=tr+Reduce('+',scm[!sapply(scm,is.null)])
    }
}))

#csum=Reduce('+',coverageP)+Reduce('+',coverageM)
runvals=runValue(csum[[1]])
qtl=quantile(runvals,const_quantile_selection)
selection=csum>=qtl

#only re-generate rcoords if there is no user specified rcoord.
if(!any(sapply(chrnoms,function(i){file.exists(paste0('../input/data/',i,'rcoord.csv'))}))){

rcoords=lapply(selection,function(i){
  starts=cumsum(c(0,runLength(i)))[-(length(runLength(i))+1)]
  startloc=starts[runValue(i)]
  widths=runLength(i)[runValue(i)]
  rci=cbind((startloc)*const_window_sz+1,(startloc+widths)*const_window_sz)
  if(nrow(rci)>0){
  lss=lapply(1:nrow(rci),function(j){
    ts=seq(rci[j,1],rci[j,2],by=const_blockmax)
    cbind(ts,c(ts[-1]-1,rci[j,2]))
  })
  do.call(rbind,lss)
  }else{
    rci}
})
names(rcoords)=chrnoms

rccs=sapply(rcoords,nrow)>const_numbin

writeLines(chrnoms[rccs],'../input/data/chrlist.txt')

for(chr in which(rccs)){#1:length(rcoords)){
  rcoords[[chr]][rcoords[[chr]]<501]=501
}
#take care of boundary conditions
#for(chr in which(sapply(csum,length)*const_window_sz<=sapply(rcoords,max))[rccs]){
for(chr in which(rccs)){
  print(chr)
  nr=nrow(rcoords[[chr]])
  if(rcoords[[chr]][nr,2]-rcoords[[chr]][nr,1]>const_window_sz){
    rcoords[[chr]][nrow(rcoords[[chr]]),2]=rcoords[[chr]][nrow(rcoords[[chr]]),2]-const_window_sz
  }else{
    rcoords[[chr]]=rcoords[[chr]][-nr,]
  }
}

   for(i in which(rccs)){#1:length(rcoords)){
     #for each chrom generates a file indicating windows (excluding flanks)
     write.csv(rcoords[[i]],file=paste0('../input/data/',names(rcoords)[i],'rcoord.csv'))
   }
}
}

#from bam generates an input for PIQ indicating max per-base # reads
writeLines(format(const_maxcut),'../input/data/maxcut.txt')  


require(Matrix)
chrset=readLines('../input/data/chrlist.txt')
for(i in 1:length(chrset)){
  print(chrset[i])
  rcoord=read.csv(paste('../input/data/',chrset[i],'rcoord.csv',sep=''))[,2:3]
  allplus=do.call(rbind,lapply(1:length(allsbPLUS),function(j){
      ss=rle(start(allsbPLUS[[j]][seqnames(allsbPLUS[[j]])==chrset[i]]))
      if(length(ss$values)>0){
            cbind(j,ss$values,ss$lengths)
      }else{
          integer(0)
      }
  }))
  allminus=do.call(rbind,lapply(1:length(allsbMINUS),function(j){
    ss=rle(end(allsbMINUS[[j]][seqnames(allsbMINUS[[j]])==chrset[i]]))
    if(length(ss$values)>0){
        cbind(j,ss$values,ss$lengths)
    }else{
        integer(0)
    }
  }))
  smplus=sparseMatrix(i=allplus[,2],j=allplus[,1],x=allplus[,3],dims=c(max(max(allplus[,2]),max(rcoord)+const_padding),max(allplus[,1])))
  smminus=sparseMatrix(i=allminus[,2],j=allminus[,1],x=allminus[,3],dims=c(max(max(allminus[,2]),max(rcoord)+const_padding),max(allminus[,1])))
  textbuf=list()
  linds=lapply(1:nrow(rcoord),function(rcd){
    st=rcoord[rcd,1]-const_padding
    ed=rcoord[rcd,2]+const_padding
    st:ed
  })
  winds=do.call(c,lapply(1:nrow(rcoord),function(rcd){
    st=rcoord[rcd,1]-const_padding
    ed=rcoord[rcd,2]+const_padding
    st:ed
  }))
  stset=cumsum(c(0,sapply(linds,length)[-length(linds)]))+1
  edset=cumsum(sapply(linds,length))
  allplus=as.matrix(smplus[do.call(c,linds),])
  allminus=as.matrix(smminus[do.call(c,linds),])
  allplus[allplus>const_maxcut]=const_maxcut
  allminus[allminus>const_maxcut]=const_maxcut
  if(ncol(allplus)<length(bamlist)){
      allplus=cbind(allplus,matrix(0,nrow(allplus),length(bamlist)-ncol(allplus)))
  }
  if(ncol(allminus)<length(bamlist)){
      allminus=cbind(allminus,matrix(0,nrow(allminus),length(bamlist)-ncol(allminus)))
  }
  for(rcd in 1:nrow(rcoord)){
    estring=as.vector(rbind(allplus[stset[rcd]:edset[rcd],],allminus[stset[rcd]:edset[rcd],]))
    restring=rle(estring)
    textbuf[[rcd]]=paste(paste(restring$lengths,collapse=','),paste(restring$values,collapse=','),sep=';')
  }
  #from bam generates ascii encoded run-length encoded (compressed) read pileup for PIQ 
  writeLines(unlist(textbuf),paste0('../input/data/',chrset[i],'datin.txt',sep=''))
  gc()
}






