
require('rtracklayer')
ss=sapply(list.files('dat',pattern='UCSC'),function(i){
  print(i)
  is=import(paste0('dat/',i))
  us=unlist(ranges(is))
  GRanges(space(is),IRanges(start(us),end(us)))
})
require('GenomicRanges')
r1=read.csv('/dnase3/bt2remall/calltmp/4/697.tfout.gz')
r2=read.csv('/dnase3/bt2remall/calltmp/4/697.tfout.control.gz')
r1=rbind(as.matrix(r1),as.matrix(r2))
gh=GRanges(paste0('chr',c(1:19,'X','Y'))[r1[,1]],IRanges(start=abs(r1[,6]),width=1))
ssall=sapply(ss,function(i){gh%in%i})
colnames(ssall)=list.files('dat',pattern='UCSC')
heatmap(t(ssall)+1,scale='none',Colv=NA)

bulllist=c(97,703,914,930,986)
for(day in 1:14){
  for(i in bulllist){
    submitJob('makeindtffile.r',c(day,1),T,i,i+1,2)
  }
}
for(i in bulllist){
  day=1
  allday=sapply(c(1,8:14),function(day){
    daymat=do.call(rbind,lapply(1:2,function(chr){
      print(chr)
      load(paste0(callsum,day,'-',chr,'-',i,'-factmat.RData'))
      chrid=sapply(chrc,function(i){i$values[2]})
      bigmat=t(sapply(which(chrid==i),function(i){as.integer(inverse.rle(chrc[[i]])[-(1:3)])}))
    }))
    pss=do.call(c,lapply(1:2,function(chr){
      print(chr)
      load(paste0(callsum,day,'-',chr,'-',i,'-factmat.RData'))
      chrid=sapply(chrc,function(i){i$values[2]})
      sapply(chrc[which(chrid==i)],function(i){i$values[3]})    
    }))
    opss=order(pss)
    #rowSums(daymat[opss,])
    c(colSums(daymat[opss[1:5000],]),colSums(daymat[rev(opss)[1:5000],]))
  })
}

pdf(paste0(summ,'test.pdf'))
#image(t(allday)/apply(allday,2,median))
plot(allday[,1]/median(allday[1:1000,1]),type='l')
for(i in 2:ncol(allday)){
  points(allday[,i]/median(allday[1:1000,i]),type='l',col=rgb(0,0,0,alpha=0.1))
}
dev.off()


submitJob('makeindtffile.r',c(1,10),T,1,maxtf,10)
#install.packages("CENTIPEDE", repos="http://R-Forge.R-project.org") 
library(CENTIPEDE) 
submitJob('runcentipede.r',c(1),T,1,maxtf,1)


while(length(system('/opt/sge6/bin/linux-x86/qstat',intern=T))>2){print('waiting for sge to clear');Sys.sleep(60)}
sendmail('tatsu23456@gmail.com',subject='parse-step done',message='rdb is done',password='rmail')


for(i in 1:length(exptsub)){
  print(i)
  submitJob('fullparse.r',c(i,10),T,1,maxtf,10)
}



load(paste(pwmout,'commonfiles.RData',sep=''))
load(paste(kernout,'profsbg.RData',sep=''))

pdf(paste(summ,'profiles.pdf',sep=''))
ylim=c(-0.5,0.5)
for(fnum in 1:maxtf){
  colset=rgb(r=seq(0,1,length=numdays),g=0,b=0)
  plot((-wsize/2)+(1:wsize),profs[[1]][[1]][fnum,(wsize+1):(2*wsize)]+profs[[1]][[3]][fnum,(wsize+1):(2*wsize)],type='l',ylim=ylim,col=colset[1],main=names(lgpwm[fnum]),ylab='log change in coverage at binidng sites',xlab='distance from motif center')
  #for(i in 2:numdays){
  #  points(exp(profs[[i]][[3]][fnum,(wsize+1):(2*wsize)]),type='l',col=colset[i])
  #}
}
dev.off()

write.csv(data.frame(names=names(lgpwm),profs[[9]][[3]][,(wsize+1):(2*wsize)]),file=paste0(summ,'profiles.csv'))

submitJob('recalcprof.r',c(1,wsize,maxtf),T,1,maxtf,1)
source('makeRefineKernelFromBG.R')

for(chr in 1:chrlen){
  print(chr)
  lf=list.files(pwmout,pattern=paste('-',chr,'-',sep=''))
  batches=makeBatch(1,length(lf),by=20)
  for(i in 1:length(batches)){
    submitJob('dorefinecallqscript.r',c(chr,numdays,min(batches[[i]]),max(batches[[i]])))
  }
}

for(i in 1:length(exptsub)){
  print(i)
  submitJob('fullreparse.r',c(i,10),T,1,maxtf,10)
}


###
# allfact(?)

load(paste(pwmout,'commonfiles.RData',sep=''))
load(paste(kernout,'profsbg.RData',sep=''))

for(j in 1:maxtf){
  print(j)
  ccmat=do.call(rbind,lapply(1:3,function(chr){
    lf=list.files('pwmout',pattern=paste('-',chr,'-',sep=''))
    batches=makeBatch(1,length(lf),by=2)
    do.call(rbind,lapply(1:length(batches),function(i){
      load(paste(callout,'chr-call-',chr,'-',min(batches[[i]]),'-',j,'.RData',sep=''))
      callmat
    }))}))
  save(ccmat,file=paste(callout,'allfact-',j,'.RData',sep=''))
}

#####
# factor cutoffs

load(paste(pwmout,'commonfiles.RData',sep=''))
load(paste(kernout,'profsbg.RData',sep=''))

softtr<-function(x,t){
  y=abs(x)-t
  y[y<0]=0
  y*sign(x)
}

hist(log(abs(profs[[1]][[1]])),200)
cut=median(log(abs(profs[[1]][[1]])))
strs=softtr(profs[[1]][[1]],exp(cut))
plot(strs[141,],type='l')
plot(rowSums(strs!=0))
hist(rowSums(strs!=0),200)

write.csv(data.frame(name=names(lgpwm),informbase=rowSums(strs!=0),use=rowSums(strs!=0)>200),row.names=F,file=paste0(summ,'pwmcut.csv'))

plot(sort(rowSums(strs!=0)),ylab='number of informative bases')





######
# Factor clustering

require('Rcpp')
require('inline')
mytabST2='
Rcpp::NumericVector cvec1(vec1);
Rcpp::NumericVector cvec2(vec2);
Rcpp::NumericVector cmaxval(maxval);
Rcpp::NumericVector toret(cmaxval[0]);
for(int i =0; i < cvec1.size();i++){
int a = cvec1[i];
toret[a-1]+=cvec2[i];
}
return toret;'
mytab2 <- cxxfunction(signature(vec1='numeric',vec2='numeric',maxval='numeric'),mytabST2,plugin='Rcpp')

chr=1;ppar=1;
ais=matrix(0,1331,1331)
for(ppar in 1:10){
  print(ppar)
  load(paste(pwmout,'outfile-',chr,'-',ppar,'.RData',sep=''))
  require('IRanges')
  for(i in 1:length(pwmscores)){
    ir=flank(sort(IRanges(start=abs(pwmscores[[i]][,2]),width=1,names=pwmscores[[i]][,1])),width=20,both=T)
    hsite=unique(pwmscores[[i]][,1])#which((1:tfmax)%in%pwmscores[[i]][,1])
    ira=flank(IRanges(start=abs(pwmscores[[i]][,2]),width=1),width=20,both=T)
    for(j in hsite){
      cos=countOverlaps(ir,ira[pwmscores[[i]][,1]==j])
      mt=mytab2(as.double(names(cos)),cos,1331)
      #tis=table(as.double(names(ir)[ir%in%ira[pwmscores[[i]][,1]==j]]))
      ais[j,]=ais[j,]+mt#as.double(tis)
    }
  }
}

rsm=diag(1/(sqrt(diag(ais+1e-10))))
cormat=rsm%*%(ais+1e-10)%*%rsm
hcc=hclust(as.dist(1-cormat),method='complete')
plot(hcc)
heatmap(cormat,Colv=as.dendrogram(hcc),Rowv=as.dendrogram(hcc))

fclust=cutree(hcc,h=0.1)
clusters=lapply(1:max(fclust),function(i){
  which(fclust==i)
})

factorclust=sapply(clusters,paste,collapse=',')
factorclustname=sapply(clusters,function(i){paste(names(lgpwm[i]),collapse=',')})
writeLines(factorclust,paste0(summ,'motifclust.txt'))
writeLines(factorclustname,paste0(summ,'motifclustnames.txt'))

fclust=cutree(hcc,h=0.2)
clusters=lapply(1:max(fclust),function(i){
  which(fclust==i)
})

factorclust=sapply(clusters,paste,collapse=',')
factorclustname=sapply(clusters,function(i){paste(names(lgpwm[i]),collapse=',')})
writeLines(factorclust,paste0(summ,'motifclust-perm.txt'))
writeLines(factorclustname,paste0(summ,'motifclustnames-perm.txt'))


###
#expression

load(paste0(kernout,'profsbgdr.RData'))
for(refexp in 1:numdays){
  sc1=sqrt(apply(discrs[[refexp]][[1]],1,crossprod))
  dexpr=sapply(1:length(discrs),function(i){
    print(i)
    shift=colMeans(discrs[[refexp]][[1]]-discrs[[i]][[1]])
    refmat=t(t(discrs[[i]][[1]])+shift)
    sc2=sqrt(apply(refmat,1,crossprod))
    so=svd((t(discrs[[refexp]][[1]])/mean(sc1))%*%(refmat/mean(sc2)))
    rot=so$u%*%t(so$v)
    mag=sqrt(apply(refmat%*%t(rot),1,crossprod))
    log(mag/mean(sc2))-log(sc1/mean(sc1))
    #mag/mean(sc2)
  })
  rownames(dexpr)=names(lgpwm)
  colnames(dexpr)=estr
  write.csv(dexpr,file=paste0(summ,refexp,'-dexpr.csv'))
}
exprstr=readLines('RS_mar2012_dnaseseqtissue_arrayvalues.csv')
exprspl=strsplit(exprstr[-c(1,length(exprstr))],',')
probename=sapply(exprspl,function(i){i[1]})
lgname=tolower(sapply(strsplit(names(lgpwm)[1:1313],' '),function(i){i[2]}))
pnscan=sapply(1:length(probename),function(i){print(i);match(tolower(probename[i]),lgname)})
pnhits=which(!is.na(pnscan))
pntarg=pnscan[pnhits]
exprs=t(sapply(exprspl[pnhits],function(i){
  as.double(i[c(2,4,6,7)])
}))
dnexpr=dexpr[pntarg,c(refexp,8,9,13)]
lexpr=log(exprs)-log(exprs[,1])


###########
# ROC PLOTS

getROC <- function(eval,truth){
  reval=rank(eval)
  t(sapply(seq(min(reval),max(reval),length=100),function(i){
    c(sum((1-truth)[reval>i])/sum(1-truth),
      sum(truth[reval>i])/sum(truth)
    )
  }))
}

load(paste0(tmpfile,'commonfiles.RData'))
fstab=matrix(c(141,"dat/YL_ES_Oct4_2_GPS_significant.txt",'Oct4',138,"dat/ES_Ctcf_GEM_events.txt",'CTCF',145,"dat/ES_Zfx_GEM_events.txt","zfx",140,"dat/ES_Esrrb_GEM_events.txt",'Esrrb',155,"dat/ES_n-Myc_GEM_events.txt",'n-Myc',151,"dat/ES_Klf4_GEM_events.txt",'Klf4',146,"dat/ES_c-Myc_GEM_events.txt","c-Myc"),ncol=3,byrow=T)
tfmax=length(lgpwm)

pdf(paste0(summ,'PIQunittest.pdf'))
for(fnum in 1:nrow(fstab)){
  print(fnum)
  j=as.double(fstab[fnum,1])
  tfnum=j
  ccmat=do.call(rbind,lapply(1:chrlen,function(chr){
    print(chr)
    lf=list.files(pwmout,pattern=paste('-',chr,'-',sep=''))
    batches=makeBatch(1,length(lf),by=20)
    rcoord=read.csv(paste(datain,'chr-',chr,'-rcoord.csv',sep=''))[,3:4]
    do.call(rbind,lapply(1:length(batches),function(i){
      tk=floor((tfnum-1)/10)*10+1
      #rl = readLines(paste0(calltmp,0,'/',chr,'-',min(batches[[i]]),'-',tk,'.out.gz'))
      load(paste0(calltmp,0,'/',chr,'-',min(batches[[i]]),'-',tk,'.out'))
      rl=str
      lseq=seq(tfnum-tk+1,length(rl),by=10)
      oss=nchar(paste0(tfnum,';'))+1
      rls=strsplit(substring(rl[lseq],oss),'[;,]')
      annot=do.call(cbind,lapply(1:length(rls),function(j){
        annot=as.double(rls[[j]][nchar(rls[[j]])!=0])
        rcint=100*(min(batches[[i]])-1+j-1)
        if(length(annot)>0){
          x1=annot[seq(1,length(annot),by=3)]
          x2=annot[seq(2,length(annot),by=3)]
          x3=annot[seq(3,length(annot),by=3)]
          x4=(rcoord[rcint+x1,1]+abs(x2))#*sign(x2)
          annot=rbind(x1,x2,x3,x4)
        }else{
          annot
        }
      }))
      #dim(annot)=c(3,length(annot)/3)
      callmat=do.call(cbind,lapply(1:length(exptsub),function(day){
        #rl=readLines(paste0(calltmp,day,'/',chr,'-',min(batches[[i]]),'-',tk,'.out.gz'))
        load(paste0(calltmp,day,'/',chr,'-',min(batches[[i]]),'-',tk,'.out'))
        rl=str
        lseq=seq(tfnum-tk+1,length(rl),by=10)
        rlt=do.call(c,strsplit(substring(rl[lseq],oss),'[;,]'))
        call=as.double(rlt[nchar(rlt)!=0])
        dim(call)=c(2,length(call)/2)
        t(call)
      }))
      cbind(chr,t(annot),callmat)
    }))}))
  pos = read.table(fstab[fnum,2],sep='\t',header=T,skip=1)
  factorname=fstab[fnum,3]
  pssms=ccmat[,4]
  #
  posvec=strsplit(format(pos[,1]),":")
  frames=data.frame(chrom=sapply(posvec,function(i){i[1]}),pos=sapply(posvec,function(i){as.integer(i[2])}))
  bdelt=do.call(c,lapply(1:19,function(chr){
    fsub=frames[frames[,1]==chr,2]
    dsub=ccmat[ccmat[,1]==chr,5]
    sfsub = sort(fsub)
    fis=findInterval(dsub,sfsub)
    fis[fis==0]=1
    abs(sfsub[fis]-dsub)
  }))
  omax=10
  evalpts=seq(0,1,length=1000)
  allmat=matrix(0,1000,length(exptsub)*4+1)
  allmat[,1]=evalpts
  for(ofs in 0:(length(exptsub)-1)){
    ccsub=ccmat[ccmat[,1]%in%(1:19),]
    r1=getROC(ccsub[,4],as.double(bdelt<omax))
    r2=getROC(ccsub[,7+ofs*2],as.double(bdelt<omax))
    r3=getROC(ccsub[,6+ofs*2],as.double(bdelt<omax))
    plot(r1,type='l',col='purple',main=paste('factor:',fstab[fnum,3],' expt:',substr(estr,8,100)[ofs+1],sep=''),xlab='False Positive',ylab='True Positive')
    points(r2,type='l',col='red')
    #points(r3,type='l',lty=2)
    bdun=isoreg(ccsub[,7+ofs*2],ccsub[,6+ofs*2])$yf[rank(ccsub[,7+ofs*2])]
    psun=isoreg(ccsub[,4],ccsub[,6+ofs*2])$yf[rank(ccsub[,4])]*5+ccsub[,4]*0.2
    r4=getROC(ccsub[,6+ofs*2]/2+bdun+psun,as.double(bdelt<omax))
    points(r4,type='l',lty=1)
    mout=cbind(approx(r1,xout=evalpts,rule=0)$y,approx(r2,xout=evalpts,rule=0)$y,approx(r3,xout=evalpts,rule=0)$y,approx(r4,xout=evalpts,rule=0)$y)
    allmat[,4*(ofs)+2:5]=mout
  }
  colnames(allmat)=c('falsepos',as.vector(outer(substr(estr,8,100),c('.pssm','.counts','.profile','.piq'),paste0)))
  write.csv(allmat,file=paste0(summ,fstab[fnum,3],'.roc.csv'))
}
dev.off()


#bulldozers
bldz=rowSums(discrs[[2]][[1]])
pdf('summaries/bulldozer.pdf',7,10)
par(mar=c(2,20,1,1))
bp=barplot(-sort(-bldz)[2:50],names.arg=names(lgpwm)[order(-bldz)[2:50]],horiz=T,yaxt='n')
axis(2,at=bp,labels=names(lgpwm)[order(-bldz)[2:50]],las=2)
par(mar=c(2,2,2,2))
hist(bldz,200)
dev.off()


#daydaycor
load('kernout/kernreduced.RData')
ddcov=matrix(0,numdays,numdays)
for(dayone in 1:numdays){
  for(daytwo in dayone:numdays){
    ind=(2.0*numdays-dayone)*(dayone-1)/2.0+(daytwo)
    x=covSS[ind,1]*sqrt(marginals[2,dayone]*marginals[2,daytwo])
    ddcov[dayone,daytwo]=x
    ddcov[daytwo,dayone]=x
  }
}
require('glasso')
sdcov=glasso(ddcov,0.2,maxit=1000)$wi
precdaydz=diag(diag(1/sdcov))%*%sdcov
diag(precdaydz)=0
dayprec=diag(sdcov)

enames=estr#c('d0','d0rep1','d0rep2','d3','d5','d5mes','d6','d6panc','d7')
require('igraph')
g=graph.adjacency(sdcov<0,mode='undirected')
sdnorm=diag(1/sqrt(diag(sdcov)))%*%sdcov%*%diag(1/sqrt(diag(sdcov)))
V(g)$label=enames
E(g)$label=format(-sdnorm[get.edgelist(g)],digits=1)
E(g)$weight=-1/sdnorm[get.edgelist(g)]
plot(g,layout=shortest.paths(g))
ll=layout.fruchterman.reingold(g,list(weights=1/E(g)$weight*10))
pdf(paste0(summ,'exppcor.pdf'))
plot(g,layout=ll,edge.width=1/E(g)$weight*20)
dev.off()


load(paste0(kernout,'profsbgdr.RData'))

pssmbldz=sapply(1:maxtf,function(j){
  print(j)
  load(paste('/dnase2/callout/allfact-',j,'.RData',sep=''))
  ofs=c(1)
  if(nrow(ccmat)>1){#length(readLines(paste0(calltmp,'1/',j,'.tfout.gz')))>1 & length(readLines(paste0(calltmp,'1/',j,'.tfout.control.gz')))>1){
    #rs=rbind(as.matrix(read.csv(paste0(calltmp,'1/',j,'.tfout.gz'))),as.matrix(read.csv(paste0(calltmp,'1/',j,'.tfout.control.gz'))))
    cor(ccmat[,5+ofs],ccmat[,4])
    #cor(as.double(rs[,7]),as.double(rs[,5]),use='complete.obs')
  }else{
    0
  }
})
#pssmbldz=do.call(cbind,pssmbldz)

load(paste0(kernout,'profsbgdr.RData'))
load(paste0('/dnase3/bt2promrev/kernout/','profsbgdr.RData'))

blold=read.csv('/dnase3/bt2promrev/summaries/2dbuldz.csv')[,3]
lgg=match(as.character(read.csv('/dnase3/bt2promrev/summaries/2dbuldz.csv')[,2]),names(lgpwm))

bldz2=rowSums(discrs[[1]][[1]])
plot(bldz2[lgg],blold)
write.csv(data.frame(names(lgpwm),bldz2),'bldz2.csv')

pio2=rowSums(discrs[[8]][[2]])

pdf(paste0(summ,'pio.pdf'))
hist(pio2,200)
dev.off()
write.csv(data.frame(name=names(lgpwm),pio=pio2),file=paste0(summ,'pio.csv'))

pdf(paste0(summ,'buldz.pdf'))
hist(bldz2,200)
dev.off()

cuttop=1
load(paste0('/dnase3/k562/kernout/','profsbgdr.RData'))
dc=discrs[[1]][[1]]
dc[dc>cuttop]=cuttop
dc[dc< -cuttop]=-cuttop
bldzk562=rowSums(dc)
load(paste0('/dnase3/bt2remall/kernout/','profsbgdr.RData'))
dc=discrs[[1]][[1]]
dc[dc>cuttop]=cuttop
dc[dc< -cuttop]=-cuttop
bldz2=rowSums(dc)
plot(cbind(bldz2,bldzk562),xlim=c(-2,12),ylim=c(-2,12))

write.csv(data.frame(bldz2,bldzk562),file=paste0('bldzcomp.csv'))


cbind(read.csv(paste0(summ,'bldz2.csv'))[,3],read.csv('/dnase3/k562/summaries/bldz2.csv')[,3])



####
# Social index

#motclus=as.double(sapply(strsplit(readLines(paste0('/dnase3/bt2promrev/summaries/motifclust-perm.txt')),','),function(i){i[1]}))
#motcut=read.csv('/dnase3/bt2promrev/summaries/pwmcut.csv')
motclus=1:1331#motclus[motclus%in%which(motcut[,3])]
require('GenomicRanges')
allbds=lapply(motclus,function(i){
  print(i)
  rcv=read.csv(paste0(calltmp,'1/',i,'.tfout.gz'))
  as.integer(abs(rcv[rcv[,1]==1,6]))[0:min(Inf,sum(rcv[,1]==1))]
})

allbds3=lapply(motclus,function(i){
  print(i)
  rcv=read.csv(paste0(calltmp,'8/',i,'.tfout.gz'))
  as.integer(abs(rcv[rcv[,1]==1,6]))[0:min(Inf,sum(rcv[,1]==1))]
})

overlaps=sapply(1:length(allbds),function(i){c(sum(allbds[[i]]%in%allbds3[[i]]),length(allbds[[i]]))})

allcrd=do.call(c,allbds)
covall=coverage(IRanges(start=allcrd,width=1))
covall[covall>2]=2

wids=199
iwid=0
vmall=sapply(1:length(motclus),function(j){
  print(j)
  #vs=Views(covall,allbds[[j]]-wids,allbds[[j]]+wids)
  vs=Views(covall,allbds[[j]]-wids,allbds[[j]]-iwid)
  vs2=Views(covall,allbds[[j]]+iwid,allbds[[j]]+wids)  
  #vmat=sapply(vs,function(i){
  #  as.double(i)
  #})
  #  sum(log(rowSums(vmat)+1))
  #sum(vmat)
  (sum(mean(vs))+sum(mean(vs2)))
})

pdf(paste0(summ,'social.pdf'))
hist(log((vmall-0*sapply(allbds,length))/sapply(allbds,length),2),100,xlab = 'expected number of binding sites within 500bp',main='social index')
#abline(v=log(((vmall-0*sapply(allbds,length))/sapply(allbds,length)),2)[match(697,motclus)])
dev.off()

write.csv(data.frame(name=names(lgpwm),social=(vmall-0*sapply(allbds,length))/sapply(allbds,length)),file=paste0(summ,'social.csv'))



require('KernSmooth')

psbldz=!is.na(pssmbldz)
ker=bkde2D(cbind(bldz2[psbldz],pssmbldz[psbldz]),c(0.05,0.02),gridsize=c(200,200))#,range.x=list(range(bldz[psbldz]),range(pssmbldz[2,psbldz])))

pdf(paste0(summ,'2dbuldz.pdf'),20,20)
image(ker$x1,ker$x2,log(ker$fhat+5e-3),xlab='counts',ylab='binding',col=topo.colors(50))#,xlim=range(ker$x1),ylim=range(ker$x2))
#points(bldz2,pssmbldz[2,],pch=20)
#text(bldz2,pssmbldz[2,],names(lgpwm))
dev.off()
write.csv(data.frame(names=names(lgpwm)[psbldz],bulldozer=bldz2[psbldz],pssm=pssmbldz[psbldz]),file=paste0('2dbuldz.csv'))

pdf(paste0(summ,'pssmhist.pdf'))
hist(pssmbldz,200,main='',xlab='percent of PiQ calls accounted for by PSSM')
dev.off()



ctsbldz=sapply(1:tfmax,function(j){
  print(j)
  load(paste('/dnase2/callout/allfact-',j,'.RData',sep=''))
  ofs=1
  if(nrow(ccmat)>1){
    cor(ccmat[,5+ofs],ccmat[,5+11+ofs])
  }else{
    0
  }
})

#ctsbldz=do.call(cbind,ctsbldz)


pdf(paste0(summ,'2dsettl.pdf'),50,50)
psbldz=!is.na(pssmbldz) & !is.na(ctsbldz)
ker=bkde2D(cbind(ctsbldz[psbldz],pssmbldz[psbldz]),c(0.02,0.02),gridsize=c(200,200))#,range.x=list(range(bldz[psbldz]),range(pssmbldz[2,psbldz])))
image(ker$x1,ker$x2,log(ker$fhat+5e-3),xlab='counts',ylab='binding',col=topo.colors(50))#,xlim=range(ker$x1),ylim=range(ker$x2))
#text(ctsbldz[2,],pssmbldz[2,],names(lgpwm))
dev.off()
write.csv(data.frame(names=names(lgpwm)[psbldz],chromatin=ctsbldz[psbldz],pssm=pssmbldz[psbldz]),file=paste0('2dsettl.csv'))


ofr=1
pdf(paste0(summ,'ctshist.pdf'),10,10)
hist(ctsbldz[ofr,],200,main='',xlab='correlation of accessibility with PiQ')
dev.off()


lghat=sapply(lgpwm,function(i){pp=t(exp(i))/colSums(exp(i));sum(log(4)+rowSums(pp*log(pp)))})
psbldz=!is.na(pssmbldz)
ker=bkde2D(cbind(lghat[psbldz],pssmbldz[psbldz]),c(0.05,0.02),gridsize=c(200,200),range.x=list(range(lghat[psbldz]),range(pssmbldz[psbldz])))

write.csv(data.frame(name=names(lgpwm),inform=lghat,pssm=pssmbldz),file=paste0('2dinform.csv'))

pdf('summaries/2dinform.pdf',20,20)
image(ker$x1,ker$x2,log(ker$fhat+5e-3),xlab='counts',ylab='binding',col=topo.colors(50))#,xlim=range(ker$x1),ylim=range(ker$x2))
#points(bldz2,pssmbldz[2,],pch=20)
text(lghat,pssmbldz[2,],names(lgpwm))
dev.off()

pfonly=which(!is.na(pssmbldz[2,]))[grep('PF',names(lgpwm)[!is.na(pssmbldz[2,])])]
pdf('summaries/2dinform-pf.pdf',20,20)
ker=bkde2D(cbind(lghat[pfonly],pssmbldz[2,pfonly]),c(0.05,0.02),gridsize=c(200,200),range.x=list(range(lghat[psbldz]),range(pssmbldz[2,psbldz])))
image(ker$x1,ker$x2,log(ker$fhat+5e-3),xlab='counts',ylab='binding',col=topo.colors(50))#,xlim=range(ker$x1),ylim=range(ker$x2))
#points(bldz2,pssmbldz[2,],pch=20)
text(lghat[pfonly],pssmbldz[2,pfonly],names(lgpwm)[pfonly])
dev.off()

pfonly=which(!is.na(pssmbldz[2,]))[grep('MA',names(lgpwm)[!is.na(pssmbldz[2,])])]
pdf('summaries/2dinform-MA.pdf',20,20)
ker=bkde2D(cbind(lghat[pfonly],pssmbldz[2,pfonly]),c(0.05,0.02),gridsize=c(200,200),range.x=list(range(lghat[psbldz]),range(pssmbldz[2,psbldz])))
image(ker$x1,ker$x2,log(ker$fhat+5e-3),xlab='counts',ylab='binding',col=topo.colors(50))#,xlim=range(ker$x1),ylim=range(ker$x2))
#points(bldz2,pssmbldz[2,],pch=20)
text(lghat[pfonly],pssmbldz[2,pfonly],names(lgpwm)[pfonly])
dev.off()

pfonly=which(!is.na(pssmbldz[2,]))[grep('PB',names(lgpwm)[!is.na(pssmbldz[2,])])]
pdf('summaries/2dinform-PB.pdf',20,20)
ker=bkde2D(cbind(lghat[pfonly],pssmbldz[2,pfonly]),c(0.05,0.02),gridsize=c(200,200),range.x=list(range(lghat[psbldz]),range(pssmbldz[2,psbldz])))
image(ker$x1,ker$x2,log(ker$fhat+5e-3),xlab='counts',ylab='binding',col=topo.colors(50))#,xlim=range(ker$x1),ylim=range(ker$x2))
#points(bldz2,pssmbldz[2,],pch=20)
text(lghat[pfonly],pssmbldz[2,pfonly],names(lgpwm)[pfonly])
dev.off()

plot(pssmbldz[1,],lghat)

# require('RColorBrewer')


while(TRUE){
  image(ker$x1,ker$x2,log(ker$fhat+5e-3),xlab='counts',ylab='binding',col=topo.colors(50))#,xlim=range(ker$x1),ylim=range(ker$x2))
  points(bldz2,pssmbldz[2,],pch=20)
  identify(bldz2,pssmbldz[2,],labels=names(lgpwm),n=1)
  identify(bldz2,pssmbldz[2,],labels=names(lgpwm),n=1)
}

load(paste0(kernout,'profsbgdr.RData'))

ofr=2
pdf('summaries/pssmbldz.pdf',10,10)
par(mar=c(2,20,1,1))
bp=barplot(-sort(-pssmbldz[ofr,])[1:50],names.arg=names(lgpwm)[order(-pssmbldz[ofr,])[1:50]],horiz=T,yaxt='n')
axis(2,at=bp,labels=names(lgpwm)[order(-pssmbldz[ofr,])[1:50]],las=2)
bp=barplot(-sort(-pssmbldz[ofr,])[1:50+50],names.arg=names(lgpwm)[order(-pssmbldz[ofr,])[1:50+50]],horiz=T,yaxt='n')
axis(2,at=bp,labels=names(lgpwm)[order(-pssmbldz[ofr,])[1:50+50]],las=2)
par(mar=c(2,2,2,2))
hist(pssmbldz[ofr,],200)
dev.off()

numdays=12



lghat=sapply(lgpwm,function(i){pp=t(exp(i))/colSums(exp(i));sum(log(4)+rowSums(pp*log(pp)))})
psbldz=!is.na(ctsbldz[2,])
ker=bkde2D(cbind(lghat[psbldz],ctsbldz[2,psbldz]),c(0.05,0.02),gridsize=c(200,200),range.x=list(range(lghat[psbldz]),range(ctsbldz[2,psbldz])))

pdf('summaries/2dinformcts.pdf',20,20)
image(ker$x1,ker$x2,log(ker$fhat+5e-3),xlab='counts',ylab='binding',col=topo.colors(50))#,xlim=range(ker$x1),ylim=range(ker$x2))
#points(bldz2,pssmbldz[2,],pch=20)
text(lghat,ctsbldz[2,],names(lgpwm))
dev.off()

e2flist=grep('E2F',names(lgpwm))

require('GenomicRanges')
gll=lapply(e2flist,function(j){
  cz=as.double(strsplit(scan(file=paste(callsum,'allfact-',j,'-day-',1,'.csv.gz',sep=''),,what=list(''),sep='\n')[[1]],',')[[1]])
  sh=scan(file=paste(callsum,'allfact-',j,'-annot.csv.gz',sep=''),what=list(0,0,0,0),sep=',')
  czs=cz>2
  GRanges(seqnames=sh[[1]][czs],ranges=IRanges(start=sh[[2]][czs],width=1),scores=cz[czs])
})

require('GenomicRanges')
glbg=lapply(e2flist,function(j){
  cz=as.double(strsplit(scan(file=paste(callsum,'allfact-',j,'-day-',1,'.csv.gz',sep=''),,what=list(''),sep='\n')[[1]],',')[[1]])
  sh=scan(file=paste(callsum,'allfact-',j,'-annot.csv.gz',sep=''),what=list(0,0,0,0),sep=',')
  czs=sample(1:length(cz),sum(cz>2))
  GRanges(seqnames=sh[[1]][czs],ranges=IRanges(start=sh[[2]][czs],width=1))
})

sss=sapply(1:length(gll),function(i){
  sapply(1:length(gll),function(j){length(findOverlaps(gll[[j]],flank(gll[[i]],20,both=T)))
  })
})

sssbg=t(sapply(1:length(gll),function(i){
  sapply(1:length(gll),function(j){length(findOverlaps(gll[[j]],flank(glbg[[i]],20,both=T)))
  })
}))
e2fnoms=sapply(strsplit(names(lgpwm)[e2flist],' '),function(i){i[2]})
for(i in 1:(length(gll)-1)){
  for(j in (i+1):length(gll)){
    fos=findOverlaps(gll[[i]],flank(gll[[j]],width=20,both=T))
    pur=punion(gll[[i]][queryHits(fos)],gll[[j]][subjectHits(fos)],fill.gap=TRUE)
    sca=score(gll[[i]])[queryHits(fos)]+score(gll[[j]])[subjectHits(fos)]
    write.csv(data.frame(chr=as.double(seqnames(pur)),tf1c=start(pur),tf2c=end(pur),score=sca),file=paste('/dnase/summaries/',e2fnoms[i],'-',e2fnoms[j],'.csv',sep=''))
  }
}

###topic anal

#install.packages('lda')
require('lda')
require('GenomicRanges')
for(j in 1:maxtf){
  ghr=sapply((c(1,6)-1),function(i){
    as.double(strsplit(scan(file=paste(callsum,'allfact-',j,'-day-',i,'.csv.gz',sep=''),,what=list(''),sep='\n')[[1]],',')[[1]])
  })
  gh=rowSums(ghr>1)
  sh=scan(file=paste(callsum,'allfact-',j,'-annot.csv.gz',sep=''),what=list(0,0,0,0),sep=',')
  #czs=sample(1:length(cz),sum(cz>2))
  #GRanges(seqnames=sh[[1]][czs],ranges=IRanges(start=sh[[2]][czs],width=1))
  ghz=gh>0
  if(sum(ghz)>0){
    write.csv(data.frame(chr=sh[[1]][ghz],coord=sh[[2]][ghz],ghr[ghz,,drop=F]),file=paste(callsum,'cutfact-',j,'.csv.gz',sep=''))
  }
}

ll=lapply(1:maxtf,function(j){
  print(j)
  fn=paste(callsum,'cutfact-',j,'.csv.gz',sep='')
  if(file.exists(fn)){
    rc=read.csv(fn)
    GRanges(rc[,2],IRanges(start=rc[,3],width=1),type=rep(j,nrow(rc)))
  }else{
    NULL
  }
})




allunion=do.call(c,sapply(ll,function(i){if(!is.null(i)){as.double(values(i)[,1])}else{integer(0)}}))
boundtf=which(sapply(ll,length)>0)
allgr=do.call(c,ll[boundtf])
allfreq=table(c(1:1331,values(allgr)[,1]))-1
alltfid=values(allgr)[,1]
#save(allunion,boundtf,allgr,allfreq,ll,file=paste(tmpfile,'ldatmp.RData'),compress='gzip')
#save(alltfid,allunion,boundtf,allgr,allfreq,ll,lgpwm,file=paste(tmpfile,'ldatmp.RData'),compress='gzip')
load(paste(tmpfile,'ldatmp.RData'))
#fogr=findOverlaps(allgr,flank(allgr,width=50,both=T))

boundtf=which(sapply(ll,length)>0)
for(i in boundtf){
  submitJob('ldascript.r',i)
}

load(paste(pwmout,'commonfiles.RData',sep=''))

require('lda')
for(i in boundtf){
  load(paste(summ,i,'-ldaout.RData',sep=''))
  apply(lgs$topics,1,sort,decreasing=T)[1:20,]
  top.topic.words(lgs$topics)[1:20,]
  print()
}

fconn <- file(paste(summ,'chaperones.txt',sep=''),open='wt');close(fconn)
fconn <- file(paste(summ,'chaperones.txt',sep=''),open='at')
for(i in boundtf){
  load(paste(summ,i,'-ldaout.RData',sep=''))
  cxs=colSums(t(lgs$document_sums)/colSums(lgs$document_sums))
  xv=order(cxs,decreasing=T)
  strs=matrix(names(lgpwm)[apply(lgs$topics,1,order,decreasing=T)[1:20,]],20)[,xv]
  writeLines(c(paste('TF:',names(lgpwm)[i],paste(signif(rev(sort(cxs)),digits=4),collapse='\t'),collapse='\t',sep='\t'),apply(strs,2,paste,collapse='\t'),''),fconn)
}
close(fconn)



rowMeans(lgs$document_sums)
cor(t(lgs$document_sums))



require("Rcpp")
require("inline")
mytabST='
    Rcpp::NumericVector cvec1(vec1);
Rcpp::NumericVector cmaxval(maxval);
Rcpp::NumericVector toret(cmaxval[0]);
for(int i =0; i < cvec1.size();i++){
int a = cvec1[i];
toret[a-1]++;
}
return toret;'
  mytab <- cxxfunction(signature(vec1='numeric',maxval='numeric'),mytabST,plugin='Rcpp')



boundtf=which(sapply(ll,length)>0)
mtmat=sapply(boundtf,function(i){
  print(i)
  load(paste(tmpfile,i,'-ctout.RData',sep=''))
  mt
})

llas=sapply(ll,length)
mtmm=t((mtmat)*sqrt(llas[boundtf]))*sqrt(llas[boundtf])  

lnks=which(((mtmm>1e-2)&upper.tri(mtmm)),arr.ind=T)



require('linkcomm')
glc=getLinkCommunities(cbind(lnks,mtmm[lnks]),check.duplicates=F)


#require('igraph')
gas=graph.adjacency(as.matrix(as.dist(mtmm)),mode='undirected',weighted=TRUE)

sapply(1:nrow(mtmm){
  neighbors(gas,0)
}
       
       plot(gas,layout=layout.fruchterman.reingold,vertex.size=2,vertex.label='')
       
       require('snow')
       
       alliact=sapply(boundtf,function(i){
         print(i)
         fos=findOverlaps(flank(ll[[i]],width=50,both=T),allgr)
         qh=queryHits(fos)
         sh=subjectHits(fos)
         typ=values(allgr[sh])[,1]
         gh=c(0,findInterval(unique(qh),qh))
         mytab(alltfid[sh],1331)[boundtf]/(length(ll[[i]])*as.double(llas[boundtf]))
         #lgs=lda.collapsed.gibbs.sampler(rld,5,names(lgpwm),num.iterations=500,alpha=1,eta=1)
         #top.topic.words(lgs$topics)
         #plot((table(c(1:1331,typ))-1)/(allfreq+1e-10))
         #lgs
       })
       
       ll[[20]]
       
       #nextdaypred
       
       require('GenomicRanges')
       ll=lapply(1:maxtf,function(j){
         print(j)
         fn=paste(callsum,'cutfact-',j,'.csv.gz',sep='')
         if(file.exists(fn)){
           rc=read.csv(fn)
           if(ncol(rc)<5){
             ghr=sapply((c(1,6)-1),function(i){
               as.double(strsplit(scan(file=paste(callsum,'allfact-',j,'-day-',i,'.csv.gz',sep=''),,what=list(''),sep='\n')[[1]],',')[[1]])
             })
             gh=rowSums(ghr>1)
             sh=scan(file=paste(callsum,'allfact-',j,'-annot.csv.gz',sep=''),what=list(0,0,0,0),sep=',')
             #czs=sample(1:length(cz),sum(cz>2))
             #GRanges(seqnames=sh[[1]][czs],ranges=IRanges(start=sh[[2]][czs],width=1))
             ghz=gh>0
             if(sum(ghz)>0){
               write.csv(data.frame(chr=sh[[1]][ghz],coord=sh[[2]][ghz],ghr[ghz,,drop=F]),file=paste(callsum,'cutfact-',j,'.csv.gz',sep=''))
             }
             rc=read.csv(fn)
           }
           GRanges(rc[,2],IRanges(start=rc[,3],width=1),type=rep(j,nrow(rc)),d0=rc[,4],d3=rc[,5])
         }else{
           NULL
         }
       })
       
       allfacts=do.call(c,ll[sapply(ll,length)>0])
       allfactsnm=allfacts[,integer(0)]
       llnm=lapply(ll,function(i){
         if(!is.null(i)){
           i[,integer(0)]
         }
         else{
           NULL
         }
       })
       allvaltyp=Rle(values(allfacts)[,1])
       allvald0=values(allfacts)[,2]
       allvald3=lapply(ll,function(i){
         if(!is.null(i)){
           values(i)[,3]
         }else{
           NULL
         }
       })
       
       #    save(allfactsnm,llnm,allvaltyp,allvald0,allvald3,file=paste(tmpfile,'lassotmp.RData'),compress='gzip')
       load(paste(tmpfile,'lassotmp.RData'))
       
       require("Rcpp")
       require("inline")
       mytabmuST='
    Rcpp::NumericVector cvec1(vec1);
       Rcpp::NumericVector cscs(scs);
       Rcpp::NumericVector cmaxval(maxval);
       Rcpp::NumericVector toretsum(cmaxval[0]);
       Rcpp::NumericVector toretct(cmaxval[0]);
       for(int i =0; i < cvec1.size();i++){
       int a = cvec1[i];
       toretsum[a-1]+=cscs[i];
       toretct[a-1]++;
       }
       Rcpp::List toret;
       toret["sum"]=toretsum;
       toret["ct"]=toretct;
       return toret;'
  mytabmu <- cxxfunction(signature(vec1='numeric',scs='numeric',maxval='numeric'),mytabmuST,plugin='Rcpp')
       
       aff=lapply(which(sapply(ll,length)>0),function(i){
         print(i)
         fos=findOverlaps(flank(ll[[i]],width=20,both=T),allfacts)
       })
       
       lasoll=lapply(which(sapply(ll,length)>0),function(i){
         print(i)
         fos=findOverlaps(flank(ll[[i]],width=20,both=T),allfacts)
         qh=queryHits(fos)
         sh=subjectHits(fos)
         gh=c(0,findInterval(unique(qh),qh))
         allvaltype=allval[,1]
         allvald0=allval[,2]
         rld=t(sapply(1:(length(gh)-1),function(j){
           lm=((gh[j]+1):(gh[j+1]))
           mtm=mytabmu(allvaltype[sh[lm]],allvald0[sh[lm]],1331)
           mtm[[1]]/(mtm[[2]]+1e-5)
         }))
         isectsub=which(colSums(rld!=0)>0)
         yrld=values(ll[[i]])[,3]
         require('lars')
         maxsub=min(100000,nrow(rld))
         larout=lars(rld[1:maxsub,isectsub],yrld[1:maxsub],type='lasso',trace=TRUE,normalize=FALSE)
         clout=coef(larout)
       })
       
       
       for(i in which(sapply(llnm,length)>0)){
         submitJob('lassoscript.r',i)
       }
       
       load(paste(tmpfile,'lassotmp.RData'))         
       require('glmnet')
       
       ell=do.call(rbind,lapply(which(sapply(llnm,length)>0),function(i){
         if(file.exists(paste(tmpfile,i,'-larout.RData',sep=''))){
           load(paste(tmpfile,i,'-larout.RData',sep=''))
           pgl=coef(glout,s='lambda.1se')[-1]
           if(sum(pgl!=0)>0){
             cbind(i,which(pgl!=0),as.double(pgl[pgl!=0]))
           }else{
             integer(0)
           }
         }else{
           integer(0)
         }
       }))
       
       require('igraph')
       
       
       gel=graph.edgelist(ell[,1:2])
       E(gel)$weights=ell[,3]
       nsubset=degree(gel)>2
       sgel=subgraph(gel,which(nsubset))
       pdf('laytest.pdf',20,20)
       plot(sgel,layout=layout.fruchterman.reingold,vertex.label='',vertex.size=2)
       dev.off()
       
       load(paste(pwmout,'commonfiles.RData',sep=''))
       gadj=get.adjacency(gel,attr='weights')
       hcc=hclust(gdist,method='average')
       allnom=c(sapply(names(lgpwm)[1:1313],function(i){strsplit(i,' ')[[1]][2]}),names(lgpwm)[-(1:1313)])
       rownames(gadj)=allnom
       colnames(gadj)=allnom
       diag(gadj)=0
       greal=gadj[rowSums(gadj!=0)>1,colSums(gadj!=0)>1]
       greal[greal>5]=5
       greal[greal< -5]=-5
       # pdf('heattest.pdf',40,40)
       heatmap(t(as.matrix(greal)),scale='none')
       #dev.off()
       
       pdf('effecttest.pdf')
       for(k in 1:ncol(greal)){
         hist(greal[greal[,k]!=0,k],100,main=colnames(greal)[k],xlab='coefs')    
         #hist(greal[k,greal[k,]!=0],100)
       }
       dev.off()
       
       wgadj=diag(get.adjacency(gel,attr='weights'))[colSums(gadj!=0)>1]
       
       #nextpred=sapply(1:ncol(greal),function(k){mean((greal[greal[,k]!=0,k]))/(wgadj[k]+1e-2)})
       nextpred=sapply(1:ncol(greal),function(k){sum(greal[greal[,k]!=0,k]>0)-sum(greal[greal[,k]!=0,k]<0)})
       names(lgpwm)[colSums(gadj!=0)>1][rev(order(nextpred))[1:60]]
       names(lgpwm)[colSums(gadj!=0)>1][order(nextpred)[1:60]]
       
       gelbp=graph.edgelist(cbind(ell[,1],ell[,2]+1331))
       E(gelbp)$weights=ell[,3]
       nsubset=degree(gelbp)>0
       sgel=subgraph(gelbp,which(nsubset))
       lll=rbind(cbind(1,which(nsubset[1:1331])),cbind(2,which(nsubset[1332:length(nsubset)])))
       pdf('laytest.pdf',20,20)
       plot(sgel,layout=lll,vertex.label='',vertex.size=2)
       dev.off()
       
       ellsparse=do.call(rbind,lapply(which(sapply(llnm,length)>0),function(i){
         if(file.exists(paste(tmpfile,i,'-larout.RData',sep=''))){
           load(paste(tmpfile,i,'-larout.RData',sep=''))
           pgl=coef(glout,s=glout$lambda[which(glout$cvm<(min(glout$cvm)+1.5))[1]])[-1]
           #pgl=coef(glout,s='lambda.1se')[-1]
           if(sum(pgl!=0)>0){
             cbind(i,which(pgl!=0),as.double(pgl[pgl!=0]))
           }else{
             integer(0)
           }
         }else{
           integer(0)
         }
       }))
       gelsp = graph.edgelist(cbind(ellsparse[,1],ellsparse[,2]))
       E(gelsp)$weights=ellsparse[,3]
       hcr=hclust(dist(get.adjacency(gelsp,attr='weights')),method='average')
       hcc=hclust(dist(t(get.adjacency(gelsp,attr='weights'))),method='average')
       ohcr=order.dendrogram(as.dendrogram(hcr))
       ohcc=order.dendrogram(as.dendrogram(hcc))
       gelss = graph.edgelist(cbind(ellsparse[,1],ellsparse[,2]+1331))
       #sgelsp=subgraph(gelsp,which(clusters(gelsp)$membership==which.max(clusters(gelsp)$csize)))
       #
       #
       lls=rbind(cbind(1,ohcr),cbind(2,ohcc))
       xz=allnom[1:max(ellsparse[,1:2])]#[which(clusters(gelss)$membership==which.max(clusters(gelss)$csize))]
       pdf('graphout.pdf',70,90)
       plot(gelss,layout=lls,vertex.label=c(xz,xz),vertex.size=0)
       dev.off()
       
       elllist=lapply(which(sapply(llnm,length)>0),function(i){
         if(file.exists(paste(tmpfile,i,'-larout.RData',sep=''))){
           load(paste(tmpfile,i,'-larout.RData',sep=''))
           #pgl=coef(glout,s=glout$lambda[which(glout$cvm<(min(glout$cvm)+0.5))[1]])[-1]
           pgl=coef(glout,s='lambda.1se')[-1]
           #pgl=coef(glout,s='lambda.min')[-1]
           if(sum(pgl!=0)>0){
             cbind(i,which(pgl!=0),as.double(pgl[pgl!=0]))
           }else{
             integer(0)
           }
         }else{
           integer(0)
         }
       })
       
       fileconn=file('settlerpred.txt',open='wt');close(fileconn)
       fileconn=file('settlerpred.txt',open='at')
       for(i in 1:length(elllist)){
         g=elllist[[i]]
         if(length(g)>0){
           gc=g[1,1]
           gr=allnom[g[rev(order(g[,3])),2]]
           gf=rev(sort(g[,3]))
           s1=paste(c(allnom[gc],gr),collapse='\t')
           s2=paste(c(allnom[gc],signif(gf,digits=3)),collapse='\t')
           s3=''
           writeLines(c(s1,s2,s3),fileconn)
         }
       }
       close(fileconn)
       require('snow')
       cl<-makeCluster(rep('localhost',8))
       require('GenomicRanges')       
       lf1=list.files(paste0(calltmp,'/1'),pattern='tfout.gz')
       d1sig=lapply(1:1331,function(i){
         print(i)
         tab=read.table(paste0(calltmp,'/1/',i,'.tfout.gz'),sep=',')
         GRanges(seqnames=paste0('chr',tab[,1]),ranges=IRanges(start=abs(tab[,6]),width=1),score=rep(i,nrow(tab)))
       })
       factsub=as.double(fstab[,1])
       d3sig=lapply(factsub,function(i){
         print(i)
         while(!file.exists(paste0(calltmp,'/8/',i,'.tfout.gz'))){Sys.sleep(10)}
         tab=read.table(paste0(calltmp,'/8/',i,'.tfout.gz'),sep=',')
         GRanges(seqnames=paste0('chr',tab[,1]),ranges=IRanges(start=abs(tab[,6]),width=1),score=rep(i,nrow(tab)))
       })
       clusterExport(cl,c('d3sig'))
       clusterCall(cl,function(){require('GenomicRanges')})
       ss=clusterApplyLB(cl,d1sig,function(i){
         which(countOverlaps(flank(d3sig[[1]],20,both=T),i)>0)
       })
       d3ctr=lapply(factsub,function(i){
         print(i)
         while(!file.exists(paste0(calltmp,'/8/',i,'.tfout.control.gz'))){Sys.sleep(10)}
         tab=read.table(paste0(calltmp,'/8/',i,'.tfout.control.gz'),sep=',')
         GRanges(seqnames=paste0('chr',tab[,1]),ranges=IRanges(start=abs(tab[,6]),width=1),score=rep(i,nrow(tab)))
       })
       clusterExport(cl,c('d3ctr'))
       ssctr=clusterApplyLB(cl,d1sig,function(i){
         which(countOverlaps(flank(d3ctr[[1]],20,both=T),i)>0)
       })
       nmax=length(d3sig[[1]])
       doctrue=lapply(1:nmax,function(i){integer(0)})
       docctr=lapply(1:nmax,function(i){integer(0)})
       matin=Matrix(0,length(ss),nmax*2)
       for(i in 1:length(ss)){
         print(i)
         for(j in ss[[i]]){
           doctrue[[j]]=c(doctrue[[j]],i)
         }
         
         for(j in ssctr[[i]]){
           docctr[[j]]=c(docctr[[j]],i)
         }
         if(length(ss[[i]])>0)  matin[i,ss[[i]]]=1
         if(length(ssctr[[i]])>0)  matin[i,ssctr[[i]]+nmax]=1
       }
       doctrue2=lapply(doctrue[sapply(doctrue,length)>0],function(i){rbind(as.integer(i-1),1L)})
       docctr2=lapply(docctr[sapply(docctr,length)>0],function(i){rbind(as.integer(i-1),1L)})
       
       require('glmnet')
       require('Matrix')
       #slt=slda.em(c(doctrue2,docctr2),30,names(lgpwm),num.e.iterations=100,num.m.iterations=10,alpha=0.1,eta=1,annotations=c(rep(TRUE,length(doctrue2)),rep(FALSE,length(docctr2))),params=rep(0,30),1,logistic=TRUE,trace=1L)
       #top.topic.words(slt$topics)
       #slt$coefs
       glmt=glmnet(t(matin),as.factor(c(rep(1,nmax),rep(0,nmax))),family='binomial',nlambda=10,standardize=F)
       
       require('lda')
       d1sigalldo.call(c,d1sig)
       
       
       
       
       
       
       plot(1:nrow(exprs),lexpr[order(exprs[,k]-exprs[,1]),k],type='l')
       
       k=5       
       ir1=isoreg(lexpr[,k],dnexpr[,k])
       plot(ir1)
       
       kcut = exprs[,k]>10 & exprs[,k-1]>10
       plot(data.frame((log(exprs[,k])-log(exprs[,k-1]))[kcut],(log(dnexpr[,k])-log(dnexpr[,k-1]))[kcut]))
       
       pdf('exprcomp.pdf')
       for(k in 2:4){
         mat=cbind(exprs[,k]-exprs[,1],log(dnexpr[,k])-log(dexpr[pntarg,6]))
         #mat=cbind(log(exprs[,k])-log(exprs[,k-1]),log(dnexpr[,k])-log(dnexpr[,k-1]))
         ir1=isoreg(mat[,1],mat[,2])
         plot(ir1,main=paste0(colnames(dnexpr)[k-1],'-',colnames(dnexpr)[k]))
         nsel= which(abs((exprs[,k]-exprs[,1]))>500)
         text(mat[nsel,],lgname[pntarg][nsel])
       }
       dev.off()
       
       #############################
       
       require('snowfall')
       sfInit(parallel=T,cpus=8)
       
       load(paste(tmpfile,'commonfiles.RData',sep=''))
       
       pairHL <- function(x1,x2){
         ddset = apply(x2,1,function(i){sqrt(colSums((t(sqrt(x1))-sqrt(i))^2))})
         ltt=lapply(0:(nrow(x2)-1),function(i){(nrow(x2)-i):nrow(x2)})
         ltt2=lapply(0:(nrow(x1)-1),function(i){(nrow(x1)-i):nrow(x1)})
         min(c(sapply(ltt,function(i){
           it = i[1:min(length(i),ncol(ddset),nrow(ddset))]
           jm = 1:min(length(i),ncol(ddset),nrow(ddset))
           dd1=diag(ddset[jm,it,drop=F])
           dd2=sqrt(rowSums((sqrt(x1[-jm,,drop=F])-sqrt(0.25))^2))
           dd3=sqrt(rowSums((sqrt(x2[-it,,drop=F])-sqrt(0.25))^2))
           sum(dd1)+sum(dd2)+sum(dd3)
           #sum(dd1)
         }),sapply(ltt2,function(i){
           it = i[1:min(length(i),ncol(ddset),nrow(ddset))]
           jm = 1:min(length(i),ncol(ddset),nrow(ddset))
           dd1=diag(ddset[it,jm,drop=F])
           dd2=sqrt(rowSums((sqrt(x2[-jm,,drop=F])-sqrt(0.25))^2))
           dd3=sqrt(rowSums((sqrt(x1[-it,,drop=F])-sqrt(0.25))^2))
           sum(dd1)+sum(dd2)+sum(dd3)
         })))
       }
       
       eps=1e-3
       sfExport('lgpwm')
       sfExport('eps')
       sfExport('pairHL')
       sfExport('maxtf')
       test=sfSapply(1:maxtf,function(i){
         print(i)
         sapply(1:maxtf,function(j){
           ci=exp(lgpwm[[i]])
           cj=exp(lgpwm[[j]])
           csi=colSums(ci+eps)
           csj=colSums(cj+eps)
           x1=t(ci+eps)/csi
           x2=t(cj+eps)/csj
           pairHL(x1,x2)/(nrow(x1)+nrow(x2))
         })
       })
       
       row.names(test)<-names(lgpwm)
       write.csv(test,file=paste(summ,"distmat.csv",sep=''))
       test<-read.csv(paste(summ,"distmat.csv",sep=''),row.names=1)
       hcc=hclust(as.dist(test),method="average")
       plot(hcc,label=names(lgpwm))
       ctt=cutree(hcc,h=0.15)
       s1=matrix(names(lgpwm)[as.vector(t(apply(test,1,order)[2:10,]))],length(lgpwm))
       s2=t(apply(test,1,sort)[2:10,])
       aa=data.frame(names=names(lgpwm),do.call(cbind,lapply(1:9,function(i){data.frame(s1[,i],s2[,i])})))
       write.csv(aa,file=paste(summ,"motifcluster.csv",sep=''))
       
       require('MotIV')
       exportAsTransfacFile(lgpwm,file=paste(summ,"motifsused.txt",sep=''))
       
)         




