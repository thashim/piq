setwd('/dnase')
source('initec2.r')

pwmin='/dnase/pwms/'
tmpfile='/dnase/tmp/'
datain='/dnase/data/'
pwmout='/dnase/pwmout/'
kernout='/dnase/kernout/'
bgout='/dnase/tmp/'
calltmp='/dnase/tmp/'
callout='/dnase/output/'
summ='/dnase/output/'
callsum='/dnase/output/'



numdays=length(readLines('data/exptlist.txt'))
maxtf=length(grep('>',readLines('pwms/jaspar.txt')))+length(grep('^DE',readLines('pwms/transfac.txt')))  #counts PWMs in files from grep'ing "DE" and ">"
chrlen=length(readLines('data/chrlist.txt'))
chrstr=readLines('data/chrlist.txt')
maxct=as.double(readLines('data/maxcut.txt'))
email="cwo@csail.mit.edu"
exptsub=1:numdays

copyfiles=c('maxcut.txt','chrlist.txt','exptlist.txt')
file.copy(paste0(datain,copyfiles),paste0(callout,copyfiles))

#User modifiable parameters
numbp=150      # experiment-specific window for non-factor-specific effects  (for estimating count variation)
wsize=199      # window of dnase-seq read profile (each direction)
topnum=10000   # columb topnum windows before stopping for count estimation
pwmcut=5       # pre-filter PWM's with log(pwm)<pwmcut will be discarded, normalized by length: log-odds wrt random ATCG (1/[1+e^-(pwmcut)] or better)
nvec=5         # approximation factor... smaller=faster/worse... larger much slower (quadratic scaling, stay <10) -- degrees of freedom of noise
niterbg=5      # number of EM iterations used to estimate non-factor-specific params  (non-factor-specific first big loop)
niter=3        # number of EM iterations usest to estimate factor-specific params (factor-specific second big loop)
maxpertf=50000 # max bound genomic sites per TF computational 
flank = 500    # flank size added during preprocessing
multiweight = 0 #weight for multiday correlation

require("BSgenome.Mmusculus.UCSC.mm10")
cgenome=Mmusculus

save(multiweight,pwmin,tmpfile,datain,pwmout,kernout,bgout,calltmp,callout,summ,callsum,numdays,maxtf,chrlen,wsize,topnum,maxct,numbp,email,pwmcut,chrstr,nvec,niterbg,niter,maxpertf,flank,exptsub,file='/dnase/params.RData')

source('getPWMec2.r')
checkq()
for(i in 1:numdays){
  submitJob('makebgtffile.r',i,F)
}
checkq()
source('makecovmat.r')
if(multiweight > 0 & numdays > 0){
    source('makecovmatmultiday.r')
}
#
system('qconf -rattr queue slots 4 all.q@master')
strin=strsplit(system('qhost',intern=T),' ',fixed=T)
sapply(sapply(strin[5:length(strin)],function(i){i[1]}),function(j){
    system(paste0('qconf -rattr queue slots 8 all.q@',j))
})
nsep=2
for(i in 1:numdays){
  submitJob('makeindtffile.r',c(i,nsep),T,1,maxtf,nsep)
}
checkq()
#
#rm pwm calls
#unlink(paste0(pwmout,'/*'))
#
source('docallsnew.r')
#on the fly, remove indtffile.
sendmail(email,subject='everything done, or crashed',message=paste0('ec2 finished, experiments',paste(readLines('data/exptlist.txt'),collapse=' ')),password='rmail')
file.create('/dnase/done')

