source('initec2.r')
load('/dnase/params.RData')
for(i in 1:numdays){
submitJob('makecovmatmultidayqscript.r',i)
}
checkq()
allmuvecs=sapply(1:numdays,function(i){
  as.vector(get(load(paste0('tmp/',i,'-muallout.RData'))))
})

allmucovs=cov(allmuvecs)

lmfconst=matrix(0,ncol(allmuvecs),ncol(allmuvecs))
lmfcoefs=matrix(0,ncol(allmuvecs),ncol(allmuvecs))

for(i in 1:ncol(allmuvecs)){
  for(j in 1:ncol(allmuvecs)){
    lmf=lm.fit(cbind(1,allmuvecs[,i]),allmuvecs[,j])
    lmfc=lmf$coefficients
    lmfconst[i,j]=lmfc[1]
    lmfcoefs[i,j]=lmfc[2]
  }
}

save(lmfconst,lmfcoefs,file='tmp/multidaycoef.RData')
