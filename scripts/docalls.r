setwd('/dnase')
source('initec2.r')

runBG=F

load('/dnase/params.RData')
for(day in 0:numdays){
  dir.create(paste(calltmp,'/',day,sep=''),recursive=T,showWarnings=F)
}
for(day in 0:numdays){
  dir.create(paste(callout,'/',day,sep=''),recursive=T,showWarnings=F)
}

for(chr in 1:chrlen){
  print(chr)
  lf=list.files(pwmout,pattern=paste(chrstr[chr],'-',sep=''))
  if(length(lf)>0){
  batches=makeBatch(1,length(lf),by=20)
  for(i in 1:length(batches)){
    submitJob('docallsqscript.R',c(chr,numdays,min(batches[[i]]),max(batches[[i]])))
  }
  }
}

if(runBG==TRUE){
for(chr in 1:chrlen){
  print(chr)
  lf=list.files(pwmout,pattern=paste(chrstr[chr],'-',sep=''))
  batches=makeBatch(1,length(lf),by=20)
  for(i in 1:length(batches)){
    submitJob('docallsqscriptbg.R',c(chr,numdays,min(batches[[i]]),max(batches[[i]])))
  }
}
}

while(length(system('/opt/sge6/bin/linux-x86/qstat',intern=T))>2){print('waiting for sge to clear');Sys.sleep(60)}

if(runBG==FALSE){
for(i in 1:length(exptsub)){
  print(i)
  submitJob('parsegpmodel.r',c(i,10),T,1,maxtf,10)
}
}else{
  for(i in 1:length(exptsub)){
    print(i)
    submitJob('parsegpmodelbg.r',c(i,10),T,1,maxtf,10)
  }  
}

