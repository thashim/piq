#!/usr/bin/Rscript

source('initec2.r')
require("Rcpp")
require("inline")

args=as.double(commandArgs(trailingOnly=TRUE))
chr=args[1]
numdays=args[2]
spos=args[3]
epos=args[4]

if(!file.exists(paste('calltmp/chr-',chr,'-',spos,'.txt',sep=''))){

decodeRLE <- function(str,nrows=9){
  ss=strsplit(str,';',fixed=T)[[1]]
  ssi=strsplit(ss,',',fixed=T)
  rlein=list()
  rlein$lengths=as.integer(ssi[[1]])
  rlein$values=as.integer(ssi[[2]])
  class(rlein)='rle'
  rlevec=matrix(inverse.rle(rlein),nrow=nrows*2)
}

uniconv ='
Rcpp::NumericVector cvadd(vadd);
Rcpp::NumericVector cvret(cvadd.size());
int cksize = as<int>(ksize);
int halfsz = (cksize-1)/2;
double temp=0;
for(int i=0; i < (halfsz);i++){
temp+=cvadd[i];
}
for(int i=0; i < cvadd.size();i++){
if((i+halfsz)< cvadd.size()){
temp+=cvadd[i+halfsz];
}
cvret[i]=temp/cksize;
if((i-halfsz)>=0){
temp-=cvadd[i-halfsz];
}
}
return cvret;
'

uconv <- cxxfunction(signature(vadd="numeric",ksize="integer"),uniconv,plugin="Rcpp",includes="#include <numeric>")

poisAllIncludes='
#define cast_uint32_t (uint32_t)
#include <stdint.h>
#include <numeric>
#include <algorithm>
#include<time.h>
/* approximate exponential function */
static inline float
fastpow2 (float p)
{
  float offset = (p < 0) ? 1.0f : 0.0f;
  float clipp = (p < -126) ? -126.0f : p;
  int w = clipp;
  float z = clipp - w + offset;
  union { uint32_t i; float f; } v = { cast_uint32_t ( (1 << 23) * (clipp + 121.2740575f + 27.7280233f / (4.84252568f - z) - 1.49012907f * z) ) };
  return v.f;
}
//
static inline float
fastexp (float p)
{
  return fastpow2 (1.442695040f * p);
}
'


#Take - lmupri,sigpri,dpt=data,gknots-nodes,gknots-weights,fdpt,lookuptable1,lookuptable2,directionmatrix(sb),muctrl,lambertseq,lambertlookup,lambertseqctrl
poisAllST ='
Rcpp::NumericVector cdatain(datain);
Rcpp::NumericVector cdfact(dfact);
Rcpp::NumericVector cvadj(vadj);
Rcpp::NumericVector cquadpts(quadpts);
Rcpp::NumericVector cquadwts(quadwts);
Rcpp::NumericVector cquadwtsq(cquadwts.size());
for(int i=0; i<cquadwts.size();i++){
  cquadwtsq[i]=cquadpts[i]*cquadpts[i];
}
Rcpp::NumericMatrix csb(sb);
Rcpp::NumericVector clamseq(lamseq);
Rcpp::NumericVector clamlookup(lamlookup);
Rcpp::NumericVector clamctr(lamctr);
//
Rcpp::List ckmatSS(kmatSS);
Rcpp::List ckmatOS(kmatOS);
Rcpp::NumericVector cdirection(direction); //vector of +1 / -1 of length tf candidates
Rcpp::NumericVector ctfid(tfid); //vector of tf ids based on binding sites.
Rcpp::List ctfmap(tfmap); // list of length wsize, each entry is a list of TF candidates
Rcpp::List ctfoffs(tfoffs); // each entry is a offset.
Rcpp::NumericVector cwsize(wsize); //entry 0 - size of windwo, entry 1 - size of kern prof, entry 3
//
Rcpp::NumericMatrix tcavmu(csb.nrow(),cdatain.size());
Rcpp::NumericMatrix tcavprec(csb.nrow(),cdatain.size());
//
int numdp = cdatain.size();
int numvec = csb.nrow();
int quadsize = cquadpts.size();
//
Rcpp::NumericVector lambmupri(Rcpp::clone(lmupri));
Rcpp::NumericVector lambsigpri(Rcpp::clone(sigpri));
Rcpp::NumericVector lambmuprec(lambmupri.size());
Rcpp::NumericVector lambprec(lambsigpri.size());
//
int bindsize = ckmatSS.size()*cdirection.size();
int odsize = cdirection.size();
int kernsize = cwsize[1];
Rcpp::NumericVector bindmuprec(bindsize);
Rcpp::NumericVector bindprec(bindsize);
Rcpp::NumericMatrix tbcavmu(bindsize,2*kernsize); // x2 to encode both strands
Rcpp::NumericMatrix tbcavprec(bindsize,2*kernsize);
for(int bnum=0;bnum<bindsize;bnum++){
  bindprec[bnum]=1/cwsize[2]+cwsize[3]*(2*kernsize);
  bindmuprec[bnum]=0;
  for(int ept=0;ept<(2*kernsize);ept++){
    tbcavmu(bnum,ept)=0;
    tbcavprec(bnum,ept)=cwsize[3];
  }
}
//
Rcpp::NumericVector evidence(cdatain.size());
Rcpp::NumericVector rerr(cdatain.size());
Rcpp::NumericVector rsig(cdatain.size());
//
for(int bnum=0;bnum<numvec;bnum++){
  lambprec[bnum]=1/lambsigpri[bnum];
  lambmuprec[bnum]=lambmupri[bnum]/lambsigpri[bnum];
  for(int ept=0; ept<numdp;ept++){
  tcavmu(bnum,ept)=lambmupri[bnum];
  tcavprec(bnum,ept)=0;
  }
}
//
Rcpp::NumericVector qprec(lambsigpri.size());
Rcpp::NumericVector qmuprec(lambsigpri.size());
Rcpp::NumericVector qbprec(bindprec.size());
Rcpp::NumericVector qbmuprec(bindprec.size());
//size checks
bool sizeflag = (cdatain.size()==numdp) && (cdfact.size()==numdp) && (cquadpts.size()==quadsize) && (cquadwts.size()==quadsize)  && csb.nrow()==numvec && csb.ncol()==numdp && clamseq.size() == clamlookup.size() && lambmupri.size()==numvec && lambsigpri.size()==numvec;
//
Rcpp::NumericVector lambmuprev(bindsize);
Rcpp::NumericVector cconv(conv);
bool runflag = true;
int counter=0;
double err=0;
double eps=cwsize[4];
while(runflag && counter < cconv[1]){
  for(int bnum=0;bnum<bindsize;bnum++){
    lambmuprev[bnum]=bindmuprec[bnum];
  }
  err=0;
  for(int eptn=0; eptn<numdp;eptn++){
    int ept=eptn;//rand()%numdp;
    //
    // step 0 - preprocessing to find out which binding sites overlap on ept
    //
    int winsize = cwsize[0];
    int halfwid= (cwsize[1]-1)/2;
    int iday = floor((ept)/(2*winsize));
    int ipart = (ept)%winsize;
    int istrand = ((int)floor((ept)/winsize))%2;
    if(iday >= 9){Rcpp::Rcout<<1<<std::endl;}
    if(ipart >= 1000){Rcpp::Rcout<<2<<std::endl;}
    if(istrand >= 2){Rcpp::Rcout<<3<<std::endl;}
    Rcpp::NumericMatrix curdaySS = ckmatSS(iday);
    Rcpp::NumericMatrix curdayOS = ckmatOS(iday);
    //Marginal contributions arising from binding
    Rcpp::NumericVector bcands = ctfmap(ipart);
    Rcpp::NumericVector boffs = ctfoffs(ipart);
    double bindcoefs [bcands.size()];
    for(int bnum=0; bnum<bcands.size();bnum++){
      int bcd = bcands[bnum]-1;
      if(bcd>=0){
      int bid = ctfid[bcd]-1;
      int boff = boffs[bnum]-1;
      if(istrand==0){ //pos strand 
        if(cdirection[bcd]>0){
          bindcoefs[bnum]=curdaySS(bid,boff);
        }else{
          bindcoefs[bnum]=curdayOS(bid,boff);
        }
      }else{ // neg strand
        if(cdirection[bcd]>0){
          bindcoefs[bnum]=curdayOS(bid,boff);
        }else{
          bindcoefs[bnum]=curdaySS(bid,boff);
        }
      }}else{
        bindcoefs[bnum]=0;
      }
    }
    //
    // step 1 - calculate marginal sufficient stats etc
    //
    double mumarg=0.0;
    double sigmarg=cvadj[ept];
    for(int bnum=0; bnum<numvec;bnum++){
      qprec[bnum]=(lambprec[bnum]-tcavprec(bnum,ept));
      qmuprec[bnum]=(lambmuprec[bnum]-tcavmu(bnum,ept)*tcavprec(bnum,ept));
      mumarg+=csb(bnum,ept)*qmuprec[bnum]/qprec[bnum];
      sigmarg+=csb(bnum,ept)*csb(bnum,ept)/qprec[bnum];
    }
    for(int bnum=0; bnum<bcands.size();bnum++){
      int bcd = bcands[bnum]-1;
      if(bcd>=0){
      int boff = boffs[bnum]-1 + kernsize*istrand;
      int brind = bcd+odsize*(iday);
      qbprec[brind]=(bindprec[brind]-tbcavprec(brind,boff));
      qbmuprec[brind]=(bindmuprec[brind]-tbcavmu(brind,boff)*tbcavprec(brind,boff));
      mumarg+=bindcoefs[bnum]*qbmuprec[brind]/qbprec[brind];
      sigmarg+=bindcoefs[bnum]*bindcoefs[bnum]/qbprec[brind];
      }
    }
    //
    int curdata=cdatain[ept];
    int curfact = cdfact[ept];
    // precalculate the main integral over each datapoint
    double evalpts [quadsize];
    double prpts [quadsize];
    double pr=0;
    double zsum=0;
    double zsumexp=0;
    double zsumsq=0;
    //laplace approx
    double primu=mumarg;
    double prisig=sigmarg;
    double loglam=curdata*prisig+primu+log(prisig);
    int lamind=floor((loglam-clamctr[0])/clamctr[1]);
    if(lamind<0){lamind=0;}
    if(lamind>=clamseq.size()){lamind=clamseq.size()-1;}
    double lam=clamlookup[lamind];
    double lapmu = curdata*prisig+primu-lam;
    double lapsig = 1/(fastexp(lapmu)+1/prisig);
    double sqls = sqrt(2*lapsig);
    // actual integration step (hermite)
    for(int k=0;k<quadsize;k++){
      evalpts[k]=lapmu+sqls*cquadpts[k];
      double lamb = fastexp(evalpts[k]);
      double lterm=cquadwts[k]-curfact-lamb-(evalpts[k]-primu)*(evalpts[k]-primu)/(2*prisig)+cquadwtsq[k];
      //prpts[k]=fastexp(-1*lamb-(evalpts[k]-primu)*(evalpts[k]-primu)/(2*prisig)+cquadwtsq[k])/(curfact*gaussconst)*cquadwts[k]*sqls;
      if(curdata==1){
        //prpts[k]*=lamb;
        lterm+=evalpts[k];
      }
      if(curdata>1){
        //prpts[k]*=pow(lamb,curdata);
        lterm+=curdata*(evalpts[k]);
      }
      prpts[k]=fastexp(lterm);
      pr+=prpts[k];
      zsum+=prpts[k]*evalpts[k];
      zsumexp+=prpts[k]*lamb;
      zsumsq+=prpts[k]*evalpts[k]*evalpts[k];
    }
    evidence[ept]=pr;
    double zmu=zsum/pr;
    double zmuexp=zsumexp/pr;
    double zsqmu=zsumsq/pr;
    double zvarrat=(zsqmu-zmu*zmu)/sigmarg;
    double zresid=zmu-mumarg;
    rerr[ept]=zvarrat;
    rsig[ept]=sigmarg;
    if(zvarrat<1 && zvarrat >0){
    for(int bnum=0; bnum<numvec;bnum++){
      //see notes - moments of integral are weightec combinations of polynomial functionals of the evaluation points
      //updates for mean
      //get posterior expectations and cavity distributions
      if(csb(bnum,ept)!=0){
      double tprop=1/(sigmarg*qprec[bnum]);
      double newmu=qmuprec[bnum]/qprec[bnum]+csb(bnum,ept)*tprop*zresid;
      double tvar=csb(bnum,ept)*csb(bnum,ept)/qprec[bnum];
      double newprec=1/(tprop*(zvarrat*tvar+sigmarg-tvar));
      double cavprec=(newprec-qprec[bnum]);
        //update posterior with message if cavity distribution is nondegenerate
      tcavprec(bnum,ept)=cavprec*(1-eps)+eps*tcavprec(bnum,ept);
      tcavmu(bnum,ept)=(newmu*newprec-qmuprec[bnum])/cavprec*(1-eps)+eps*tcavmu(bnum,ept);
      lambmuprec[bnum]=newmu*newprec*(1-eps)+eps*lambmuprec[bnum];
      lambprec[bnum]=newprec*(1-eps)+eps*lambprec[bnum];
      }
    }
    for(int bnum=0; bnum<bcands.size(); bnum++){
      int bcd = bcands[bnum]-1;
      if(bcd>=0){
      double coef = bindcoefs[bnum];
      int boff = boffs[bnum]-1+kernsize*istrand;
      int brind = bcd+odsize*(iday);
      if(coef!=0){
      double tprop=1/(sigmarg*qbprec[brind]);
      double newmu=qbmuprec[brind]/qbprec[brind]+coef*tprop*zresid;
      double tvar=coef*coef/qbprec[brind];
      double newprec=1/(tprop*(zvarrat*tvar+sigmarg-tvar));
      double cavprec=(newprec-qbprec[brind]);
        //update posterior with message if cavity distribution is nondegenerate
      if(newmu<0){newmu=0;}
      bindmuprec[brind]=newmu*newprec*(1-eps)+eps*bindmuprec[brind];
      bindprec[brind]=newprec*(1-eps)+eps*bindprec[brind];
      tbcavprec(brind,boff)=cavprec*(1-eps)+eps*tbcavprec(brind,boff);
      tbcavmu(brind,boff)=(newmu*newprec-qbmuprec[brind])/cavprec*(1-eps)+eps*tbcavmu(brind,boff);

      }}
    }
    }
  }
  for(int bnum=0;bnum<bindmuprec.size();bnum++){
      double del = (bindmuprec[bnum]-lambmuprev[bnum]);
      err+=del*del;
  }
  err=err/bindmuprec.size();
  if(err<cconv[0]){runflag=false;}
  Rcpp::Rcout << counter << " " << err << std::endl;
  counter++;
}
Rcpp::List toret;
toret[\"lambmu\"]=lambmuprec;
toret[\"lambsig\"]=lambprec;
toret[\"rerr\"]=bindmuprec;
toret[\"rsig\"]=bindprec;
toret[\"rerrt\"]=err;
return toret;'
poisJoint <- cxxfunction(signature(lmupri='numeric',sigpri='numeric',vadj='numeric',datain='numeric',sb='matrix',quadpts='numeric',quadwts='numeric',dfact='numeric',lamseq='numeric',lamlookup='numeric',lamctr='numeric',conv='numeric',kmatSS='list',kmatOS='list',tfmap='list',tfoffs='list',direction='numeric',tfid='numeric',wsize='numeric'),poisAllST,plugin="Rcpp",includes=poisAllIncludes,verbose=0)



load('tmp/callcommon.RData')

interleave<-function(x){g=matrix(x,length(x)/2,2);as.vector(t(g))}

bindoffset=500


oconn=file(paste('calltmp/chr-',chr,'-',spos,'.txt',sep=''),open='wt')
close(oconn)
oconn=file(paste('calltmp/chr-',chr,'-',spos,'.txt',sep=''),open='at')
fconn=file(paste('data/chr-',chr,'-datin.txt',sep=''),open='rt')
offsets=c(0,scan(paste('data/chr-',chr,'-datin.txt.index',sep=''))+1)
seekpos=cumsum(offsets)[(spos-1)*20+1]
seek(fconn,where=seekpos)
for(ppar in spos:epos){
  print(ppar)
  load(paste('pwmout/outfile-',chr,'-',ppar,'.RData',sep=''))
  for(i in 1:length(pwmscores)){
    str=readLines(fconn,1)
    at<-Sys.time()
    print(i)
    ptemp=pwmscores[[i]]
    dtest=t(decodeRLE(str,numdays))
    #
    bindloc=floor(abs(ptemp[,2])+bindoffset)
    kwid=(dim(kmatSS[[1]])[2]-1)/2
    startind=min(bindloc)-kwid
    endind=max(bindloc)+kwid
    #number of windows, nb last window is always indexed off the last location possible.
    wsiz=999
    dlen=floor((endind-startind-999)/(999-kwid)+1)
    spts=c(((1:dlen)-1)*(999-kwid)+startind-1,endind-999)
    epts=c(((1:dlen)-1)*(999-kwid)+999+startind-1,endind)
    #
    #assign tfs to windows.
    tfassign=rev(1:length(spts))[apply(outer(rev(spts),bindloc-kwid,'<'),2,function(i){which(i)[1]})]
    tfoutmu=matrix(0,length(tfassign),numdays)
    tfoutsig=matrix(0,length(tfassign),numdays)
      for(sub in 1:length(spts)){
        tfsub=which(tfassign==sub)
        if(length(tfsub)>0){
        psub=ptemp[tfsub,]
        fptest=floor(abs(ptemp[tfsub,2])+bindoffset)-spts[sub]
        require('IRanges')
        irtf=IRanges(start=fptest-kwid,end=fptest+kwid)
        fo=findOverlaps(IRanges(start=1:1000,width=1),irtf)
        #fis=c(0,findInterval(1:1000,queryHits(fo)))
        fis=c(findInterval(0:999,queryHits(fo)),length(fo))
        llind=lapply(1:(length(fis)-1),function(i){if(fis[i]==fis[i+1]){-1}else{subjectHits(fo)[(fis[i]+1):fis[i+1]]}})
        offs=lapply(1:length(llind),function(i){if(fis[i]==fis[i+1]){-1}else{sign(psub[llind[[i]],2])*(i-fptest[llind[[i]]])+200}}) #written so that if sign of binding and strand are matched, offset is correct.
        tempdin=as.vector(dtest[spts[sub]:epts[sub],interleave(1:(numdays*2))])
        pj=poisJoint(lmupri,lz,vadj,tempdin,tsb,gknots$nodes,log(gknots$weights),lgamma(tempdin+1),lambseq,lamlookup,c(lambseq[1],lambseq[2]-lambseq[1]),nruns,kmatSS,kmatOS,llind,offs,sign(ptemp[tfsub,2]),ptemp[tfsub,1],c(wsiz+1,ncol(kmatSS[[1]]),5,0,0.9))
        tfoutmu[tfsub,]=matrix(pj[[3]]/pj[[4]],nrow=length(tfsub))
        tfoutsig[tfsub,]=matrix(1/pj[[4]],nrow=length(tfsub))
        }
      }
      tdiff=Sys.time()-at
      print(paste('# ',nrow(dtest),' bases ',nrow(ptemp),' bsites ',format(tdiff),' time ',pj[[5]],' err ',sep=''))
      ucr=uconv(rep(1,nrow(dtest)),200)
      smrate=apply(dtest[,1:numdays]+dtest[,(1:numdays)+numdays],2,function(i){uconv(i,200)/ucr})
      hsens=smrate[abs(ptemp[,2])+bindoffset,]
      #dump out results - format, first line is an index (n1,n2.. identifies row offsets for tfid from index)
      rowoffs=findInterval(0:721,ptemp[,1])+1
      x1=paste('#',paste(c(rowoffs,nrow(ptemp)),collapse=','),sep='')
      slist=sapply(1:nrow(ptemp),function(i){
        paste(format(c(ptemp[i,],tfoutmu[i,],tfoutsig[i,],hsens[i,]),digit=4,scientific=F),collapse=',')
      })
      writeLines(c(x1,slist),oconn)
  }
}
close(fconn)
close(oconn)

}


