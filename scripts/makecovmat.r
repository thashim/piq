#!/usr/bin/Rscript

setwd('/dnase')
system('sudo apt-get -y install libgsl0-dev')
system('qconf -mattr queue load_thresholds np_load_avg=32 all.q')
source('initec2.r')
load('/dnase/params.RData')

#source('gpmodelCommon.R')
require('statmod')
gknotLG = gauss.quad(9000,kind="hermite")
poiscompLG <- function(mu,sig,dat){
  t=mu+sqrt(2*sig)*gknotLG$nodes
  dp=dpois(dat,exp(t))*gknotLG$weights/sqrt(pi)
  evs=dp%*%cbind(1,t,t^2)
  un=evs[2]/evs[1]
  vn=evs[3]/evs[1]-evs[2]^2/evs[1]^2
  c(un,vn,evs[1])
}

getLapLoc<-function(mu,sig,d,cut=20){
  l=rep(0,length(d))
  l[d<cut]=d[d<cut]*sig[d<cut]+mu[d<cut]-lambert_W0(exp((d*sig+mu)[d<cut])*sig[d<cut])
  l[d>cut]=(log(d[d>cut])*d[d>cut]+mu[d>cut]/sig[d>cut])/(d[d>cut]+1/sig[d>cut])
  sh=1/(exp(l)+1/sig)
  pr=sqrt(2*pi*sh)*dpois(d,exp(l))*dnorm(l,mu,sqrt(sig))
  list(l,sh,pr)
}

makeTable <- function(data,muoffs,stabval,maxcut=100){
  maxval=max(data)
  llmu=sapply(0:maxval,function(i){
    if(i<maxcut){
      v=poiscompLG(muoffs,stabval,i)
      (v[1]/v[2]-muoffs/stabval)/(1/v[2]-1/stabval)
    }else{
      (log(i)*i+muoffs/stabval)/(i+1/stabval)
    }
  })
  llprec=sapply(0:maxval,function(i){
    if(i<maxcut){
      1/poiscompLG(muoffs,stabval,i)[2]-1/stabval
    }else{
      me=(log(i)*i+muoffs/stabval)/(i+1/stabval)
      exp(me)-1/stabval
    }
  })
  list(llmu,llprec)
}

fastFit <- function(data,mupri,muadjusts,sigadjusts,scid){
  datind=which(data>0)
  if(length(datind)>0){
  sigadjust=sigadjusts[data[datind]+1]-sigadjusts[1]
  #
  dss=scid[datind,datind,drop=F]*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  sccm=t(scid[datind,,drop=F])
  cminternal=(solve(dss+diag(length(datind))))*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  a1=sccm%*%cminternal%*%t(sccm)
  allinv=(scid-a1)
  vh=as.vector(muadjusts[data+1]*sigadjusts[data+1]+mupri)
  uinv=scid%*%vh-sccm%*%(cminternal%*%(t(sccm)%*%vh))
  list(uinv,allinv)
  }else{
    list(scid%*%as.vector(mupri+muadjusts[1]*sigadjusts[1]),scid)
  }
}

fastMu <- function(data,mupri,muadjusts,sigadjusts,scid){
  datind=which(data>0)
  if(length(datind)>0){
  sigadjust=sigadjusts[data[datind]+1]-sigadjusts[1]
  #
  dss=scid[datind,datind,drop=F]*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  sccm=t(scid[datind,,drop=F])
  cminternal=(solve(dss+diag(length(datind))))*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  #a1=sccm%*%cminternal%*%t(sccm)
  #allinv=(scid-a1)
  sapply(mupri,function(mu){
    vh=as.vector(muadjusts[data+1]*sigadjusts[data+1]+mu)
    uinv=scid%*%vh-sccm%*%(cminternal%*%(t(sccm)%*%vh))
  })
  }else{
    sapply(mupri,function(mu){
      scid%*%as.vector(mu+muadjusts[1]*sigadjusts[1])
    })
  }
}

#cin = solve(covpost+zerovar)
calcPR <- function(data,mupris,muadjusts,sigadjusts,cin){
  datind=which(data>0)
  dv= sapply(mupris,function(mu){
      as.vector((muadjusts[data+1]-mu)%*%cin%*%(muadjusts[data+1]-mu))
  })
  if(length(datind)>0){
  sigadjust=1/(sigadjusts[data[datind]+1]-sigadjusts[1])
  #
  dss=cin[datind,datind,drop=F]*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  sccm=t(cin[datind,,drop=F])
  cminternal=(solve(dss+diag(length(datind))))*outer(sqrt(sigadjust),sqrt(sigadjust),'*')
  da=sapply(mupris,function(mu){
    sum((((muadjusts[data+1]-mu)%*%sccm)^2)%*%cminternal)
    #%*%(t(sccm)%*%(muadjusts[data+1]-mu))
    #as.vector((muadjusts[data+1]-mu)%*%cin%*%(muadjusts[data+1]-mu))
  })
  #a1=sccm%*%cminternal%*%t(sccm)
  #allinv=(cin-a1)
  dv-da
  }else{
    dv
  }
}

makeblocks <- function(xs,bln){
  bind=do.call(c,lapply(1:bln,function(i){
    do.call(c,lapply(2:bln,function(j){
      ofx=length(xs)*(i-1)
      ofy=length(xs)*(j-1)
      c(list(cbind(xs+ofx,xs+ofy)),
        lapply(1:(length(xs)-1),function(i){
          cbind(ofx+xs[-(length(xs):(length(xs)-i+1))],ofy+xs[-(1:i)])
        }))
    }))
  }))
}

bind=makeblocks(1:(2*wsize+1),2)

toepvals <- function(x,bind){
  sapply(bind,function(i){mean(x[i])})
}

toeptomat <- function(x,bind,sz){
  cmt=matrix(0,sz,sz)
  for(i in 1:length(bind)){
    cmt[bind[[i]]]=x[i]
  }
  cmt[cmt==0]=t(cmt)[cmt==0]
  ind1=1:(2*wsize+1)
  ind2=ind1+(2*wsize+1)
  same.strand.avg=(cmt[ind1,ind1]+cmt[ind2,ind2])/2
  cmt[ind1,ind1]=same.strand.avg
  cmt[ind2,ind2]=same.strand.avg
  cmt
}

topproj <- function(x,bind){
  spp=toepvals(x,bind)
  toeptomat(spp,bind,ncol(x))
}

topiter <- function(matinp,bind){
  matin=topproj(matinp,bind)
  ecp=eigen(matin)
  ecv=ecp$values
  while(min(ecv)< -1e-2){
    print(min(ecv))
    matin=ecp$vectors%*%diag(ecv*(ecv>0))%*%t(ecp$vectors)  
    matin=topproj(matin,bind)
    ecp=eigen(matin)
    ecv=ecp$values
  }
  matin+diag(ncol(matin))*0.011
}




for(day in 1:numdays){
    if(!file.exists(paste0('tmp/filterpar-',day,'.RData'))){
        chrc=do.call(c,lapply(1:length(chrstr),function(i){
            get(load(paste0('/dnase/tmp/',day,'-',chrstr[i],'-bgmat.RData')))
        }))
        chrc=chrc[1:min(50000,length(chrc))]
        datin=t(sapply(chrc[1:min(20000,length(chrc))],function(i){
            inverse.rle(i)
        }))
        datin[datin>2]=2
        stabval=4
        mustab=-3
        sz=ncol(datin)
        tabs = makeTable(datin,mustab,stabval)
        covin=topiter(cov(matrix(tabs[[1]][datin+1],nrow(datin))),bind)
        sci=solve(covin)
        muin= mustab
        mupri=rep(muin,sz)%*%sci
        scid = solve(sci+diag(rep(tabs[[2]][1],sz)))
                                        #scid=covin%*%solve(covin+diag(rep(1/tabs[[2]][1],sz)))%*%diag(rep(1/tabs[[2]][1],sz))
        save(gknotLG,file='tmp/gknots.RData')
        mb=makeBatch(1,length(chrc),300)
        for(j in 1:length(mb)){
            chin=chrc[mb[[j]]]
            save(chrc=chin,file=paste0('tmp/',j,'-covin.RData'))
        }
        for(m in 1:5){
            print(m)
                                        #
            musum=rep(0,ncol(covin))
            devsum=0
            muevsum=0
            covsumT=matrix(0,ncol(covin),ncol(covin))
            sqmuT=matrix(0,ncol(covin),ncol(covin))
            muT=rep(0,ncol(covin))
            tct=0
            save(stabval,mustab,sz,tabs,mupri,scid,file='tmp/tmpparams.RData')
                                        #for(j in 1:length(mb)){b
            submitJob('makecovmatqscript.r','',T,1,length(mb),1)
  #}
                                        #
            checkq()
                                        #
            for(i in 1:length(mb)){
                load(paste0('tmp/out-',i,'-covin.RData'))
                sqmuT=sqmuT+t(muall)%*%muall
                muT=muT+colSums(muall)
                covsumT=covsumT+covsum
                devsum=devsum+sum(devs)
                muevsum=muevsum+sum(muevs)
                tct=tct+nrow(muall)
            }
            cch=sqmuT/tct-(t(t(muT))%*%t(muT))/(tct^2)
            cpost=(covsumT/tct+cch)
            cpost=topiter(cpost,bind)#cpost*outer(1/sqrt(diag(cpost)),1/sqrt(diag(cpost)),'*')*mean(diag(cpost))
            muhat=muT/tct
            stabval=devsum/(tct*ncol(covin))#mean(diag(cpost))
            muin=muevsum/(tct*ncol(covin))
            print(c(stabval,muin))
            tabs = makeTable(datin,muin,stabval)
            print(tabs)
                                        #plot(cpost[1,],ylim=c(0,max(2.5,max(cpost[1,]))))
                                        #points(covin[1,],col='green')
                                        #points(Covtrue[1,],type='l',col='red')
            covin=cpost
            sci=solve(covin)
            mupri=muhat%*%sci
            scid = solve(sci+diag(rep(tabs[[2]][1],sz)))
        }
        save(scid,mupri,muhat,sci,cpost,tabs,muin,stabval,file=paste0('tmp/normparams-',day,'.RData'))
        save(scid,mupri,muhat,sci,cpost,tabs,muin,stabval,file=paste0('output/normparams-',day,'.RData'))
        load(paste0('tmp/normparams-',day,'.RData'))
        evs=eigen(cpost+diag(1,sz))
        filtermat=evs$vectors%*%diag(1/sqrt(evs$va))%*%t(evs$vectors)
        transvec=tabs[[1]]*sqrt(tabs[[2]])
        save(filtermat,transvec,file=paste0('tmp/filterpar-',day,'.RData'))
        save(filtermat,transvec,file=paste0('output/filterpar-',day,'.RData'))
    }
}

