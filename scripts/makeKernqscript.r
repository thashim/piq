#!/usr/bin/Rscript

.libPaths('/dnase/libs/')

######
# needed functions

decodeRLE <- function(str,nrows=9){
  ss=strsplit(str,';',fixed=T)[[1]]
  ssi=strsplit(ss,',',fixed=T)
  rlein=list()
  rlein$lengths=as.integer(ssi[[1]])
  rlein$values=as.integer(ssi[[2]])
  class(rlein)='rle'
  rlevec=matrix(inverse.rle(rlein),nrow=nrows*2)
}

require('Rcpp')
require('inline')

rssST='
Rcpp::NumericMatrix cdatin(datain); // numdays x window
Rcpp::NumericMatrix cmarginals(marginals); // maxcts x numdays
Rcpp::NumericVector cSSmat(SS); // numdays*numdays-1 x maxcts x maxcts
Rcpp::NumericVector cOSmat(OS); //
Rcpp::NumericVector cOSnegmat(OSneg);
Rcpp::NumericVector cDDmat(DD);
Rcpp::NumericVector ssflag(sflag);
int ndays=cmarginals.ncol();
int maxcts=cmarginals.nrow();
for(int i=0; i<cdatin.ncol();i++){
for(int j=0; j<ndays;j++){
int lanc=cdatin(j,i);
if(lanc<maxcts){cmarginals(lanc,j)++;}
for(int l=0;l<cdatin.ncol();l++){
int abi = abs(l-i);
if(abi<ssflag[1]){
int ranc=cdatin(j,l);
int rancop=cdatin(j+ndays,l);
cSSmat[ndays*(abi+ssflag[1]*ranc+ssflag[1]*maxcts*lanc)+j]++;
if(l>i){
cOSmat[ndays*(abi+ssflag[1]*rancop+ssflag[1]*maxcts*lanc)+j]++;
}else{
cOSnegmat[ndays*(abi+ssflag[1]*rancop+ssflag[1]*maxcts*lanc)+j]++;
}
}
}
for(int k=j; k<ndays;k++){
  int ranc=cdatin(k,i);
  cDDmat[ndays*(k+ndays*ranc+ndays*maxcts*lanc)+j]++;
}
}
}
'
reducestat <- cxxfunction(signature(datain='matrix',marginals='matrix',SS='numeric',OS='numeric',OSneg='numeric',DD='numeric',sflag='numeric'),rssST,plugin='Rcpp')

##########
# rest of code
# params - chr, numdays,  spos, nlines

args=as.double(commandArgs(trailingOnly=TRUE))
chr=args[1]
numdays=args[2]
spos=args[3]
nlines=args[4]

load('/dnase/params.RData')

#SS=lapply(1:numdays,function(i){array(0,dim=c(numbp,maxct+1,maxct+1))})
#OS=lapply(1:numdays,function(i){array(0,dim=c(numbp,maxct+1,maxct+1))})
#OSneg=lapply(1:numdays,function(i){array(0,dim=c(numbp,maxct+1,maxct+1))})
#DD=lapply(1:numdays,function(i){array(0,dim=c(numdays,maxct+1,maxct+1))})
SS=array(0,dim=c(numdays,numbp,maxct+1,maxct+1))
OS=array(0,dim=c(numdays,numbp,maxct+1,maxct+1))
OSneg=array(0,dim=c(numdays,numbp,maxct+1,maxct+1))
DD=array(0,dim=c(numdays,numdays,maxct+1,maxct+1))
marginals=matrix(0,maxct+1,numdays)


fconn=file(paste(datain,chrstr[chr],'datin.txt',sep=''),open='rt')
offsets=c(0,scan(paste(datain,chrstr[chr],'datin.txt.index',sep=''))+1)
seekpos=cumsum(offsets)[spos]
seek(fconn,where=seekpos)
for(i in 1:nlines){
    str=readLines(fconn,1)
    dtest=decodeRLE(str,numdays)
    rtes=reducestat(dtest,marginals,SS,OS,OSneg,DD,c(0,numbp))
}
save(SS,OS,OSneg,DD,marginals,maxct,numbp,file=paste(tmpfile,chr,'-',spos,'.RData',sep=''))




