#!/usr/bin/Rscript

source('initec2.r')
require("Rcpp")
require("inline")

discrCallST = '
Rcpp::NumericVector cdeltaPOS(deltaPOS);
Rcpp::NumericVector cdeltaNEG(deltaNEG);
Rcpp::NumericVector ctfid(tfid);
Rcpp::NumericVector cblocus(blocus);
Rcpp::NumericVector coffset(offset);
Rcpp::NumericMatrix cprofvec(profvec);
Rcpp::NumericVector evvec(ctfid.size());
for(int i =0; i < cblocus.size(); i++){
int locus = abs(cblocus[i])-1+coffset[0];
int halfwidth = (cprofvec.nrow()-2)/4;
int startpt = std::max(0,locus-halfwidth);
int kstartpt = std::max(0,halfwidth-locus);
int endpt = std::min(((int)cdeltaPOS.size())-1,locus+halfwidth);
int ktype = (ctfid[i]-1);
Rcpp::NumericVector SSmat = cdeltaPOS;
Rcpp::NumericVector OSmat = cdeltaNEG;
if(cblocus[i]<0){
SSmat=cdeltaNEG;
OSmat=cdeltaPOS;
}
double evval=0;
for(int j=0; j<=endpt-startpt;j++){
if(cblocus[i]<0){
evval+=cprofvec(kstartpt+j,ktype)*SSmat[startpt+j];
evval+=cprofvec(kstartpt+j+halfwidth*2+1,ktype)*OSmat[startpt+j];
}else{
evval+=cprofvec(kstartpt+j,ktype)*SSmat[endpt-j];
evval+=cprofvec(kstartpt+j+halfwidth*2+1,ktype)*OSmat[endpt-j];
}
}
evvec[i]=evval;
}
return evvec;
'
discrCall <- cxxfunction(signature(deltaPOS="numeric",deltaNEG='numeric',tfid='numeric',blocus='numeric',offset='numeric',profvec="numeric"),discrCallST,plugin="Rcpp",includes="#include <numeric>\n #include<math.h>")



args=as.double(commandArgs(trailingOnly=TRUE))
chr=args[1]
numdays=args[2]
spos=args[3]
epos=args[4]

bindoffset=500
tfblock=10

#makebit<-function(x,conn=NULL){memCompress(serialize(x,connection=NULL),type='bzip2')}
#makebit<-function(x){paste(as.character(serialize(x,connection=NULL)),collapse='')}

  load('/dnase/params.RData')
  load(paste(kernout,'profsbgdr2.RData',sep=''))
  load(paste0(tmpfile,'callcommon-bg.RData'))
  realtmp = calltmp
  for(day in 0:length(discrs)){
    tfbatch=makeBatch(1,maxtf,tfblock)
    for(tfb in tfbatch){
    sr=paste(realtmp,day,'/',chr,'-',spos,'-',tfb[1],'.out.gz',sep='')
    if(file.exists(sr)){file.remove(sr)}
    sr=paste(realtmp,day,'/',chr,'-',spos,'-',tfb[1],'.out',sep='')
    if(file.exists(sr)){file.remove(sr)}
    }
  }
  tfret=list()
  for(ppar in spos:epos){
    load(paste(pwmout,'outfile-',chr,'-',ppar,'.RData',sep=''))
    load(paste(bgout,'chr-',chr,'-',ppar,'.RData',sep=''))
    #allmatlis=list()#lapply(1:length(discrs),function(day){lapply(1:maxtf,function(i){lapply(1:length(pwmscores),function(j){integer(0)})})})
    #allmatdiscr=list()#lapply(1:maxtf,function(i){lapply(1:length(pwmscores),function(j){integer(0)})})
    allmatlis=rep('',length(pwmscores)*maxtf*numdays)
    allmatdiscr=rep('',length(pwmscores)*maxtf)
    for(i in 1:length(pwmscores)){
      print(c(ppar,i))
      ptemp=pwmscores[[i]]
      if(length(ptemp>0)){
      numdays=length(discrs)
      dtestPOS=fgset[[i]][[1]][[1]]
      dtestNEG=fgset[[i]][[1]][[2]]
      hsens=fgset[[i]][[2]][abs(ptemp[,2])+bindoffset,,drop=F]
      fgset[[i]]=list(NULL)
      gc();
      rowoffs=c(0,findInterval(1:maxtf,ptemp[,1]))
      foundTF=which(diff(rowoffs)>0)
      #evsmat=matrix(0,nrow(ptemp),numdays)
      #evsnmat=matrix(0,nrow(ptemp),numdays)
      trmat=matrix(0,nrow(ptemp),2)
      for(tfnum in foundTF){
        tfind=(rowoffs[tfnum]+1):(rowoffs[tfnum+1])
        allmatdiscr[(tfnum-1)*length(pwmscores)+i]=paste(paste(i,apply(ptemp[tfind,-1,drop=F],1,paste,collapse=','),sep=','),collapse=';')
      }
      for(k in 1:length(exptsub)){
        trmat[,2]=hsens[,k]
        discvec=t(discrs[[k]][[1]])
        evs=discrCall(dtestPOS[k,],dtestNEG[k,],ptemp[,1],ptemp[,2],bindoffset,discvec)
        evs=evs+discrs[[k]][[3]][ptemp[,1]]
        trmat[,1]=signif(evs,digits=4)
        trall=paste(trmat[,1],trmat[,2],sep=',')
        tfls = sapply(foundTF,function(tfnum){
          tfind=(rowoffs[tfnum]+1):(rowoffs[tfnum+1])
          paste(trall[tfind],collapse=',')
        })
        allmatlis[(i-1)*length(exptsub)*maxtf+(k-1)*maxtf+foundTF]=tfls
      }
      }
    }
    tfbatch=makeBatch(1,maxtf,tfblock)
    tfret[[ppar-spos+1]]=lapply(tfbatch,function(tfb){
      #oconns<-lapply(0:length(discrs),function(day){gzfile(paste(realtmp,day,'-',chr,'-',spos,'-',tfb[1],'.out',sep=''),open='ab')})
      tdr=sapply(tfb,function(tfnum){
        paste(tfnum,';',paste(allmatdiscr[(tfnum-1)*length(pwmscores)+1:length(pwmscores)],collapse=';'),sep='')
      })
      #writeLines(tdr,oconns[[1]])
      #for(day in 1:length(discrs)){
      ddr=lapply(1:length(discrs),function(day){
        lpp=sapply(tfb,function(tfnum){
          paste(tfnum,';',paste(allmatlis[((1:length(pwmscores))-1)*length(exptsub)*maxtf+(day-1)*maxtf+tfnum],collapse=';'),sep='')
        })
        #writeLines(lpp,oconns[[day+1]])
        #allmatlis[[day]]<-list(NULL)
        #gc()
      })
      list(tdr,ddr)
      #for(i in 1:(length(discrs)+1)){close(oconns[[i]])}
    })
  }

tfbatch=makeBatch(1,maxtf,tfblock)
for(tfind in 1:length(tfbatch)){
  str=do.call(c,lapply(1:length(tfret),function(k){tfret[[k]][[tfind]][[1]]}))
  save(str,file=paste(realtmp,0,'/',chr,'-',spos,'-',min(tfbatch[[tfind]]),'-ref.out',sep=''),compress='bzip2')
  for(day in 1:length(discrs)){
    str=do.call(c,lapply(1:length(tfret),function(k){tfret[[k]][[tfind]][[2]][[day]]}))
    save(str,file=paste(realtmp,day,'/',chr,'-',spos,'-',min(tfbatch[[tfind]]),'-ref.out',sep=''),compress='bzip2')
  }
}



#for(day in 0:length(discrs)){
#system(paste('perl -nle \'print length\' ',fns,filenom,' > ',fns,filenom,'.index',sep=''))
#}





