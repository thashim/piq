#!/usr/bin/Rscript
setwd('/dnase')
.libPaths('/dnase/libs/')

require("Rcpp")
require("inline")

decodeRLE <- function(str,nrows=9){
  ss=strsplit(str,';',fixed=T)[[1]]
  ssi=strsplit(ss,',',fixed=T)
  rlein=list()
  rlein$lengths=as.integer(ssi[[1]])
  rlein$values=as.integer(ssi[[2]])
  class(rlein)='rle'
  rlevec=matrix(inverse.rle(rlein),nrow=nrows*2)
}

convKernST = '
Rcpp::NumericVector cdeltaPOS(deltaPOS);
Rcpp::NumericVector cdeltaNEG(deltaNEG);
Rcpp::NumericVector ctfid(tfid);
Rcpp::NumericVector cblocus(blocus);
Rcpp::NumericVector coffset(offset);
Rcpp::NumericMatrix cprofvec(profvec);
Rcpp::NumericMatrix csigvec(sigvec);
Rcpp::NumericVector ctct(tct);
for(int i =0; i < cblocus.size(); i++){
int locus = abs(cblocus[i])-1+coffset[0];
int halfwidth = (cprofvec.nrow()-2)/4;
int startpt = std::max(0,locus-halfwidth);
int kstartpt = std::max(0,halfwidth-locus);
int endpt = std::min(((int)cdeltaPOS.size())-1,locus+halfwidth);
int ktype = (ctfid[i]-1);
ctct[ktype]++;
Rcpp::NumericVector SSmat = cdeltaPOS;
Rcpp::NumericVector OSmat = cdeltaNEG;
if(cblocus[i]<0){
  SSmat=cdeltaNEG;
  OSmat=cdeltaPOS;
}
for(int j=0; j<=endpt-startpt;j++){
    //ckmat(kstartpt+j,ktype-1)+=(SSmat[startpt+j]);
    if(cblocus[i]<0){
    cprofvec(kstartpt+j,ktype)+=SSmat[startpt+j];
    cprofvec(kstartpt+j+halfwidth*2+1,ktype)+=OSmat[startpt+j];
    csigvec(kstartpt+j,ktype)+=SSmat[startpt+j]*SSmat[startpt+j];
    csigvec(kstartpt+j+halfwidth*2+1,ktype)+=OSmat[startpt+j]*OSmat[startpt+j];
    }else{
    cprofvec(kstartpt+j,ktype)+=SSmat[endpt-j];
    cprofvec(kstartpt+j+halfwidth*2+1,ktype)+=OSmat[endpt-j];
    csigvec(kstartpt+j,ktype)+=SSmat[endpt-j]*SSmat[endpt-j];
    csigvec(kstartpt+j+halfwidth*2+1,ktype)+=OSmat[endpt-j]*OSmat[endpt-j];
    }
  }
}
return coffset;
'
convtest <- cxxfunction(signature(deltaPOS="numeric",deltaNEG='numeric',tfid='numeric',blocus='numeric',offset='numeric',profvec="numeric",sigvec='numeric',tct='numeric'),convKernST,plugin="Rcpp",includes="#include <numeric>\n #include<math.h>")



convKern2ST = '
Rcpp::NumericVector cdeltaPOS(deltaPOS);
Rcpp::NumericVector cdeltaNEG(deltaNEG);
Rcpp::NumericVector ctfid(tfid);
Rcpp::NumericVector cblocus(blocus);
Rcpp::NumericVector coffset(offset);
Rcpp::NumericMatrix cprofvec(profvec);
Rcpp::NumericMatrix cscat(scatvec);
Rcpp::NumericVector ctct(tct);
for(int i =0; i < cblocus.size(); i++){
int locus = abs(cblocus[i])-1+coffset[0];
int halfwidth = (cprofvec.nrow()-2)/4;
int startpt = std::max(0,locus-halfwidth);
int kstartpt = std::max(0,halfwidth-locus);
int endpt = std::min(((int)cdeltaPOS.size())-1,locus+halfwidth);
int ktype = (ctfid[i]-1);
ctct[ktype]++;
Rcpp::NumericVector SSmat = cdeltaPOS;
Rcpp::NumericVector OSmat = cdeltaNEG;
if(cblocus[i]<0){
  SSmat=cdeltaNEG;
  OSmat=cdeltaPOS;
}
for(int j=0; j<=endpt-startpt;j++){
    cprofvec(kstartpt+j,ktype)+=SSmat[startpt+j];
    cprofvec(kstartpt+j+halfwidth*2+1,ktype)+=OSmat[startpt+j];
    for(int k=j; k<=endpt-startpt;k++){
      cscat(kstartpt+j,kstartpt+k)+=SSmat[startpt+j]*SSmat[startpt+k];
      cscat(kstartpt+j,kstartpt+k+halfwidth*2+1)+=SSmat[startpt+j]*OSmat[startpt+k];
      cscat(kstartpt+j+halfwidth*2+1,kstartpt+k)+=OSmat[startpt+j]*SSmat[startpt+k];
      cscat(kstartpt+j+halfwidth*2+1,kstartpt+k+halfwidth*2+1)+=OSmat[startpt+j]*OSmat[startpt+k];
    }
  }
}
return coffset;
'
convtest2 <- cxxfunction(signature(deltaPOS="numeric",deltaNEG='numeric',tfid='numeric',blocus='numeric',offset='numeric',profvec="numeric",scatvec='numeric',tct='numeric'),convKern2ST,plugin="Rcpp",includes="#include <numeric>\n #include<math.h>")




args=as.double(commandArgs(trailingOnly=TRUE))
chr=args[1]
numdays=args[2]
spos=args[3]
epos=args[4]
wsize=args[5]
tfmax=args[6]

load('/dnase/params.RData')

lao=lapply(1:length(exptsub),function(i){
  list(profvec=matrix(0,wsize*2,tfmax),
       sigvec=matrix(0,wsize*2,tfmax),
       scatmat=matrix(0,wsize*2,wsize*2),
       scatvec=matrix(0,wsize*2,1),
       totct=rep(0,tfmax),
       bgsum=0,
       bgn=0,
       covsum=0)
})


load(paste(kernout,'minvals.RData',sep=''))
bindoffset=500

#fconn=file(paste('data/chr-',chr,'-datin.txt',sep=''),open='rt')
#offsets=c(0,scan(paste('data/chr-',chr,'-datin.txt.index',sep=''))+1)
#seekpos=cumsum(offsets)[(spos-1)*20+1]
#seek(fconn,where=seekpos)
for(ppar in spos:epos){
  load(paste(pwmout,'outfile-',chrstr[chr],'-',ppar,'.RData',sep=''))
  load(paste(bgout,chrstr[chr],'-',ppar,'.RData',sep=''))
  for(i in 1:length(pwmscores)){
    ptemp=pwmscores[[i]]
    if(length(ptemp)>0){
    #str=readLines(fconn,1)
    #dtest=decodeRLE(str,numdays)
    dtestPOS=fgset[[i]][[1]][[1]]
    dtestNEG=fgset[[i]][[1]][[2]]
    subsel=ptemp[,3]>=minvals[ptemp[,1]]
    ptemp1=ptemp[subsel,]
    #ptemp2=ptemp[subsel,]
    #ptemp1[,2]=ptemp1[,2]*-1
    #pmask = which(ptemp1[,2]>0)
    covseq=seq(1,ncol(dtestPOS)-bindoffset-wsize,by=2*wsize)
    for(k in 1:length(exptsub)){
      convtest(dtestPOS[k,],dtestNEG[k,],ptemp1[,1],ptemp1[,2],bindoffset,lao[[k]]$profvec,lao[[k]]$sigvec,lao[[k]]$totct)
      convtest2(dtestPOS[k,],dtestNEG[k,],rep(1,length(covseq)),covseq,bindoffset,lao[[k]]$scatvec,lao[[k]]$scatmat,lao[[k]]$covsum)
      lao[[k]]$bgsum=lao[[k]]$bgsum+sum(dtestPOS[k,])
      lao[[k]]$bgn=lao[[k]]$bgn+ncol(dtestPOS)
    }
    }
  }
}
save(lao,file=paste(tmpfile,'ker-',chrstr[chr],'-',spos,'.RData',sep=''))


