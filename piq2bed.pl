#!/usr/bin/env perl
##############################################
##############################################
# Run from root directory:
#         piq2bed.pl <output_prefix>
#
# To install PerlIO::gzip locally
#   1) 'perl -MCPAN -e shell'  (autoconfig)
#   2) modify ~/.cpan/CPAN/MyConfig.pm, set 'makepl_arg' => q[PREFIX=/home/cwo/],
#   3) export PERL5LIB=/home/cwo/lib/perl/5.x.x/:$PERL5LIB
#
##############################################
##############################################

use PerlIO::gzip;
use POSIX qw/floor/;
#use strict;
#use warnings;
use File::Find qw(find);


##############################################
# globals 
##############################################

$MIN_RELIABILITY_SCORE = 5;
$MIN_TF_BINDING_PROB = 0.99;


if ((@ARGV) < 1) { 
   print "piq2bed.pl <output_prefix>\n";
   exit(1);
}
$OUTPUT_FILENAME = $ARGV[0];


%EXPT_MAP = {};  # sample ID -> sample name
%CHROM_MAP = {};  # chrom ID -> chrom name
%TF_NAME_MAP = {};  # TF ID -> TF name
%RELIABLE_TFS = {};  # condition ID -> array(TF ID's)


#########################################################################
#########################################################################
# Assumes file "exptlist.txt" explains Sample ID -> Sample name mapping
#########################################################################
#########################################################################
open(EXPTLIST_FILE, "<", "exptlist.txt") || die "can't open file exptlist.txt";
my $exptlist_linenum = 1;
while (<EXPTLIST_FILE>) {
   if (/^\s*?(\S+?)\s*?$/) {
      $EXPT_MAP{$exptlist_linenum} = $1;
      $exptlist_linenum += 1;
   }
}
close(EXPTLIST_FILE);

#########################################################################
#########################################################################
# Assumes file "chrlist.txt" explains chrom ID -> chrom name
#########################################################################
#########################################################################
open(CHRLIST_FILE, "<", "chrlist.txt") || die "can't open file chrlist.txt";
my $chrlist_linenum = 1;
while (<CHRLIST_FILE>) {
   if (/^\s*?(\S+?)\s*?$/) {
      $CHROM_MAP{$chrlist_linenum} = $1;
      $chrlist_linenum += 1;
   }
}
close(CHRLIST_FILE);



##############################################
##############################################
# Read *-reliability.csv files per condition  
##############################################
##############################################

for (my $expt=1; $expt < (scalar keys %EXPT_MAP); $expt += 1) {

   #print "$expt-reliability.csv\n";
   open(REL_FILE, "<", $expt."-reliability.csv") || die "can't open file $expt-reliability.txt";
    while (<REL_FILE>) {
      if (/^\"(\S+?)\",\"(.+?)\",(\S+?),(\S+?),(\S+?)\s*?$/) {
         if ($2 eq "tfname") { next; }
         if ($4 >= $MIN_RELIABILITY_SCORE) {
            my $id = $1;
            my $name = (split(/\s/, $2))[1];
            if ($name eq "") { $name = $2; }

            $TF_NAME_MAP{$id} = $name;
            push(@{$RELIABLE_TFS{$expt}}, $id);
         }
      }
   }
   close(REL_FILE);

} # end for each experiment


##############################################
##############################################
# Read each TF "compact" file per condition
##############################################
##############################################
for (my $expt=1; $expt < (scalar keys %EXPT_MAP); $expt += 1) {
   system("rm $OUTPUT_FILENAME-$EXPT_MAP{$expt}.bed.gz");
}
for (my $expt=1; $expt < (scalar keys %EXPT_MAP); $expt += 1) {

   open(OUTPUT, ">>:gzip(lazy)", $OUTPUT_FILENAME."-".$EXPT_MAP{$expt}.".bed.gz");
   #open(OUTPUT, ">>", $OUTPUT_FILENAME."-".$EXPT_MAP{$expt}.".bed");

   my $tf_list = $RELIABLE_TFS{$expt};
   for $tf (@$tf_list) {

      #open(TF_FILE, "<:gzip", $tf."-".$expt."-compact.csv.gz") || die "can't open file $tf-$expt-compact.csv.gz";
      open(TF_FILE, "<:gzip", $tf."-".$expt."-compact.csv.gz") || next;

      #print "$tf-$expt-compact.csv\n";
      while (<TF_FILE>) {

         my @cols = split(/,/);
         if ($cols[6] > $MIN_TF_BINDING_PROB) {
            my $chr = $CHROM_MAP{$cols[1]};
            my $start;
            my $end;
            if (floor($cols[2]) == $cols[2]) {
               $start = $cols[2]-2;
               $end = $cols[2]+3;
            } else {
               $start = floor($cols[2])-1;
               $end = floor($cols[2])+3;
            }
	    my $bed_score = floor($cols[6]*1000);

            print OUTPUT "$chr\t$start\t$end\t$TF_NAME_MAP{$tf}\t$bed_score\n";
         }
      }
      close(TF_FILE);
   }

   close(OUTPUT);

} #end each expt


##############################################
##############################################
# Sort bed files for good form
##############################################
##############################################
for (my $expt=1; $expt < (scalar keys %EXPT_MAP); $expt += 1) {
   #system("sort -k1,1 -k2,2n <(zcat $OUTPUT_FILENAME-$EXPT_MAP{$expt}.bed.gz) |gzip -9 -c >$OUTPUT_FILENAME-$EXPT_MAP{$expt}.temp");
   system("gunzip -c $OUTPUT_FILENAME-$EXPT_MAP{$expt}.bed.gz |sort -k1,1 -k2,2n|gzip -9 -c >$OUTPUT_FILENAME-$EXPT_MAP{$expt}.temp");
   system("cp $OUTPUT_FILENAME-$EXPT_MAP{$expt}.temp $OUTPUT_FILENAME-$EXPT_MAP{$expt}.bed.gz");
   system("rm $OUTPUT_FILENAME-$EXPT_MAP{$expt}.temp");
}


